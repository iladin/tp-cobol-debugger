
                MOVE SPACES           TO TPD-SEARCH.

                GO TO DO-EXIT-ANIM.
      *>
      *> animator data procedures managment
      *>
        DO-ANIM.

                MOVE "A"               TO TPD-SW-ANIM.
                SET TPD-ADDRESS        TO NULL.
                MOVE ZEROS             TO TPD-LENGTH.
                GO TO DO-ANIM-TEST-VARIABLES.

        DO-ANIM1.

                PERFORM DO-VIEW-ANIM   THRU EX-DO-VIEW-ANIM.

                IF TPD-NAME = SPACES
                 MOVE SPACES TO TPD-SW-ANIM
                  GO TO EX-DO-ANIM.

                MOVE "V"               TO TPD-SW-ANIM.

                COPY "do-anim.cpy".

                GO TO DO-ANIM.

        EX-DO-ANIM.
                EXIT.

        DO-VIEW-ANIM.

                CALL TPD-VIEW   USING  BY REFERENCE TPD-SW-ANIM
                                       BY REFERENCE TPD-INDEX
                                       BY REFERENCE TPD-NAME
                                       BY REFERENCE TPD-FILE
                                       BY REFERENCE TPD-ADDRESS
                                       BY REFERENCE TPD-LENGTH.


        EX-DO-VIEW-ANIM.
                EXIT.

        DO-EXIT-ANIM.


