000010  IDENTIFICATION DIVISION.
000020  PROGRAM-ID.    animate.
000030*---------------------------------------------------------------------
000040*
000050*  ANIMATORE INTERATTIVO PER GNUCOBOL
000060*
000070* Copyright (C) 2010-2014 Federico Priolo T&P Soluzioni Informatiche Srl
000080*
000090* This program is free software; you can redistribute it and/or modify
000100* it under the terms of the GNU General Public License as published by
000110* the Free Software Foundation; either version 2, or (at your option)
000120* any later version.
000130*
000140* This program is distributed in the hope that it will be useful,
000150* but WITHOUT ANY WARRANTY; without even the implied warranty of
000160* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
000170* GNU General Public License for more details.
000180*
000190* You should have received a copy of the GNU General Public License
000200* along with this software; see the file COPYING.  If not, write to
000210* the Free Software Foundation, 51 Franklin Street, Fifth Floor
000220* Boston, MA 02110-1301 USA
000230*
000240*
000250*---------------------------------------------------------------------
000260*
000270*  TPD-ACTION = "S" SINGLE STEP
000280*             = "F" FAST STEP (WITHOUT SHOWING ANYTHING..)
000290*             = "A" ANIMATE STEP (STEP WITH SHORT DELAY SLEEP=....)
000300*
000310*  TIPO-LIN   = "I" WHERE YOU CAN SET A BREAKPOINT
000320*
000330*---------------------------------------------------------------------
000340  ENVIRONMENT DIVISION.
000350  CONFIGURATION SECTION.
000360  SOURCE-COMPUTER.        PC-IBM.
000370  OBJECT-COMPUTER.        PC-IBM.
000380  SPECIAL-NAMES.
000390
000400     DECIMAL-POINT IS COMMA.
000410
000420  INPUT-OUTPUT SECTION.
000430  FILE-CONTROL.
000440
000450          SELECT ARK-FIELD ASSIGN TO FILE-FIELD
000460           ORGANIZATION IS INDEXED
000470           ACCESS MODE  IS  DYNAMIC
000480           RECORD KEY   IS  CHIAVE-FIELD
000490           ALTERNATE RECORD KEY IS BREAK-FIELD WITH DUPLICATES
000500           FILE STATUS  IS STATUS-FIELD.
000510
000520          SELECT ARK-LIN ASSIGN TO FILE-LIN
000530           ORGANIZATION IS INDEXED
000540           ACCESS MODE  IS  DYNAMIC
000550           RECORD KEY   IS  CHIAVE-LIN
000560           LOCK MODE    IS AUTOMATIC WITH LOCK ON RECORD
000570           FILE STATUS  IS STATUS-LIN.
000580
000590
000600  DATA DIVISION.
000610  FILE SECTION.
000620
000630  FD ARK-FIELD.
000640  01 REC-FIELD GLOBAL.
000650   02 CHIAVE-FIELD.
000660      09 NOME-FIELD             PIC X(30).
000670   02 TIPO-FIELD                PIC X(01).
000680   02 VALUE-FIELD               PIC X(256).
000690   02 SIZE-FIELD                PIC 9(9) COMP-5.
000700   02 BREAK-FIELD               PIC X.
000710     88 BREAK-IF-EQUAL          VALUE "1".
000720     88 BREAK-IF-LESS           VALUE "2".
000730     88 BREAK-IF-GREATER        VALUE "3".
000740     88 BREAK-IF-NOT-GREATER    VALUE "4".
000750     88 BREAK-IF-NOT-LESS       VALUE "5".
000760     88 BREAK-IF-CHANGE         VALUE "6".
000770     88 BREAK-IF-OK             VALUE "1" THRU "6".
000780   02 FILLER                    PIC XX.
000790   02 VALUE-BREAK-FIELD         PIC X(256).
000800   02 KEY-FIELD-FIELD           PIC X(30).
000810   02 ADDRESS-FIELD             USAGE POINTER.
000820   02 LINE-FIELD                PIC X(6).
000830   02 FILLER-FIELD              PIC X(50).
000840
000850  FD ARK-LIN.
000860  01 REC-LIN.
000870   02 CHIAVE-LIN.
000880    05 PAG-LIN                  PIC 9(4).
000890    05 RIGA-LIN                 PIC 9(2).
000900   02 DATI-LIN                  PIC X(80).
000910   02 TIPO-LIN                  PIC X.
000920   02 BREAK-LIN                 PIC X.
000930   02 CODICE-LIN                PIC X.
000940   02 ROW-PERC-LIN              PIC 9(2).
000950   02 FILLER                    PIC X(17).
000960
000970  WORKING-STORAGE SECTION.
000980
000990     COPY "workanim.cpy".
001000
001010  01 A                          PIC X VALUE SPACE.
001020  01 B                          PIC X VALUE SPACE.
001030  01 C                          PIC X VALUE SPACE.
001040  01 D                          PIC X VALUE SPACE.
001050  01 FILE-LIN                   PIC X(200) VALUE SPACE.
001060  01 FILE-FIELD                 PIC X(200) VALUE SPACE.
001070  01 FILE-LOG                   PIC X(200) VALUE SPACE.
001080  01 STATUS-LIN                 PIC XX     VALUE SPACE.
001090  01 STATUS-LOG                 PIC XX     VALUE SPACE.
001100  01 STATUS-FIELD               PIC XX     VALUE SPACE.
001110  01 RIGA                       PIC 99     VALUE ZERO.
001120  01 XX                         PIC X      VALUE SPACE.
001130  01 BIANCO                     PIC X(80)  VALUE ALL " ".
001140  01 IND                        PIC 99     VALUE ZEROS.
001150  01 QUESTO                     PIC 9999 VALUE ZERO.
001160  01 SETTA-QUESTO               PIC X(10) VALUE SPACES.
001170  01 STRINGA.
001180    02 TESTO1                   PIC X(18) VALUE SPACES.
001190    02 TESTO2                   PIC X(92) VALUE SPACES.
001200    02 TESTO3                   PIC X(90) VALUE SPACES.
001210  01 MESSAGGIO                  PIC X(80) VALUE SPACE.
001220  01 EOF-DO                     PIC X     VALUE SPACE.
001230  01 EOF-FIELD                  PIC X     VALUE SPACE.
001240  01 COMANDO                    PIC X(40) VALUE SPACES.
001250  01 MAX                        PIC 9999  VALUE ZEROS.
001260  01 IND1                       PIC 99 VALUE ZEROS.
001270  01 IND2                       PIC 99 VALUE ZEROS.
001280  01 DO-BREAK                   PIC X  VALUE SPACE.
001290
001300     COPY "screenio.cpy".
001310
001320
001330  01 COMANDI.
001340    05 FILLER                    PIC X(54) VALUE
001350     "Next Break Go fast Animate View data eXscape Find Cont".
001360    05 FILLER                    PIC X(26) VALUE
001370     "inue find  atch  truct.  ".
001380  01 COMANDI1.
001390    05 FILLER                    PIC X(80) VALUE
001400     "Running to the next break or STOP RUN.. ".
001410
001420  01 WHERE-ARE                  PIC X(6).
001430  01 DATA-WORK                  PIC X(1024) BASED.
001440  01 RIGA-VIDEO                 PIC X(48).
001450  78 A1                         VALUE X"DA".
001460  78 A2                         VALUE X"C0".
001470  78 A3                         VALUE X"BF".
001480  78 A4                         VALUE X"D9".
001490  78 T1                         VALUE X"C2".
001500  78 T2                         VALUE X"C1".
001510  78 LO                         VALUE X"C4".
001520  78 LV                         VALUE X"B3".
001530
001540
001550
001560  LINKAGE SECTION.
001570  01 PROG-LIN                   PIC X(6).
001580
001590  PROCEDURE DIVISION USING PROG-LIN.
001600  INIZIO SECTION.
001610
001620      MOVE PROG-LIN            TO WHERE-ARE.
001630
001640      PERFORM APERTURE         THRU EX-APERTURE.
001650
001660      PERFORM ESEGUI           THRU EX-ESEGUI.
001670
001680      PERFORM CHIUSURE         THRU EX-CHIUSURE.
001690
001700      GOBACK.
001710
001720  APERTURE.

001730
001740      MOVE "temp/" TO PATH-TEMP
001750      STRING PATH-TEMP DELIMITED BY "     "
001760       TPD-WHERE ".dat" DELIMITED BY SIZE INTO FILE-LIN.
001770
001780      STRING PATH-TEMP DELIMITED BY "     "
001790       TPD-WHERE ".log" DELIMITED BY SIZE INTO FILE-LOG.
001800
001810      STRING PATH-TEMP DELIMITED BY "     "
001820       TPD-WHERE ".fld" DELIMITED BY SIZE INTO FILE-FIELD.
001830
001840      OPEN I-O ARK-LIN.
001850
001860      IF STATUS-LIN  = "35"
001870       MOVE SPACES     TO MESSAGGIO
001880       string "Animate: file " DELIMITED BY SIZE
001890       FILE-LIN DELIMITED BY "   "
001900       " unavailable, recompile the source " DELIMITED BY SIZE
001910       INTO MESSAGGIO
001920       DISPLAY MESSAGGIO
001930        LINE 1 COL 01 WITH REVERSE-VIDEO
001940        PERFORM PAUSE  THRU EX-PAUSE
001950         STOP RUN
001960         END-IF.
001970
001980      OPEN I-O ARK-FIELD.
001990
002000      IF STATUS-FIELD = "35"
002010       MOVE SPACES     TO MESSAGGIO
002020       string "Animate: file " DELIMITED BY SIZE
002030       FILE-FIELD DELIMITED BY "   "
002040       " unavailable, recompile the source " DELIMITED BY SIZE
002050       INTO MESSAGGIO
002060       DISPLAY MESSAGGIO
002070        LINE 1 COL 01 WITH REVERSE-VIDEO
002080        PERFORM PAUSE  THRU EX-PAUSE
002090        CLOSE ARK-LIN
002100         STOP RUN
002110         END-IF.
002120
002130  EX-APERTURE.
002140      EXIT.
002150
002160
002170  CHIUSURE.
002180
002190      CLOSE ARK-LIN
002200            ARK-FIELD.
002210
002220      IF TPD-SW-ANIM = "Z"
002230       CALL "CBL_DELETE_FILE" USING FILE-LOG
002240       PERFORM GOOD-BYE       THRU EX-GOOD-BYE
002250       END-IF.
002260
002270  EX-CHIUSURE.
002280      EXIT.
002290
002300  PAUSE.
002310
002320       DISPLAY " ".
002330       DISPLAY "ANIMATE: Press any key to continue..."
002340       ACCEPT XX      LINE 25 COL 79 WITH AUTO.
002350
002360  EX-PAUSE.
002370       EXIT.
002380
002390  GOOD-BYE.
002400
002410       PERFORM PAUSE THRU EX-PAUSE.
002420
002430       DISPLAY "  " LINE 01 COL 01 WITH ERASE EOS
002440       DISPLAY "  TP-COBOL-DEBUGGER  has  finished to run    "
002450                    line 02 COL 01 WITH ERASE EOL
002460       DISPLAY "  It is an opensource work, any suggestions  "
002470                    line 03 COL 01 WITH ERASE EOL
002480       DISPLAY "  will be appreciated you can send me an email "
002490                    line 04 COL 01 WITH ERASE EOL
002500       DISPLAY "  at priolof@tp-srl.it to communicate them."
002510                    line 05 COL 01 WITH ERASE EOL
002520       display "  If you have found TP-COBOL-DEBUGGER useful   "
002530                    line 06 COL 01 WITH ERASE EOL
002540       DISPLAY "  and comfortable tell somebody you think "
002550                    line 07 COL 01 WITH ERASE EOL
002560       DISPLAY "  could be interested to try and use it. "
002570                    line 08 COL 01 WITH ERASE EOL
002580       DISPLAY "  If you have found it absolutely useless...then"
002590                    line 09 COL 01 WITH ERASE EOL
002600       DISPLAY "                                         "
002610                    line 10 COL 01 WITH ERASE EOL
002620       DISPLAY "                                         "
002630                    line 11 COL 01 WITH ERASE EOL
002640       DISPLAY "                   tell only me. "
002650                    line 12 COL 01 WITH ERASE EOL
002660       DISPLAY "                                         "
002670                    line 13 COL 01 WITH ERASE EOL
002680       DISPLAY "                                         "
002690                    line 14 COL 01 WITH ERASE EOL
002700       DISPLAY "           T&P Soluzioni Informatiche Srl"
002710                    line 15 COL 01 WITH ERASE EOL
002720       DISPLAY "                    Federico Priolo      "
002730                    line 16 COL 01 WITH ERASE EOL
002740       DISPLAY "                                         "
002750                    line 17 COL 01 WITH ERASE EOL
002760       DISPLAY "                                         "
002770                    line 18 COL 01 WITH ERASE EOL.
002780
002790  EX-GOOD-BYE.
002800      EXIT.
002810
002820  ESEGUI.
002830
002840*
002850* TPD-ACTION CAN ASSUME:
002860*       S=STEP BY STEP (DEFAULT VALUE)
002870*       A=ANIMATE (FAST RUN...)
002880*       F=FAST
002890*
002900
002910      IF TPD-ACTION = SPACES
002920       MOVE "S"         TO TPD-ACTION.
002930
002940  INIZIO-LOG.
002950
002960      IF TPD-ACTION  = "F"
002970       MOVE PROG-LIN TO CHIAVE-LIN SETTA-QUESTO
002980*      DISPLAY " Executing line:"
002990*                       LINE 01 COL 55 WITH REVERSE-VIDEO
003000*      DISPLAY PROG-LIN LINE 01 COL 73 WITH REVERSE-VIDEO
003010        ELSE
003020      MOVE PROG-LIN              TO SETTA-QUESTO
003030      PERFORM PAGINA             THRU EX-PAGINA.
003040
003050      IF SETTA-QUESTO = SPACES GO TO END-ESEGUI.
003060
003070      MOVE SETTA-QUESTO          TO CHIAVE-LIN.
003080*
003090* the only valid motive to have a invalid key here is that a page next
003100* out bound the max pages available for the source
003110*
003120      READ ARK-LIN KEY IS CHIAVE-LIN
003130       INVALID KEY
003140           GO TO END-ESEGUI.
003150
003160      IF TPD-ACTION = "F"
003170       AND BREAK-LIN NOT = "S"
003180        GO TO END-ESEGUI.
003190
003200
003210      IF TPD-ACTION = "A"
003220       AND BREAK-LIN NOT = "S"
003230      DISPLAY COMANDI1
003240        LINE 25 COL 2 WITH REVERSE-VIDEO
003250        CALL "CBL_OC_NANOSLEEP" USING SECONDI
003260        GO TO END-ESEGUI.
003270
003280      IF ( TPD-ACTION = "A" OR "F" )
003290       AND BREAK-LIN = "S"
003300        MOVE "S" TO TPD-ACTION
003310        MOVE " " TO BREAK-LIN
003320        PERFORM VIS-COMANDI THRU EX-VIS-COMANDI
003330
003340        REWRITE REC-LIN
003350       DISPLAY DATI-LIN(7:66)
003360         LINE RIGA-LIN COL 9 WITH REVERSE-VIDEO
003370       DISPLAY "Reached break at"   LINE 01 COL 55
003380                        WITH REVERSE-VIDEO
003390       DISPLAY   CHIAVE-LIN         LINE 01 COL 72 WITH
003400                        REVERSE-VIDEO BEEP
003410      PERFORM PAGINA    THRU EX-PAGINA.
003420
003430      DISPLAY "<"   LINE ROW-PERC-LIN COL 80
003440           WITH BACKGROUND-COLOR REM-COLOR-BACK
003450            FOREGROUND-COLOR REM-COLOR-FORE
003460
003470      ACCEPT XX      LINE 25 COL 79  WITH SECURE AUTO.
003480
003490      MOVE FUNCTION UPPER-CASE(XX) TO XX.
003500
003510      PERFORM VIS-LINE           THRU EX-VIS-LINE.
003520
003530      IF XX = DO-ESCAPE
003540        MOVE "Z" TO TPD-SW-ANIM
003550        PERFORM CHIUSURE THRU EX-CHIUSURE
003560        STOP RUN.
003570
003580      IF  XX = SET-BREAK
003590       AND BREAK-LIN NOT = "S"
003600       AND DATI-LIN(7:1) NOT = "*"
003610       AND TIPO-LIN = "I"
003620       MOVE "S" TO BREAK-LIN
003630       REWRITE REC-LIN
003640        GO TO INIZIO-LOG.
003650
003660      IF  XX = SET-BREAK
003670       AND BREAK-LIN = "S"
003680       MOVE " " TO BREAK-LIN
003690       REWRITE REC-LIN
003700        GO TO INIZIO-LOG.
003710
003720
003730      IF XX = DO-GO
003740       MOVE "F" TO TPD-ACTION.
003750
003760      IF XX = DOES-ANIM
003770       MOVE "A" TO TPD-ACTION.
003780
003790
003800      IF COB-CRT-STATUS = LINE-NEXT
003810       READ ARK-LIN NEXT RECORD AT END GO TO END-ESEGUI
003820       END-READ
003830       MOVE CHIAVE-LIN TO PROG-LIN
003840       GO TO INIZIO-LOG.
003850
003860      IF COB-CRT-STATUS = LINE-PREV
003870       READ ARK-LIN PREVIOUS RECORD AT END GO TO END-ESEGUI
003880       END-READ
003890       MOVE CHIAVE-LIN TO PROG-LIN
003900       GO TO INIZIO-LOG.
003910
003920      IF COB-CRT-STATUS = PAG-NEXT
003930       ADD  1          TO PAG-LIN
003940       MOVE CHIAVE-LIN TO PROG-LIN
003950       GO TO INIZIO-LOG.
003960
003970      IF COB-CRT-STATUS = PAG-HOME
003980       MOVE 1          TO PAG-LIN
003990       MOVE CHIAVE-LIN TO PROG-LIN
004000       GO TO INIZIO-LOG.
004010
004020      IF COB-CRT-STATUS = PAG-END
004030       MOVE 9999       TO PAG-LIN
004040       START ARK-LIN KEY IS NOT > CHIAVE-LIN
004050        INVALID KEY
004060        MOVE 1          TO PAG-LIN
004070        MOVE CHIAVE-LIN TO PROG-LIN
004080        GO TO INIZIO-LOG
004090        END-START
004100       READ ARK-LIN PREVIOUS RECORD AT END
004110        CONTINUE
004120       END-READ
004130        MOVE CHIAVE-LIN TO PROG-LIN
004140        GO TO INIZIO-LOG.
004150
004160
004170
004180      IF COB-CRT-STATUS = PAG-PREV
004190       AND PAG-LIN NOT = 1
004200       SUBTRACT 1 FROM PAG-LIN
004210       MOVE CHIAVE-LIN TO PROG-LIN
004220       GO TO INIZIO-LOG.
004230
004240       IF XX = DO-FIND
004250        MOVE "N"       TO TPD-CONTINUE
004260        MOVE PROG-LIN  TO TPD-LINE
004270        CALL TPD-FIND  USING FILE-LIN
004280        MOVE TPD-LINE  TO PROG-LIN
004290         GO TO INIZIO-LOG.
004300
004310       IF XX = DO-CONTINUE
004320        AND TPD-SEARCH > SPACES
004330        MOVE PROG-LIN  TO TPD-LINE
004340        MOVE "Y"       TO TPD-CONTINUE
004350        CALL TPD-FIND  USING FILE-LIN
004360        MOVE TPD-LINE  TO PROG-LIN
004370         GO TO INIZIO-LOG.
004380
004390       IF XX = DO-STRUCTURE
004400        MOVE PROG-LIN   TO TPD-LINE
004410        CALL TPD-LOCATE USING FILE-LIN
004420        CANCEL TPD-LOCATE
004430        MOVE PROG-LIN   TO PROG-LIN
004440         GO TO INIZIO-LOG.
004450
004460       IF XX = DO-VIEW
004470        MOVE PROG-LIN  TO TPD-LINE
004480        MOVE "D" TO TPD-SW-ANIM
004490         GO TO  END-ESEGUI.
004500
004510       IF XX = DOES-WATCH
004520        MOVE PROG-LIN  TO TPD-LINE
004530        MOVE "W" TO TPD-SW-ANIM
004540         GO TO  END-ESEGUI.
004550
004560       IF XX = STEP1      GO TO END-ESEGUI.
004570       IF XX = STEP2      GO TO END-ESEGUI.
004580       IF XX = STEP3      GO TO END-ESEGUI.
004590       IF XX = DOES-ANIM  GO TO END-ESEGUI.
004600       IF XX = DO-GO      GO TO END-ESEGUI.
004610
004620       GO TO INIZIO-LOG.
004630
004640  END-ESEGUI.
004650*
004660* check to evaluate CONDITIONAL BREAK occurred
004670*
004680* if LINE-FIELD = PROG-LINE AND LINE-FILED > SPACE
004690* then the break is occurred at the
004700* current line (break at cursor) and NOT global
004710*
004720*
004730       MOVE "1"        TO BREAK-FIELD.
004740       START ARK-FIELD KEY IS NOT < BREAK-FIELD
004750        INVALID KEY GO TO EX-ESEGUI.
004760
004770       MOVE SPACES     TO EOF-FIELD.
004780
004790       PERFORM UNTIL EOF-FIELD = "S"
004800
004810         READ ARK-FIELD NEXT RECORD
004820          AT END MOVE "S" TO EOF-FIELD
004830         END-READ
004840
004850         IF EOF-FIELD NOT = "S"
004860          AND BREAK-FIELD NOT = SPACES
004870          AND ADDRESS-FIELD > NULL
004880
004890*
004900**** check for field changes
004910*
004920          MOVE ADDRESS-FIELD TO ADDRESS OF DATA-WORK
004930
004940          MOVE SIZE-FIELD    TO MAX
004950
004960          IF MAX > LENGTH OF VALUE-FIELD
004970           MOVE LENGTH OF VALUE-FIELD  TO MAX
004980          END-IF
004990
005000          PERFORM VALUTA-BREAK THRU EX-VALUTA-BREAK
005010
005020          IF DO-BREAK = "S"
005030            PERFORM GENERATE-A-BREAK THRU EX-GENERATE-A-BREAK
005040            GO TO INIZIO-LOG
005050          END-IF
005060
005070         END-IF
005080
005090       END-PERFORM.
005100
005110  EX-ESEGUI.
005120       EXIT.
005130
005140  VALUTA-BREAK.
005150
005160       MOVE "N"       TO DO-BREAK.
005170
005180          EVALUATE BREAK-FIELD
005190
005200           WHEN "1"
005210
005220            IF DATA-WORK(1:SIZE-FIELD)
005230             EQUAL  TO VALUE-BREAK-FIELD(1:SIZE-FIELD)
005240             AND LINE-FIELD = SPACES
005250             MOVE "S" TO DO-BREAK
005260            END-IF
005270
005280            IF DATA-WORK(1:SIZE-FIELD)
005290             EQUAL  TO VALUE-BREAK-FIELD(1:SIZE-FIELD)
005300             AND LINE-FIELD = PROG-LIN
005310             MOVE "S" TO DO-BREAK
005320            END-IF
005330
005340           WHEN "2"
005350
005360            IF DATA-WORK(1:SIZE-FIELD)
005370             LESS THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005380             AND LINE-FIELD = SPACES
005390             MOVE "S" TO DO-BREAK
005400            END-IF
005410
005420            IF DATA-WORK(1:SIZE-FIELD)
005430             LESS THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005440             AND LINE-FIELD = PROG-LIN
005450             MOVE "S" TO DO-BREAK
005460            END-IF
005470
005480           WHEN "3"
005490
005500            IF DATA-WORK(1:SIZE-FIELD)
005510             GREATER THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005520             AND LINE-FIELD = SPACES
005530             MOVE "S" TO DO-BREAK
005540            END-IF
005550
005560            IF DATA-WORK(1:SIZE-FIELD)
005570             GREATER THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005580             AND LINE-FIELD = PROG-LIN
005590             MOVE "S" TO DO-BREAK
005600            END-IF
005610
005620           WHEN "4"
005630
005640            IF DATA-WORK(1:SIZE-FIELD)
005650             NOT GREATER THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005660             AND LINE-FIELD = SPACES
005670             MOVE "S" TO DO-BREAK
005680            END-IF
005690
005700            IF DATA-WORK(1:SIZE-FIELD)
005710             NOT GREATER THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005720             AND LINE-FIELD = PROG-LIN
005730             MOVE "S" TO DO-BREAK
005740            END-IF
005750
005760           WHEN "5"
005770
005780            IF DATA-WORK(1:SIZE-FIELD)
005790             NOT LESS THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005800             AND LINE-FIELD = SPACES
005810             MOVE "S" TO DO-BREAK
005820            END-IF
005830
005840            IF DATA-WORK(1:SIZE-FIELD)
005850             NOT LESS THAN VALUE-BREAK-FIELD(1:SIZE-FIELD)
005860             AND LINE-FIELD = PROG-LIN
005870             MOVE "S" TO DO-BREAK
005880            END-IF
005890
005900           WHEN "6"
005910
005920           IF DATA-WORK(1:SIZE-FIELD)
005930            NOT EQUAL VALUE-FIELD(1:SIZE-FIELD)
005940            AND LINE-FIELD = SPACES
005950             MOVE "S" TO DO-BREAK
005960           END-IF
005970
005980           IF DATA-WORK(1:SIZE-FIELD)
005990            NOT EQUAL VALUE-FIELD(1:SIZE-FIELD)
006000            AND LINE-FIELD = PROG-LIN
006010             MOVE "S" TO DO-BREAK
006020           END-IF
006030
006040          END-EVALUATE.
006050
006060  EX-VALUTA-BREAK.
006070          EXIT.
006080
006090  GENERATE-A-BREAK.
006100
006110       PERFORM VIS-BREAK     THRU EX-VIS-BREAK.
006120       MOVE "S"              TO BREAK-LIN.
006130       REWRITE REC-LIN.
006140       MOVE "S"              TO TPD-ACTION.
006150
006160       IF BREAK-IF-CHANGE
006170        MOVE DATA-WORK(1:SIZE-FIELD) TO VALUE-FIELD(1:)
006180        ELSE
006190        MOVE SPACES           TO BREAK-FIELD
006200        MOVE SPACES           TO VALUE-BREAK-FIELD
006210       END-IF
006220
006230       REWRITE REC-FIELD.
006240
006250  EX-GENERATE-A-BREAK.
006260       EXIT.
006270
006280  VIS-BREAK.
006290
006300       MOVE ALL LO               TO RIGA-VIDEO.
006310       MOVE A1                   TO RIGA-VIDEO(1:1)
006320       MOVE A3                   TO RIGA-VIDEO(48:1)
006330
006340       DISPLAY RIGA-VIDEO        LINE 09 POSITION 19
006350         WITH FOREGROUND-COLOR COB-COLOR-CYAN
006360               BACKGROUND-COLOR COB-COLOR-BLACK.
006370
006380       MOVE ALL SPACES           TO RIGA-VIDEO.
006390       MOVE LV                   TO RIGA-VIDEO(1:1)
006400       MOVE LV                   TO RIGA-VIDEO(48:1)
006410       PERFORM VARYING IND FROM 10 BY 1 UNTIL IND > 12
006420        DISPLAY RIGA-VIDEO LINE IND POSITION 19
006430         WITH FOREGROUND-COLOR COB-COLOR-CYAN
006440               BACKGROUND-COLOR COB-COLOR-BLACK
006450        END-DISPLAY
006460       END-PERFORM .
006470
006480       MOVE ALL LO               TO RIGA-VIDEO.
006490       MOVE A2                   TO RIGA-VIDEO(1:1)
006500       MOVE A4                   TO RIGA-VIDEO(48:1)
006510
006520       DISPLAY RIGA-VIDEO        LINE 13 POSITION 19
006530         WITH FOREGROUND-COLOR COB-COLOR-CYAN
006540               BACKGROUND-COLOR COB-COLOR-BLACK.
006550
006560       IF LINE-FIELD = SPACES
006570       DISPLAY "CONDITIONAL BREAKPOINT REACHED "
006580             LINE 09 POSITION  21
006590         WITH FOREGROUND-COLOR COB-COLOR-CYAN   BEEP
006600               BACKGROUND-COLOR COB-COLOR-BLACK.
006610
006620       IF LINE-FIELD > SPACES
006630       DISPLAY "AT LINE "
006640             LINE 09 POSITION  21
006650         WITH FOREGROUND-COLOR COB-COLOR-CYAN   BEEP
006660               BACKGROUND-COLOR COB-COLOR-BLACK
006670       DISPLAY LINE-FIELD
006680             LINE 09 POSITION  29
006690         WITH FOREGROUND-COLOR COB-COLOR-CYAN   BEEP
006700               BACKGROUND-COLOR COB-COLOR-BLACK
006710       DISPLAY
006720             "CONDITIONAL BREAKPOINT REACHED "
006730             LINE 09 POSITION  36
006740         WITH FOREGROUND-COLOR COB-COLOR-CYAN   BEEP
006750               BACKGROUND-COLOR COB-COLOR-BLACK.
006760
006770
006780       DISPLAY "field "
006790             LINE 10 POSITION  21
006800         WITH FOREGROUND-COLOR COB-COLOR-YELLOW
006810               BACKGROUND-COLOR COB-COLOR-BLACK.
006820
006830       DISPLAY NOME-FIELD LINE 10 POSITION 27
006840         WITH FOREGROUND-COLOR COB-COLOR-WHITE
006850               BACKGROUND-COLOR COB-COLOR-BLACK.
006860
006870       DISPLAY " from"
006880             LINE 11 POSITION  21
006890         WITH FOREGROUND-COLOR COB-COLOR-YELLOW
006900               BACKGROUND-COLOR COB-COLOR-BLACK.
006910
006920       IF SIZE-FIELD > 48
006930        MOVE 47                  TO SIZE-FIELD.
006940
006950       DISPLAY VALUE-FIELD(1:SIZE-FIELD) LINE 11 POSITION 27
006960         WITH FOREGROUND-COLOR COB-COLOR-WHITE
006970               BACKGROUND-COLOR COB-COLOR-BLACK.
006980
006990       DISPLAY "   to"
007000             LINE 12 POSITION  21
007010         WITH FOREGROUND-COLOR COB-COLOR-YELLOW
007020               BACKGROUND-COLOR COB-COLOR-BLACK.
007030
007040
007050       DISPLAY DATA-WORK(1:SIZE-FIELD) LINE 12 POSITION 27
007060         WITH FOREGROUND-COLOR COB-COLOR-WHITE
007070               BACKGROUND-COLOR COB-COLOR-BLACK.
007080
007090       DISPLAY " press any key to continue .. "
007100             LINE 13 POSITION  21
007110         WITH FOREGROUND-COLOR COB-COLOR-CYAN
007120               BACKGROUND-COLOR COB-COLOR-BLACK.
007130
007140       ACCEPT XX LINE 25 POSITION 79 WITH AUTO.
007150       MOVE SPACES TO XX.
007160
007170  EX-VIS-BREAK.
007180       EXIT.
007190
007200  PAGINA.
007210
007220      DISPLAY " " LINE 1 COL 1 WITH ERASE EOS.
007230
007240      DISPLAY BIANCO LINE 1  COL 1 WITH REVERSE-VIDEO.
007250
007260      DISPLAY
007270       TPD-COPYRIGHT
007280        LINE 1 COL 2 WITH REVERSE-VIDEO ERASE EOL
007290             BACKGROUND-COLOR COB-COLOR-BLUE
007300             FOREGROUND-COLOR COB-COLOR-WHITE.
007310
007320
007330      PERFORM VARYING IND FROM 2 BY 1 UNTIL IND > 24
007340
007350      DISPLAY " "    LINE IND COL 1  WITH REVERSE-VIDEO
007360      DISPLAY BIANCO(1:78) LINE IND COL 2 WITH LOWLIGHT
007370*     DISPLAY " "    LINE IND COL 80 WITH REVERSE-VIDEO
007380      DISPLAY " "    LINE IND COL 80 WITH LOWLIGHT
007390
007400      END-PERFORM.
007410
007420      DISPLAY BIANCO LINE 25 COL 1 WITH REVERSE-VIDEO.
007440
007450      PERFORM VIS-COMANDI THRU EX-VIS-COMANDI.
007460
007470      INITIALIZE REC-LIN.
007480
007490      MOVE PROG-LIN    TO CHIAVE-LIN.
007500      MOVE ZEROS       TO RIGA-LIN.
007510
007520      START ARK-LIN  KEY IS NOT < CHIAVE-LIN
007530       INVALID KEY GO TO FINE-ESEGUI.
007540
007550  CICLO-ESEGUI.
007560
007570       READ ARK-LIN NEXT RECORD AT END GO TO FINE-ESEGUI.
007580
007590       IF PAG-LIN NOT = PROG-LIN(1:4) GO TO FINE-ESEGUI.
007600
007610       IF CHIAVE-LIN NOT = PROG-LIN
007620        AND TIPO-LIN = "D" GO TO CICLO-ESEGUI.
007630
007640
007650       DISPLAY " "       LINE RIGA-LIN COL 2  WITH HIGHLIGHT
007660
007670       IF CHIAVE-LIN = WHERE-ARE
007680       DISPLAY ">"       LINE RIGA-LIN COL 2  WITH HIGHLIGHT
007690       END-IF
007700
007710       IF BREAK-LIN = "S"
007720       DISPLAY "Break"   LINE RIGA-LIN COL 75 WITH HIGHLIGHT
007730       ELSE
007740       DISPLAY "     "   LINE RIGA-LIN COL 75.
007750
007760
007770       IF CHIAVE-LIN = PROG-LIN
007780       MOVE CHIAVE-LIN TO SETTA-QUESTO
007790
007800        IF COB-CRT-STATUS = PAG-NEXT
007810         OR = PAG-PREV
007820         OR = LINE-NEXT
007830         OR = LINE-PREV
007840         DISPLAY " "       LINE RIGA-LIN COL 2  WITH HIGHLIGHT
007850        END-IF
007860
007870
007880       DISPLAY CHIAVE-LIN(1:06)
007890         LINE RIGA-LIN COL 3 WITH LOWLIGHT
007900
007910       DISPLAY DATI-LIN(7:66)
007920         LINE RIGA-LIN COL 9 WITH REVERSE-VIDEO
007930
007940       ELSE
007950
007960        PERFORM VIS-LINE   THRU EX-VIS-LINE.
007970
007980       GO TO CICLO-ESEGUI.
007990
008000  FINE-ESEGUI.
008010
008020      PERFORM VIS-COMANDI THRU EX-VIS-COMANDI.
008030
008040  EX-PAGINA.
008050      EXIT.
008060
008070  VIS-LINE.
008080
008090       MOVE SPACES TO A B C D.
008100
008110       UNSTRING DATI-LIN(8:) DELIMITED BY ALL SPACES
008120        INTO A B C D.
008130
008140         IF A = SPACES
008150          AND B > SPACES
008160          AND C = SPACES
008170          AND D = SPACES
008180          AND DATI-LIN(7:1) NOT = "*"
008190          AND TIPO-LIN NOT = "I"
008200          PERFORM VARYING IND FROM 8 BY 1 UNTIL IND > 72
008210          OR DATI-LIN(IND:1) > SPACES
008220          CONTINUE
008230          END-PERFORM
008240
008250          PERFORM VARYING IND FROM IND BY 1 UNTIL IND > 72
008260          OR DATI-LIN(IND:1) = SPACES
008270
008280          COMPUTE IND1 = IND + 2
008290          DISPLAY DATI-LIN(IND:1) LINE RIGA-LIN COL IND1
008300           WITH BACKGROUND-COLOR LAB-COLOR-BACK
008310             FOREGROUND-COLOR LAB-COLOR-FORE
008320
008330
008340          END-PERFORM
008350
008360          GO TO EX-VIS-LINE
008370
008380         END-IF.
008390
008400
008410       DISPLAY CHIAVE-LIN(1:06)
008420         LINE RIGA-LIN COL 3 WITH LOWLIGHT.
008430
008440       IF TIPO-LIN = "I"
008450        DISPLAY DATI-LIN(7:66) LINE RIGA-LIN COL 9
008460        WITH BACKGROUND-COLOR ACC-COLOR-BACK
008470             FOREGROUND-COLOR ACC-COLOR-FORE
008480        ELSE
008490*
008500*** a remark line
008510*
008520         IF DATI-LIN(7:1) = "*"
008530          DISPLAY DATI-LIN(7:66) LINE RIGA-LIN COL 9
008540           WITH BACKGROUND-COLOR REM-COLOR-BACK
008550            FOREGROUND-COLOR REM-COLOR-FORE
008560             ELSE
008570
008580          IF CODICE-LIN = "S"
008590          DISPLAY DATI-LIN(7:66) LINE RIGA-LIN COL 9
008600            WITH BACKGROUND-COLOR NUL-COLOR-BACK
008610                  FOREGROUND-COLOR NUL-COLOR-FORE
008620                  ELSE
008630          DISPLAY DATI-LIN(7:66)
008640            LINE RIGA-LIN COL 9
008650              WITH FOREGROUND-COLOR WOK-COLOR-FORE
008660                 BACKGROUND-COLOR WOK-COLOR-BACK.
008670
008680
008690  EX-VIS-LINE.
008700      EXIT.
008710
008720
008730  VIS-COMANDI.
008740
008750      DISPLAY COMANDI  LINE 25 COL 2  WITH REVERSE-VIDEO.
008760
008770      DISPLAY "N"      LINE 25 COL 2  WITH REVERSE-VIDEO
008780                        FOREGROUND-COLOR COB-COLOR-GREEN
008790                        BACKGROUND-COLOR COB-COLOR-BLUE.
008800
008810      DISPLAY "B"      LINE 25 COL 7    WITH REVERSE-VIDEO
008820                        FOREGROUND-COLOR COB-COLOR-GREEN
008830                        BACKGROUND-COLOR COB-COLOR-BLUE.
008840
008850      DISPLAY "G"      LINE 25 COL 13   WITH REVERSE-VIDEO
008860                        FOREGROUND-COLOR COB-COLOR-GREEN
008870                        BACKGROUND-COLOR COB-COLOR-BLUE.
008880
008890      DISPLAY "A"      LINE 25 COL 21   WITH REVERSE-VIDEO
008900                        FOREGROUND-COLOR COB-COLOR-GREEN
008910                        BACKGROUND-COLOR COB-COLOR-BLUE.
008920
008930
008940      DISPLAY "V"      LINE 25 COL 29   WITH REVERSE-VIDEO
008950                        FOREGROUND-COLOR COB-COLOR-GREEN
008960                        BACKGROUND-COLOR COB-COLOR-BLUE.
008970
008980      DISPLAY "X"      LINE 25 COL 40   WITH REVERSE-VIDEO
008990                        FOREGROUND-COLOR COB-COLOR-GREEN
009000                        BACKGROUND-COLOR COB-COLOR-BLUE.
009010
009020      DISPLAY "F"      LINE 25 COL 47   WITH REVERSE-VIDEO
009030                        FOREGROUND-COLOR COB-COLOR-GREEN
009040                        BACKGROUND-COLOR COB-COLOR-BLUE.
009050
009060      DISPLAY "C"      LINE 25 COL 52   WITH REVERSE-VIDEO
009070                        FOREGROUND-COLOR COB-COLOR-GREEN
009080                        BACKGROUND-COLOR COB-COLOR-BLUE.
009090
009100      DISPLAY "W"      LINE 25 COL 66   WITH REVERSE-VIDEO
009110                        FOREGROUND-COLOR COB-COLOR-GREEN
009120                        BACKGROUND-COLOR COB-COLOR-BLUE.
009130
009140      DISPLAY "S"      LINE 25 COL 72   WITH REVERSE-VIDEO
009150                        FOREGROUND-COLOR COB-COLOR-GREEN
009160                        BACKGROUND-COLOR COB-COLOR-BLUE.
009170
009180  EX-VIS-COMANDI.
009190      EXIT.
009200  end program animate.
009210
009220
