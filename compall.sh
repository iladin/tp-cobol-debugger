#!/bin/bash
clear

echo Compiling the Animator
cobc -x animator.cbl
cobc animate.cbl
cobc animdata.cbl
cobc analyzer.cbl
cobc animfind.cbl
cobc animview.cbl


if test "$1" = "install"; then

   echo Installing the Animator
   export animtarget=/opt/animator

   mkdir -p /opt/animator

   cp -f animator $animtarget
   cp -f animate.so $animtarget
   cp -f animdata.so $animtarget
   cp -f analyzer.so $animtarget
   cp -f animfind.so $animtarget
   cp -f animview.so $animtarget

   export cbl_library_path=$animtarget
   #export animate.inf=$animtarget
   #export animator.inf=$animtarget

   export PATH=$animtarget:$PATH
else
   export PATH=.:$PATH
fi

echo Testing the Animator
animator test.cbl -x
./test
cat telco.txt

