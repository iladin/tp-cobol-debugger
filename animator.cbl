        IDENTIFICATION DIVISION.
        PROGRAM-ID.    animator.
      *---------------------------------------------------------------------
      *
      * DEBUGGER interattivo for GNUCOBOL
      *
      * LAST  04 JANNUARY 2015   2.1.0.000
      *       08 OCTOBER  2014   2.0.0.000 NEW 2.0 SUPPORT FOR THE COMPILER
      *       14 APRIL 2012      0.1.9.008 HAPPY BIRTHDAY FEDERICO
      *       08 APRIL 2012      0.1.9.007 TWO YEARS LATER
      *       15 NOVEMBER        0.1.9.006
      *       30 OCTOBER         0.1.9.005
      *       01 OCTOBER         0.1.9.004
      *       18 SEPTEMBER       0.1.9.003
      *       07 AUGUST          0.1.9.002
      *       26 JULY            0.1.9.001
      *       22 JULY            0.1.9
      *       13 MARCH  2011     0.1.8 ONE YEAR LATER
      *       11 JULY            0.1.7
      *       04 APRIL           0.1.6
      *       25 MARCH           0.1.5
      *       24 MARCH           0.1.4
      *       21 MARCH           0.1.3
      *       17 MARCH           0.1.2
      *       15 MARCH           0.1.1
      * FIRST 13 MARCH  2010     0.1.0
      *
      * Copyright (C) 2010-2014 Federico Priolo T&P Soluzioni Informatiche Srl
      *                         federico.priolo@tp-srl.it
      *
      * This program is free software; you can redistribute it and/or modify
      * it under the terms of the GNU General Public License as published by
      * the Free Software Foundation; either version 2, or (at your option)
      * any later version.
      *
      * This program is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU General Public License for more details.
      *
      * You should have received a copy of the GNU General Public License
      * along with this software; see the file COPYING.  If not, write to
      * the Free Software Foundation, 51 Franklin Street, Fifth Floor
      * Boston, MA 02110-1301 USA
      *
      *
      *---------------------------------------------------------------------
        ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
        SOURCE-COMPUTER.        PC-IBM.
        OBJECT-COMPUTER.        PC-IBM.
        SPECIAL-NAMES.

           DECIMAL-POINT IS COMMA.

        INPUT-OUTPUT SECTION.
        FILE-CONTROL.

                SELECT ARK-IN ASSIGN TO  FILE-IN
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-IN.

                SELECT ARK-DO ASSIGN TO  FILE-DO
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-DO.

                SELECT ARK-OUT ASSIGN TO FILE-OUT
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-OUT.

                SELECT ARK-VAR ASSIGN TO FILE-VAR
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-VAR.

                SELECT ARK-FIELD ASSIGN TO FILE-FIELD
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS  DYNAMIC
                 RECORD KEY   IS  CHIAVE-FIELD
                 ALTERNATE RECORD KEY IS BREAK-FIELD WITH DUPLICATES
                 FILE STATUS  IS STATUS-FIELD.

                SELECT ARK-CPY ASSIGN TO FILE-CPY
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-CPY.

                SELECT ARK-LIN ASSIGN TO FILE-LIN
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS  DYNAMIC
                 RECORD KEY   IS  CHIAVE-LIN
                 FILE STATUS  IS STATUS-LIN.



        DATA DIVISION.
        FILE SECTION.

        FD ARK-LIN.
        01 REC-LIN.
         02 CHIAVE-LIN.
          05 PAG-LIN                  PIC 9(4).
          05 RIGA-LIN                 PIC 9(2).
         02 DATI-LIN                  PIC X(80).
         02 TIPO-LIN                  PIC X.
         02 BREAK-LIN                 PIC X.
         02 CODICE-LIN                PIC X.
         02 ROW-PERC-LIN              PIC 9(2).
         02 FILLER                    PIC X(17).

        FD ARK-FIELD.
        01 REC-FIELD.
         02 CHIAVE-FIELD.
            09 NOME-FIELD             PIC X(30).
         02 TIPO-FIELD                PIC X(01).
         02 VALUE-FIELD               PIC X(256).
         02 SIZE-FIELD                PIC 9(9) COMP-5.
         02 BREAK-FIELD               PIC X.
           88 BREAK-ON-CHANGE         VALUE "X".
         02 TEST-FIELD                PIC X.
           88 LESS-THEN               VALUE "L".
           88 EQUAL-TO                VALUE "E".
           88 MAJOR-OF                VALUE "M".
         02 NOT-FIELD                 PIC X.
           88 NOT-OF                  VALUE "N".
         02 VALUE-BREAK-FIELD         PIC X(256).
         02 KEY-FIELD-FIELD           PIC X(30).
         02 ADDRESS-FIELD             USAGE POINTER.
         02 OCC-FIELD                 PIC XX.
         02 TIMES-FIELD               PIC 99.
         02 FILLER-FIELD              PIC X(52).


        FD ARK-IN.
        01 REC-IN.
           02 DATI-IN                 PIC X(256).

        FD ARK-VAR.
        01 REC-VAR.
           02 DATI-VAR                PIC X(256).

        FD ARK-CPY.
        01 REC-CPY.
           02 DATI-CPY                PIC X(128).

        FD ARK-DO.
        01 REC-DO.
           02 DATI-DO                 PIC X(512).

        FD ARK-OUT.
        01 REC-OUT.
           02 DATI-OUT                PIC X(256).

        WORKING-STORAGE SECTION.
        77 XX                PIC X VALUE SPACE.

           COPY "screenio.cpy".

        01 PARAMETRI.
         07 WOFFSET                   PIC X(9)   VALUE SPACE.
         07 TIME-SYS                  PIC 9(6)   VALUE ZEROS.
         07 FILE-SYS                  PIC X(6)   VALUE SPACE.
         07 ISTRUZIONE                PIC X(30)  VALUE SPACE.
             88 COBOL-WATCH VALUE "MOVE"
                                  "ADD"
                                  "SUBTRACT"
                                  "IF"
                                  "DIVIDE"
                                  "INITIALIZE"
                                  "REWRITE"
                                  "WRITE"
                                  "COMPUTE".

         07 WATCH-TABLE.
           09 WATCH-NAME              PIC X(60)  OCCURS 10 TIMES.
         07 WATCH-PREVIOUS            PIC X(60)  VALUE SPACE.
         07 WATCH-ITEM                PIC X(60)  VALUE SPACE.
         07 WATCH-ITEMS               PIC 99     VALUE ZEROS.
         07 WATCH-REC                 PIC X(256) VALUE SPACE.
         07 WATCH-INSIDE              PIC X(60)  VALUE SPACE.

         07 END-POINT                 PIC X(30)  VALUE SPACE.
         07 COBC-INVOKE               PIC X(512) VALUE SPACE.
         07 ANALYZER-INVOKE           PIC X(512) VALUE SPACE.
         07 ANALYZER-VIEW             PIC X(512) VALUE SPACE.
         07 BASE-NODE                 PIC X(512) VALUE SPACE.
         07 DEEP-NODE                 PIC X(512) VALUE SPACE.
         07 PARAMETRO                 PIC X(30)  VALUE SPACE.
         07 VALORE                    PIC X(512) VALUE SPACE.
         07 FINE-FILE                 PIC X      VALUE SPACE.
         07 FILE-FIELD                PIC X(200) VALUE SPACE.
         07 FILE-CPY                  PIC X(200) VALUE SPACE.
         07 FILE-DO                   PIC X(200) VALUE SPACE.
         07 FILE-IN                   PIC X(512) VALUE SPACE.
         07 FILE-LIN                  PIC X(200) VALUE SPACE.
         07 FILE-LOG                  PIC X(200) VALUE SPACE.
         07 FILE-OUT                  PIC X(200) VALUE SPACE.
         07 FILE-VAR                  PIC X(200) VALUE SPACE.
         07 FILE-COPY                 PIC X(200) VALUE SPACE.
         07 STATUS-FLOW               PIC XX     VALUE SPACE.
         07 STATUS-CPY                PIC XX     VALUE SPACE.
         07 STATUS-DO                 PIC XX     VALUE SPACE.
         07 STATUS-IN                 PIC XX     VALUE SPACE.
         07 STATUS-LIN                PIC XX     VALUE SPACE.
         07 STATUS-OUT                PIC XX     VALUE SPACE.
         07 STATUS-VAR                PIC XX     VALUE SPACE.
         07 STATUS-FIELD              PIC XX     VALUE SPACE.
         07 STATUS-STACK              PIC XX     VALUE SPACE.
         07 POSIZIONE.
          17 PAGINA                   PIC 9(4)   VALUE ZEROS.
          17 CONTA                    PIC 9(2)   VALUE ZEROS.
         07 CONTA-DEBUG               PIC 9(2)   VALUE ZEROS.
         07 CONTA-LINE                PIC 9(6)   VALUE ZEROS.
         07 THIS-LINE                 PIC 9(6)   VALUE ZEROS.
         07 IND                       PIC 9(9)   VALUE ZEROS.
         07 IND1                      PIC 9(9)   VALUE ZEROS.
         07 IND2                      PIC 9(9)   VALUE ZEROS.
         07 IND3                      PIC 9(9)   VALUE ZEROS.
         07 IND5                      PIC 9(2)   VALUE ZEROS.
         07 VERBO-COBOL               PIC XX     VALUE SPACE.
         07 STRINGA                   PIC X(256) VALUE SPACE.
         07 TIPO-RIGA                 PIC X      VALUE SPACE.
         07 NOME-LABEL                PIC X(30)  VALUE SPACE.
         07 TAB-ALTRO.
           19 ALTRO                   PIC X(30)  OCCURS 10 TIMES.
         07 DA-PERFORM                PIC X(30)  VALUE SPACE.
         07 A-PERFORM                 PIC X(30)  VALUE SPACE.
         07 START-PERFORM             PIC X(30)  VALUE SPACE.
         07 EXIT-PERFORM              PIC X(30)  VALUE SPACE.
         07 IND-FREE                  PIC 999    VALUE ZEROS.
         07 CHIUSO                    PIC XX     VALUE SPACE.
         07 COMANDO                   PIC X(1024) VALUE SPACES.
         07 COPY1                     PIC X(100) VALUE SPACE.
         07 COPY2                     PIC X(100) VALUE SPACE.
         07 DO-DLL                    PIC XX     VALUE SPACE.
         07 START-CYCLE               PIC X(100) VALUE SPACE.
         07 ACTUAL-CYCLE              PIC X(100) VALUE SPACE.
         07 TAB-OPTIONS.
          09 SW-OPTION                PIC X OCCURS 20 TIMES.
         07 OC-OPTIONS                PIC X(512) VALUE SPACE.
         07 OC-CONDITIONS             PIC X(512) VALUE SPACE.
         07 OC-BUFFER                 PIC X(512) VALUE SPACE.
         78 MAXOPZ                    VALUE 20.
         07 SW-NOCOMPILA              PIC X.
         07 SW-SAVE                   PIC X.
         07 SW-DATI                   PIC X.
         07 SW-DATI-OK                PIC X.
         07 SW-HELP                   PIC X.
         07 SW-MANUAL                 PIC X.
         07 SW-CODICE                 PIC X.
         07 SW-FREE                   PIC X.
         07 SW-CONST                  PIC X.
         07 SW-STOP                   PIC X.
         07 SW-EXE                    PIC X.
         07 SW-ANALYSIS               PIC X.
         07 SW-VERBOSE                PIC X.
         07 SW-WHEN                   PIC X.
         07 REM-COLUMN                PIC 9.
         07 CAMPO0                    PIC X.
         07 CAMPO1                    PIC X(30).
         07 CAMPO2.
           09 LIVELLO                 PIC X(10).
             88 LIVELLO-OK            VALUE 01 THRU 77
                                            79 THRU 87.

         07 CAMPO3                    PIC X(30).
         07 CAMPO4                    PIC X(30).
         07 CAMPO5                    PIC X(10).
         07 RIC-LIVELLO               PIC 99.
         07 SECOND-DIMENSION          PIC XX.
         07 INSIDE-OCCURS             PIC XX.
         07 EOF-DO                    PIC X.
         07 AREA-DATI                 PIC X.
         07 TYPE-SYSTEM               PIC X(10).
         07 ESCI                      PIC XX     VALUE SPACE.

         01 FILE-FLOW                 PIC X(200) IS EXTERNAL.
         01 FILE-STACK                PIC X(200) IS EXTERNAL.

                COPY "workanim.cpy".

        01 RIGA-SPAZI                 PIC X(80) VALUE SPACES.

        PROCEDURE DIVISION.
        INIZIO SECTION.


                PERFORM INIZIALI       THRU EX-INIZIALI.

                PERFORM LOGO           THRU EX-LOGO.

                MOVE SPACES            TO FILE-IN.

                ACCEPT FILE-IN FROM COMMAND-LINE.

                IF FILE-IN  = SPACES
                 PERFORM HELP   THRU EX-HELP
                  STOP RUN
                END-IF.



                PERFORM OPZIONI  THRU EX-OPZIONI.

                PERFORM APERTURE THRU EX-APERTURE.

                PERFORM ELABORA  THRU EX-ELABORA UNTIL FINE-FILE = "S".

                PERFORM RINUMERA THRU EX-RINUMERA.

                PERFORM CHIUSURE THRU EX-CHIUSURE.

                IF SW-ANALYSIS > SPACES
                PERFORM ANALYSIS THRU EX-ANALYSIS
                 ELSE
                PERFORM COMPILA  THRU EX-COMPILA.

                GOBACK.

        COMPILA.

      *>**************************** it goes ahead also without GOBACK..
      *         IF SW-DATI = "S"
      *          AND SW-DATI-OK = "1"
      *          DISPLAY "Missing GOBACK/STOP RUN for data evaluating.."
      *           GO TO EX-COMPILA
      *           END-IF.
      *>*****************************************************************

                IF SW-DATI = "S"
                AND SW-DATI-OK = spaces
                DISPLAY "Missing WORKING-STORAGE for data evaluating.."
                 GO TO EX-COMPILA
                  END-IF.

                MOVE SPACES TO STRINGA.

                IF SW-NOCOMPILA = SPACES
                STRING "Done...just compiling the animated "
                FILE-OUT DELIMITED BY "   "
                " source program. " DELIMITED BY SIZE INTO STRINGA
                ELSE
                STRING "Done...please compile the animated "
                FILE-OUT DELIMITED BY "   "
                " source program. " DELIMITED BY SIZE INTO STRINGA.

                IF SW-VERBOSE = "S"
                DISPLAY STRINGA.


                MOVE SPACES     TO COMANDO.

                IF COBC-INVOKE = SPACE
                 ACCEPT COBC-INVOKE FROM ENVIRONMENT "cobc.inf"
                 END-ACCEPT.

                if COBC-INVOKE = spaces
                STRING "cobc " delimited by size
                 DO-DLL DELIMITED BY SPACES
                  " -save-temps -Wall " DELIMITED BY SIZE
                  OC-OPTIONS DELIMITED BY "      "
                 " " DELIMITED BY SIZE
                 FILE-OUT DELIMITED BY  "   " INTO COMANDO
                 else

      *
      *         if set cobc= into animator.inf it looks and replace
      *         with the source program name that it will compile
      *         by searching the $program (lowercase !!) tag
      *

                STRING "cobc " delimited by size
                 DO-DLL DELIMITED BY SPACES
                 " " DELIMITED BY SIZE
                 FUNCTION TRIM(FILE-OUT)
                 " " DELIMITED BY SIZE
                  COBC-INVOKE
                  DELIMITED BY "     " INTO COMANDO.

                IF SW-NOCOMPILA = SPACES

                 IF SW-VERBOSE = "S"
                 DISPLAY "Building.. " FUNCTION TRIM(COMANDO)
                 END-IF


                  CALL "SYSTEM" USING COMANDO.

      *
      * si cancella il file (default) o si lascia se NON COMPILARE..
      *                                              KEEP SOURCE
      *
                IF SW-SAVE = "S" GO TO EX-COMPILA.

                IF SW-NOCOMPILA = "S" GO TO EX-COMPILA.

                CALL "CBL_DELETE_FILE" USING FILE-OUT.



        EX-COMPILA.
                EXIT.

        LETTURA.

                MOVE SPACES            TO REC-IN REC-OUT REC-LIN.

                IF FINE-FILE NOT = "S"
                 READ ARK-IN NEXT AT END MOVE "S" TO FINE-FILE
                 END-IF.

      ** auto exclude copy when you are animating a part of the animator itself

                IF FINE-FILE  NOT = "S"
                 MOVE ZEROS  TO IND3
                 INSPECT REC-IN TALLYING IND3 FOR ALL "workanim.cpy"

                  IF IND3 = 1 GO TO LETTURA END-IF.

      *         END-IF.

                IF FINE-FILE = "S" GO TO EX-LETTURA.

                IF SW-FREE NOT = "S"
                 AND REC-IN(7:1) = "*"
                  MOVE REC-IN          TO REC-OUT
                  PERFORM SCRITTURA    THRU EX-SCRITTURA
                  GO TO LETTURA.

                IF REC-IN = SPACES
                  MOVE REC-IN          TO REC-OUT
                  PERFORM SCRITTURA    THRU EX-SCRITTURA
                  GO TO LETTURA.

                perform varying ind3 from 1 by 1 until
                 ind3 > LENGTH OF rec-in
                 or rec-in(ind3:1) not = spaces
                 continue
                end-perform

                IF IND3 NOT > LENGTH of rec-in

      * a initial numeric could be a line counter if located before 7th column
      * then skips at significative value after that
      *


                if ind3 < 7 and rec-in(ind3:1) NUMERIC
                 perform varying ind3 from ind3 by 1 until
                 ind3 > length of rec-in
                  or rec-in(ind3:1)  = spaces
                  continue
                 end-perform

                 if rec-in(ind3:)  = spaces
                  MOVE REC-IN          TO REC-OUT
                  PERFORM SCRITTURA    THRU EX-SCRITTURA
                  GO TO LETTURA
                 END-IF

                 perform varying ind3 from ind3 by 1 until
                 ind3 > length of rec-in
                  or rec-in(ind3:1)  not = spaces
                  continue
                 end-perform


                end-if



                IF IND3 NOT > LENGTH of rec-in

                if rec-in(ind3:1) = '"' OR "'"
                  MOVE REC-IN          TO REC-OUT
                  PERFORM SCRITTURA    THRU EX-SCRITTURA
                  GO TO LETTURA
                end-if

                if rec-in(ind3:2) = "*>"
                  MOVE REC-IN          TO REC-OUT
                  PERFORM SCRITTURA    THRU EX-SCRITTURA
                  GO TO LETTURA
                end-if

                end-if

               end-if.


        EX-LETTURA.
                EXIT.

        ELABORA SECTION.

                IF FINE-FILE = "S" GO TO EX-ELABORA.

                PERFORM LETTURA        THRU EX-LETTURA.


                MOVE ZEROS TO          IND.

                INSPECT FUNCTION UPPER-CASE(REC-IN)
                 TALLYING IND FOR ALL ">>SOURCE "
                INSPECT FUNCTION UPPER-CASE(REC-IN)
                 TALLYING IND FOR ALL " FORMAT "
                INSPECT FUNCTION UPPER-CASE(REC-IN)
                 TALLYING IND FOR ALL " IS "
                INSPECT FUNCTION UPPER-CASE(REC-IN)
                 TALLYING IND FOR ALL " FREE "

      *
      *    before to set true the F R E E   FORMAT switch
      *    we must decide if it is not another ... for example
      *    if you want to debug the animator with animator itself..
      *    it could find the sentence but with other meaning...
      *    the STRING allow you to animate the animator itself
      *    usually I do this steps:
      *
      *    c:\>copy animator.cbl anim.cbl
      *
      *    c:\>animator anim.cbl -k
      *    c:\>edit anim.ani ---> remove first "workanim.cpy"
      *    change 484 line
      *    to    TALLYING IND FOR ALL "END PROGRAM".
      *    in    TALLYING IND FOR ALL "ENDXPROGRAM".
      *
      *    change 1093
      *    to   MOVE "GOBACK"   TO END-POINT.
      *    in   MOVE "STOP RUN" TO END-POINT.
      *    c:\>cobc -x anim.ani
      *
      *    let's test the animator !
      *
                  IF IND = 4
                   MOVE 1       TO REM-COLUMN
                   MOVE SPACES  TO REC-OUT
                   STRING "       "
                    ">>SOURCE"
                    " FORMAT "
                    " IS "
                    " FREE " DELIMITED BY SIZE INTO REC-OUT
                  PERFORM SCRITTURA     THRU EX-SCRITTURA
                   MOVE "S"     TO SW-FREE
                   GO TO EX-ELABORA.

      *
      ** SW-FREE must be S(YES IN ITALIAN !!) to manage a free format
      ** if at this level it's still s (required to command line)
      ** it add the line and convert the source to the free format.
      *

                IF SW-FREE = "s"
                 MOVE 1       TO REM-COLUMN
                 MOVE SPACES  TO REC-OUT
                 STRING "       "
                    ">>SOURCE"
                    " FORMAT "
                    " IS "
                    " FREE " DELIMITED BY SIZE INTO REC-OUT
                 PERFORM SCRITTURA THRU EX-SCRITTURA
                 MOVE  "S"    TO SW-FREE

                 IF SW-VERBOSE = "S"
                 Display "adjusting to the f r e e format this source"
                 end-if.

      *
      ** manage the free format option
      *

                IF SW-FREE =  "S"
                AND REC-IN > SPACES
                 PERFORM VARYING IND FROM 1 BY 1 UNTIL
                  IND > LENGTH OF REC-LIN
                  OR REC-IN(IND:1) NOT = SPACES
                  CONTINUE
                  END-PERFORM

                  MOVE SPACES          TO STRINGA

                   IF REC-IN(IND:1) = "*"

                    MOVE 7              TO IND1

                    IF REC-IN((IND + 1):1) = SPACES
                    MOVE ">" TO REC-IN((IND + 1) :1)
                    END-IF

                   ELSE

                    MOVE 8              TO IND1

                   END-IF

                   MOVE REC-IN(IND: )   TO STRINGA(IND1:)
                   MOVE STRINGA         TO REC-IN

                END-IF.


                IF FINE-FILE NOT = "S"
                  PERFORM TRATTA       THRU EX-TRATTA.

                MOVE ZEROS TO          IND.
                INSPECT FUNCTION UPPER-CASE(REC-IN)
                 TALLYING IND FOR ALL "END PROGRAM".

                IF IND = 1 MOVE "S" TO FINE-FILE.

        EX-ELABORA.
                EXIT.

        TRATTA.

                 MOVE SPACES           TO ISTRUZIONE.
                 MOVE SPACES           TO VERBO-COBOL.

                 IF REC-IN = SPACES GO TO FINE-TRATTA.

                 PERFORM VARYING IND FROM 1 BY  1 UNTIL
                  IND > LENGTH OF REC-LIN
                 OR REC-IN(IND:1) > SPACES
                  CONTINUE
                 END-PERFORM.
      *
      ****  a line with only a character is skipped (.) or other...
      *
                 IF REC-IN(IND + 1:) =  " " GO TO FINE-TRATTA.

      *
      *** ignore any characters before A area (0-6 bytes)
      *
                 IF IND < 7
                 PERFORM VARYING IND FROM IND BY 1 UNTIL
                 IND > LENGTH OF REC-LIN
                 OR REC-IN(IND:1) = SPACES
                  CONTINUE
                 END-PERFORM

                 PERFORM VARYING IND FROM IND BY 1 UNTIL
                 IND > LENGTH OF REC-LIN
                 OR REC-IN(IND:1) > SPACES
                  CONTINUE
                 END-PERFORM

                 IF SW-VERBOSE = "S"
                 Display "removig '"
                 FUNCTION TRIM(REC-IN(1: (IND - 1)))
                 "' value from 1 to " IND " position "
                 END-IF
                 MOVE SPACES          TO REC-IN(1: (IND - 1)).

                 MOVE REC-IN(IND:)    TO STRINGA.

                 MOVE FUNCTION UPPER-CASE(STRINGA) TO STRINGA.

                 IF STRINGA = SPACES  GO TO FINE-TRATTA.

                 UNSTRING STRINGA DELIMITED BY " "
                   INTO ISTRUZIONE.

                 IF ISTRUZIONE = "PROCEDURE"
                  MOVE "S"            TO SW-CODICE.

                 INSPECT ISTRUZIONE REPLACING ALL "." BY " ".

                 IF SW-CODICE = "S"
                   AND REC-IN(REM-COLUMN:1) NOT = "*"
                    PERFORM CERCA-DO     THRU EX-CERCA-DO.

                 IF VERBO-COBOL = "SI"
                  MOVE SPACES     TO REC-OUT
                  MOVE CONTA      TO CONTA-DEBUG
                  ADD  1          TO CONTA-DEBUG

                   IF CONTA-DEBUG > 24
                   ADD 1          TO PAGINA
                   MOVE 2         TO CONTA
                   MOVE 3         TO CONTA-DEBUG
                   END-IF


      *///////////////////// this when the "data view is requested

                  IF SW-DATI  = "S"
                  MOVE    "      *> " TO REC-OUT
                   PERFORM SCRITTURA-OUT THRU SCRITTURA-OUT
                   MOVE SPACES        TO REC-OUT
                   STRING "      *> Added code for:" delimited by size
                   FUNCTION TRIM(REC-IN)  DELIMITED BY SIZE INTO REC-OUT
                  PERFORM SCRITTURA-OUT THRU SCRITTURA-OUT
                  MOVE    "      *> " TO REC-OUT
                   PERFORM SCRITTURA-OUT THRU SCRITTURA-OUT
                   MOVE SPACES        TO REC-OUT

                   IF ISTRUZIONE = END-POINT
                    MOVE '               MOVE "Z" TO TPD-SW-ANIM '
                    TO REC-OUT
                    MOVE "D"            TO TIPO-RIGA
                    PERFORM SCRITTURA   THRU EX-SCRITTURA
                    MOVE SPACES         TO REC-OUT
                   END-IF

                  STRING "            " DELIMITED BY SIZE
                   'IF TPD-START  = "S"'
                      DELIMITED BY SIZE
                      INTO REC-OUT
                       END-STRING
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                  IF OC-CONDITIONS > SPACES

                  INSPECT OC-CONDITIONS REPLACING ALL "$" BY '"'

                  STRING "            OR " DELIMITED BY SIZE
                   OC-CONDITIONS
                      DELIMITED BY SIZE
                      INTO REC-OUT
                       END-STRING
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT
                  END-IF

                  STRING "            " DELIMITED BY SIZE
                   'CALL TPD-DLL USING BY CONTENT '
                    '"' PAGINA CONTA-DEBUG '" END-CALL END-IF'
                      DELIMITED BY SIZE
                      INTO REC-OUT
                       END-STRING
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                      IF NOT COBOL-WATCH
                      STRING
                       '            IF TPD-SW-ANIM = "D" PERFORM'
                       " DO-ANIM THRU EX-DO-ANIM END-IF "
                       DELIMITED BY SIZE INTO REC-OUT
                      ELSE
                       PERFORM DO-WATCH     THRU EX-DO-WATCH
                      END-IF


                     ELSE
      *///////////////////// this when the "-d" is requested (no data view)

                   IF ISTRUZIONE = END-POINT
                    MOVE '        MOVE "Z" TO TPD-SW-ANIM ' TO REC-OUT
                    MOVE "D"            TO TIPO-RIGA
                    PERFORM SCRITTURA   THRU EX-SCRITTURA
                    MOVE SPACES         TO REC-OUT
                   END-IF

                  STRING "            " DELIMITED BY SIZE
                   'IF TPD-START  = "S"'
                      DELIMITED BY SIZE
                      INTO REC-OUT
                       END-STRING
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                  IF OC-CONDITIONS > SPACES

                  INSPECT OC-CONDITIONS REPLACING ALL "$" BY '"'

                  STRING "            OR " DELIMITED BY SIZE
                   OC-CONDITIONS
                      DELIMITED BY SIZE
                      INTO REC-OUT
                       END-STRING
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT
                  END-IF

                   STRING "           " DELIMITED BY SIZE
                   'CALL "animate" USING BY CONTENT '
                    '"' PAGINA CONTA-DEBUG '" END-CALL END-IF'
                     DELIMITED BY SIZE INTO REC-OUT
                   END-STRING

                   END-IF

                 MOVE "D"              TO TIPO-RIGA
                 PERFORM SCRITTURA     THRU EX-SCRITTURA
                 MOVE "I"              TO TIPO-RIGA.


        FINE-TRATTA.

                 MOVE DATI-IN         TO DATI-OUT.
                 PERFORM SCRITTURA    THRU EX-SCRITTURA.


                 IF ISTRUZIONE = "WORKING-STORAGE"
                  AND COPY1 > SPACES
                  MOVE COPY1          TO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  MOVE "1"            TO SW-DATI-OK
                 PERFORM SCRITTURA    THRU EX-SCRITTURA.

                 IF ISTRUZIONE = "PROCEDURE"
      *
      ** manage using... with variable on multiple lines..
      ** es: PROCEDURE USING DATI1
      **                     DATI2.
      *
                  MOVE ZEROS        TO IND

                  PERFORM UNTIL IND > ZEROS

                  PERFORM VARYING IND FROM LENGTH OF REC-IN
                   BY -1 UNTIL IND = ZEROS
                   OR REC-IN(IND:1) = "."
                  END-PERFORM

                  IF IND = ZEROS
                   PERFORM LETTURA    THRU EX-LETTURA
                   MOVE DATI-IN       TO DATI-OUT
                   PERFORM SCRITTURA  THRU EX-SCRITTURA
                  END-IF

                  END-PERFORM.


                 IF ISTRUZIONE = "PROCEDURE"

      *
      * before add animate code we must be sure that DECLARATIVES weren't defined
      *
                  MOVE SPACES         TO REC-IN
                  MOVE ZEROES         TO IND

                  PERFORM UNTIL REC-IN > SPACES

                  PERFORM LETTURA     THRU EX-LETTURA

                  END-PERFORM

                    IF SW-VERBOSE = "S"
                     Display "Searching DECLARATIVES into "
                     FUNCTION TRIM(DATI-IN)
                    END-IF

                  INSPECT FUNCTION UPPER-CASE(DATI-IN)
                    TALLYING IND FOR ALL "DECLARATIVES"

                  IF IND = 1
      *
      ***** there is a declaratives section
      *
                    IF SW-VERBOSE = "S"
                     Display "Processing DECLARATIVES "
                    END-IF

                   MOVE ZEROS        TO IND

                   MOVE DATI-IN       TO DATI-OUT
                   PERFORM SCRITTURA  THRU EX-SCRITTURA

                   PERFORM UNTIL IND = 1

                    PERFORM LETTURA     THRU EX-LETTURA
                    MOVE DATI-IN        TO DATI-OUT
                    PERFORM SCRITTURA   THRU EX-SCRITTURA

                    MOVE ZEROES         TO IND

                    INSPECT FUNCTION UPPER-CASE(DATI-IN)
                     TALLYING IND FOR ALL "END DECLARATIVES"

                   END-PERFORM

                    IF SW-VERBOSE = "S"
                     Display "End process of DECLARATIVES"
                    END-IF
                 ELSE

      *
      ***** there isn't a declaratives section so we write the tested line..
      *
                   MOVE DATI-IN       TO DATI-OUT
                   PERFORM SCRITTURA  THRU EX-SCRITTURA

                 END-IF

      *
      ***** however here we add the path for the animated source code......
      *

                 MOVE SPACES         TO REC-OUT
                 STRING "            MOVE "
                  '"' FILE-SYS '"' " TO TPD-WHERE."
                  DELIMITED BY  SIZE INTO REC-OUT
                  MOVE "D"            TO TIPO-RIGA
                 PERFORM SCRITTURA    THRU EX-SCRITTURA


                 MOVE SPACES         TO REC-OUT
                 STRING "            CALL "
                  '"animload"'
                  DELIMITED BY  SIZE INTO REC-OUT
                  MOVE "D"            TO TIPO-RIGA
                 PERFORM SCRITTURA    THRU EX-SCRITTURA

                 MOVE SPACES         TO REC-OUT
                 STRING "            CANCEL "
                  '"animload"'
                  DELIMITED BY  SIZE INTO REC-OUT
                  MOVE "D"            TO TIPO-RIGA
                 PERFORM SCRITTURA    THRU EX-SCRITTURA



                IF SW-DATI = SPACES
                 MOVE SPACES         TO REC-OUT
                 STRING "            MOVE "
                      ' "S" TO TPD-START.'
                  DELIMITED BY  SIZE INTO REC-OUT
                  MOVE "D"                      TO TIPO-RIGA
                  PERFORM SCRITTURA             THRU EX-SCRITTURA
                  MOVE SPACES                   TO DATI-OUT
                END-IF

      *
      ***** and the data analysis code...
      *

                 IF COPY2 > SPACES

                 MOVE SPACES           TO DATI-OUT
                 STRING "            "
                  'ACCEPT TPD-WAIT FROM ENVIRONMENT "'
                   FUNCTION TRIM(START-CYCLE) '".'
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  PERFORM SCRITTURA   THRU EX-SCRITTURA
      *
      **** a TPD-WAIT zero with OC-CONDITIONS required a 999999 TPD-WAIT
      *    initial value setting
      *
                 IF OC-CONDITIONS > SPACES
                 STRING "            "
                  'IF TPD-WAIT = ZEROS MOVE 999999 TO TPD-WAIT END-IF.'
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  PERFORM SCRITTURA   THRU EX-SCRITTURA
                 END-IF

                 MOVE SPACES           TO DATI-OUT
                 STRING "            "
                  'ACCEPT TPD-RUN FROM ENVIRONMENT "'
                   FUNCTION TRIM(ACTUAL-CYCLE) '".'
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  PERFORM SCRITTURA   THRU EX-SCRITTURA



                 MOVE SPACES           TO DATI-OUT
                 STRING "            "
                 "ADD 1 TO TPD-RUN."
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  PERFORM SCRITTURA   THRU EX-SCRITTURA
                  MOVE SPACES         TO DATI-OUT

                 MOVE SPACES           TO DATI-OUT
                 STRING "            "
                 'IF TPD-RUN > TPD-WAIT MOVE "S" TO TPD-START.'
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  PERFORM SCRITTURA   THRU EX-SCRITTURA
                  MOVE SPACES         TO DATI-OUT

                 STRING "            "
                  "SET ENVIRONMENT '"
                   FUNCTION TRIM(ACTUAL-CYCLE) "'"
                   " TO TPD-RUN."
                   DELIMITED BY SIZE INTO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA

                  PERFORM SCRITTURA   THRU EX-SCRITTURA
                  MOVE COPY2          TO DATI-OUT
                  MOVE "D"            TO TIPO-RIGA
                  MOVE "2"            TO SW-DATI-OK
                  PERFORM SCRITTURA   THRU EX-SCRITTURA
                 END-IF
      *
      * ALL above sentences are made when we are located at PROCEDURE DIVISION
      *

                END-IF.



        EX-TRATTA.
                EXIT.

        SCRITTURA-OUT.

                 ADD 1                 TO CONTA-LINE.

                 IF SW-FREE = "S"
                  PERFORM VARYING IND-FREE FROM  1 BY 1
                  UNTIL IND-FREE > 100
                   OR REC-OUT(IND-FREE:1) > SPACES
                   CONTINUE
                  END-PERFORM
                  MOVE REC-OUT(IND-FREE:)     TO STRINGA
                  MOVE STRINGA                TO REC-OUT
                 ELSE
                 MOVE CONTA-LINE              TO REC-OUT(1:6).

                 WRITE REC-OUT.

        EX-SCRITTURA-OUT.
                EXIT.

        SCRITTURA.

                 PERFORM SCRITTURA-OUT THRU EX-SCRITTURA-OUT.

                 IF TIPO-RIGA = "D" GO TO END-SCRITTURA.

                 ADD 1                 TO CONTA.

                 IF CONTA = 1 MOVE 2 TO CONTA.

                 IF CONTA > 24
                 ADD 1                 TO PAGINA
                 MOVE 2                TO CONTA.

                 MOVE POSIZIONE        TO CHIAVE-LIN.

                 IF SW-FREE = "S"
                 MOVE DATI-OUT         TO DATI-LIN(7:)
                 MOVE CONTA-LINE       TO DATI-LIN(1:6)
                 ELSE
                 MOVE DATI-OUT         TO DATI-LIN.

                 MOVE TIPO-RIGA        TO TIPO-LIN.
                 MOVE SW-CODICE        TO CODICE-LIN.

                 WRITE REC-LIN
                  INVALID KEY
                  DISPLAY "Severe error in writing:" STATUS-LIN
                    " " REC-LIN
                  STOP RUN.


        END-SCRITTURA.

                 MOVE SPACES           TO REC-OUT.
                 MOVE SPACES           TO REC-LIN.
                 MOVE SPACES           TO TIPO-RIGA.

        EX-SCRITTURA.
                 EXIT.

        APERTURE SECTION.

                IF SW-VERBOSE = "S"
                Display "Processing open file..".

                MOVE ZEROS TO IND.
                INSPECT FILE-IN  TALLYING IND FOR ALL ".ani".
                IF IND = 1
                 DISPLAY "Source cannot contain .ANI extension"
                 STOP RUN.

                IF FILE-IN(1:1) = SPACES
                 PERFORM VARYING IND FROM 1 BY 1 UNTIL IND >
                  LENGTH OF FILE-IN
                  OR FILE-IN(IND:1) > SPACES
                  END-PERFORM
                  IF IND NOT > LENGTH OF FILE-IN
                   MOVE FILE-IN(IND:)  TO FILE-OUT
                   MOVE FILE-OUT       TO FILE-IN.

                MOVE ZEROS TO IND
                INSPECT FILE-IN TALLYING IND FOR ALL ".cbl".
                INSPECT FILE-IN TALLYING IND FOR ALL ".src".
                INSPECT FILE-IN TALLYING IND FOR ALL ".cpy".
                INSPECT FILE-IN TALLYING IND FOR ALL ".CBL".
                INSPECT FILE-IN TALLYING IND FOR ALL ".SRC".
                INSPECT FILE-IN TALLYING IND FOR ALL ".CPY".
                INSPECT FILE-IN TALLYING IND FOR ALL ".COB".
                INSPECT FILE-IN TALLYING IND FOR ALL ".cob".
                INSPECT FILE-IN TALLYING IND FOR ALL ".gui".
                INSPECT FILE-IN TALLYING IND FOR ALL ".GUI".


      *
      * ADD THE DEFAULT .CBL ESTENSION....
      *
                IF IND = ZEROS
                PERFORM VARYING IND FROM LENGTH OF FILE-IN
                 BY -1 UNTIL IND = ZEROS

                 IF FILE-IN(IND:1) NOT = SPACES
                 ADD 1 TO IND
                 MOVE ".cbl" TO FILE-IN(IND:)
                 MOVE 1      TO IND
                 END-IF
                 END-PERFORM
                END-IF.


                MOVE FILE-IN     TO FILE-OUT.

                INSPECT FILE-OUT REPLACING ALL ".CBL" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".SRC" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".CPY" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".cbl" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".src" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".cpy" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".COB" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".cob" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".GUI" BY ".ani".
                INSPECT FILE-OUT REPLACING ALL ".gui" BY ".ani".

                MOVE ZEROS TO IND.

                INSPECT FILE-OUT TALLYING IND FOR ALL ".ani".
                IF IND NOT = 1
                 DISPLAY "Source must contain cbl/src/cpy/gui extension"
                 STOP RUN.

                OPEN INPUT   ARK-IN.

                IF STATUS-IN NOT = 00
                 DISPLAY "The source supplied is not available "
                  STATUS-IN
                 " (file:" function trim(FILE-IN) ")"
                  STOP RUN
                END-IF.


                IF SW-CONST = "S"
                 MOVE "ANDATI"  TO FILE-SYS
                 ELSE
                ACCEPT TIME-SYS FROM TIME
                MOVE TIME-SYS   TO FILE-SYS
                END-IF.

                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".log" DELIMITED BY SIZE INTO FILE-LOG.

                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".dat" DELIMITED BY SIZE INTO FILE-LIN.

                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".var" DELIMITED BY SIZE INTO FILE-VAR.


                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".stk" DELIMITED BY SIZE INTO FILE-STACK.

                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".flw" DELIMITED BY SIZE INTO FILE-FLOW.

                IF SW-CONST = "S"
                 MOVE FILE-OUT TO FILE-FLOW
                 INSPECT FILE-FLOW REPLACING ALL ".ani" by ".txt"
                 INSPECT FILE-FLOW REPLACING ALL ".ANI" by ".txt".

                STRING PATH-TEMP DELIMITED BY "     "
                FILE-SYS ".fld" DELIMITED BY SIZE INTO FILE-FIELD.

                OPEN OUTPUT  ARK-OUT.

                IF STATUS-OUT NOT = 00
                 DISPLAY "Unable to create:" FUNCTION TRIM(FILE-OUT)
                 " status:" STATUS-OUT
                 STOP RUN
                END-IF.

                OPEN OUTPUT  ARK-LIN.

                IF STATUS-LIN NOT = 00
                 DISPLAY "Unable to create:" FILE-LIN
                 STOP RUN
                END-IF.

                CLOSE ARK-LIN.

                OPEN I-O ARK-LIN.

      *
      * create the data table...
      *
                IF SW-DATI = "S"
                PERFORM FAI-COMANDO    THRU EX-FAI-COMANDO

                 IF SW-VERBOSE = "S"
                 DISPLAY "Preparing data table with..."
                 FUNCTION TRIM(COMANDO)
                 END-IF

                 CALL "SYSTEM" USING COMANDO

                  MOVE FILE-OUT  TO FILE-COPY
                  INSPECT FILE-COPY REPLACING ALL ".ani" by ".i  "


                  CALL "CBL_COPY_FILE"
                    USING FUNCTION TRIM(FILE-COPY) FILE-VAR
                     RETURNING RETURN-CODE

                    IF RETURN-CODE NOT = 0
                    DISPLAY
                    "using previous compiler syntax for data analysis."
                     "(" RETURN-CODE ") "
                    END-IF

                  PERFORM CARICA-VAR  THRU EX-CARICA-VAR
                  ELSE

                OPEN OUTPUT ARK-FIELD
                CLOSE ARK-FIELD.

        EX-APERTURE.
                EXIT.


        FAI-COMANDO.

                MOVE FILE-OUT  TO FILE-COPY.

                INSPECT FILE-COPY REPLACING ALL ".ani" by ".i  ".

                CALL "CBL_DELETE_FILE" USING FILE-COPY.

                CALL "CBL_DELETE_FILE" USING FILE-VAR


                 IF SW-VERBOSE = "S"
                 DISPLAY "Deleting flat file for data table..."
                 FUNCTION TRIM(FILE-VAR)
                 END-IF


                MOVE SPACES     TO COMANDO.

                IF COBC-INVOKE = SPACE
                 ACCEPT COBC-INVOKE FROM ENVIRONMENT "cobc.inf"
                 END-ACCEPT.


                if COBC-INVOKE = spaces
                STRING "cobc " DELIMITED BY SIZE
                OC-OPTIONS DELIMITED BY " "
                " " DELIMITED BY SIZE
                FUNCTION TRIM(FILE-IN) DELIMITED BY " "
                " -E > " delimited by size
                FILE-VAR DELIMITED BY " "
                INTO COMANDO
                ELSE

      *
      * if set cobc= into animator.inf it looks and replace
      * with the source program name that it will compile
      * by searching the $program (lowercase !!) tag
      *

                STRING "cobc " DELIMITED BY SIZE
                 FUNCTION TRIM(FILE-IN) DELIMITED BY "  "
                   " " DELIMITED BY SIZE
                " -E > " delimited by size
                FILE-VAR DELIMITED BY " "
                INTO COMANDO.


        EX-FAI-COMANDO.
                EXIT.



        CHIUSURE SECTION.
        CHIUSUREX.

                CALL "CBL_DELETE_FILE" USING FILE-LOG.

                MOVE SPACES TO STRINGA.

                CLOSE ARK-IN ARK-OUT ARK-LIN.

        EX-CHIUSURE.
                EXIT.

        READ-NEXT-DO.

                MOVE SPACES TO REC-DO EOF-DO.

                READ ARK-DO NEXT RECORD AT END
                 MOVE "S"    TO EOF-DO.


                IF EOF-DO = "S"
                 MOVE SPACES TO REC-DO
                  GO TO EX-READ-NEXT-DO.

                IF DATI-DO = SPACES GO TO READ-NEXT-DO.

                IF DATI-DO(1:1) = "*" GO TO READ-NEXT-DO.


        EX-READ-NEXT-DO.
                EXIT.

        CERCA-DO.

                MOVE SPACES TO EOF-DO.

                MOVE SPACES TO VERBO-COBOL.
      *
      * get size of the INSTRUCTION...

      *

                PERFORM VARYING IND FROM 30 BY -1 UNTIL IND = ZERO
                   OR ISTRUZIONE(IND:1) > SPACES
                   CONTINUE
                END-PERFORM.

      * MATCH A COBOL WORD WITH LEADING SPACES .. "MOVE " "ADD ".

                ADD 1 TO IND.



                OPEN INPUT ARK-DO.
      *
      * arrive to the #verbs declare section
      *

                PERFORM UNTIL EOF-DO = "S"
                 OR DATI-DO = "#verbs"
                 PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO
                END-PERFORM.
      *
      * and then process records until eof or next --> #
      *
                 IF EOF-DO NOT = "S"
                 MOVE SPACES TO REC-DO
                 PERFORM UNTIL EOF-DO = "S"
                  OR DATI-DO(1:1) = "#"
                  OR VERBO-COBOL = "SI"
                  PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO
                  IF EOF-DO NOT = "S"
                   IF DATI-DO(1:IND) = ISTRUZIONE(1:IND)
                    MOVE "SI"  TO VERBO-COBOL
                    MOVE "S"   TO EOF-DO
                   END-IF
                  END-IF
                 END-PERFORM.

                CLOSE ARK-DO.

        EX-CERCA-DO.
               EXIT.


        LOGO.
                DISPLAY TPD-COPYRIGHT.

                DISPLAY TPD-COPYRIGHT1.

                IF SW-VERBOSE = "S"
                DISPLAY "Running on: " TYPE-SYSTEM.

                IF SW-VERBOSE = "S"
                DISPLAY "Processing command line..".
        EX-LOGO.
                EXIT.

        INIZIALI.

                INITIALIZE PARAMETRI.

                MOVE 1       TO PAGINA.
                MOVE 7       TO REM-COLUMN.

                ACCEPT TYPE-SYSTEM FROM ENVIRONMENT 'OS' END-ACCEPT.

                IF TYPE-SYSTEM NOT > SPACES
                 ACCEPT TYPE-SYSTEM FROM ENVIRONMENT 'OSTYPE' END-ACCEPT
                 END-IF.

                MOVE "GOBACK" TO END-POINT.

        EX-INIZIALI.
                EXIT.

        HELP.
                DISPLAY "Usage: animator cobolsource [options]".
                DISPLAY "                              ".
                DISPLAY "cobolsource:source.cbl/cpy/src (when missing "
                        '".cbl" is added by default....)'.
                DISPLAY "                              ".
                DISPLAY "Options:                      ".
                DISPLAY "                              ".
                DISPLAY "-h    This support panel           ".
                DISPLAY "-d    Turn off data analisys       ".
                DISPLAY "-c   Do not compile the generated .ani source".
                DISPLAY "-k/-K keep the generated cobol .ani source".
                DISPLAY "-f    Use constants names for temporary files".
                DISPLAY "-F    Force to free format         ".
                DISPLAY "-S    Use stop run to evaluate exit".
                DISPLAY "-x    Build an executable program  ".
                DISPLAY "-A    Perform only structure control"
                                " flow graph".
                DISPLAY "-v    Turn on verbose              ".
                DISPLAY "-o    add native option invoking "
                        " the compiler (eg: -o'-Wall') ".
                DISPLAY "-w    start  debugger when given condition is"
                        " reached eg: -w'ind equal 10' ".
                DISPLAY "      'stringa equal $hello$' where $ is for"
                        " quote meaning".

                DISPLAY "page 1 of 2 press any key to continue.. "
                ACCEPT SW-HELP.

                DISPLAY "Use these system variables to manage some"
                             " animator startup features".
                DISPLAY "                              ".
                DISPLAY "SET animator.inf=\yourdir\animator.inf"
                        "   (.\animator.inf default value)   ".
                DISPLAY "                              ".
                DISPLAY "SET start-<yourcobol>.inf=001041      "
                        "   (starts debugger at 001041 cycle) ".

                DISPLAY "                              ".
                DISPLAY "T&P Soluzioni Informatiche srl".
                DISPLAY "Via Paradiso 6,02304 Montopoli di Sabina (RI)"
                           " Italy"
                DISPLAY "www.tp-srl.it  federico.priolo@tp-srl.it ".
                DISPLAY "                              ".

                DISPLAY "page 2 of 2 press any key to continue.. "
                ACCEPT SW-HELP.

        EX-HELP.
                EXIT.

        MANUALE.

                MOVE SPACES     TO COMANDO.

                IF TYPE-SYSTEM(1:1) = "W"
                STRING "start "
                "www.tp-srl.it/federico/tp-cobol-debugger"
                "/tp-cobol-debugger.htm" delimited by size into COMANDO
                END-STRING
                ELSE
                MOVE "tp-cobol-debugger.htm" to COMANDO.

                CALL "SYSTEM" USING COMANDO.

        EX-MANUALE.
                EXIT.

        CARICA-VAR.

                MOVE SPACES            TO AREA-DATI.

                OPEN INPUT ARK-VAR.

                IF STATUS-VAR NOT = 00
                 DISPLAY
                 "Unable to locate:" FILE-VAR
                  STOP RUN.

                MOVE "do-anim.cpy"    TO FILE-CPY.
                OPEN OUTPUT ARK-CPY.

                MOVE SPACES          TO REC-CPY.

                MOVE "       DO-ANIM-TEST-VARIABLES." to rec-cpy
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.

                MOVE SPACES            TO REC-CPY.
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.
                MOVE "      *>    evaluate data variables... "
                                       TO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.
                MOVE SPACES            TO REC-CPY.
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.

                STRING "            MOVE "
                 '"' FILE-SYS '"' " TO TPD-FILE."
                 DELIMITED BY SIZE INTO REC-CPY
                  PERFORM WRITE-CPY THRU EX-WRITE-CPY.

                STRING "          "
                '  IF TPD-SW-ANIM = "A" GO TO DO-ANIM1 END-IF.'
                 DELIMITED BY SIZE INTO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.



                OPEN OUTPUT ARK-FIELD.

                CLOSE ARK-FIELD.

                OPEN I-O ARK-FIELD.

                MOVE SPACES TO CAMPO5.

        CICLO-CARICA-VAR.

                READ ARK-VAR NEXT RECORD AT END GO TO FINE-CARICA-VAR.

                MOVE FUNCTION UPPER-CASE(REC-VAR) TO REC-VAR.



                IF REC-VAR = SPACES
                 OR REC-VAR(1:1) = "#"
                  GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND
                  FOR ALL "DATA "

                  IF IND = 1
                  MOVE ZEROS TO IND

                  INSPECT REC-VAR TALLYING IND
                  FOR ALL " DIVISION."

                  IF IND = 1
                   MOVE "S" TO AREA-DATI
                   GO TO CICLO-CARICA-VAR.

                IF AREA-DATI NOT = "S" GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND
                  FOR ALL " POINTER "
                  IF IND = 1 GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND
                  FOR ALL " USAGE "
                  IF IND = 1 GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND
                  FOR ALL " SECTION."
                  IF IND = 1 GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND
                  FOR ALL "PROCEDURE "

                  IF IND = 1
                  MOVE ZEROS TO IND
                  INSPECT REC-VAR TALLYING IND
                  FOR ALL " DIVISION."

                  IF IND = 1 GO TO FINE-CARICA-VAR.


                  MOVE ZEROS TO IND
                  INSPECT REC-VAR TALLYING IND
                  FOR ALL " SECTION."

                  IF IND = 1 GO TO CICLO-CARICA-VAR.


                MOVE SPACES TO CAMPO1 CAMPO2 CAMPO3 CAMPO4.

                UNSTRING REC-VAR DELIMITED BY ALL SPACES
                 INTO  CAMPO1 CAMPO2 CAMPO3 CAMPO4.


                IF CAMPO2(3:) NOT = SPACES

                   MOVE CAMPO2         TO CAMPO5
                   INSPECT CAMPO5 REPLACING ALL " " BY "0"

                   IF CAMPO5 NOT NUMERIC
                    GO TO CICLO-CARICA-VAR
                   END-IF

                   IF SW-VERBOSE = "S"
                    AND CAMPO2 NOT = "88"
                     Display "fixind level for "
                      FUNCTION TRIM(REC-VAR)
                   END-IF

                 PERFORM VARYING IND FROM LENGTH OF CAMPO2
                  BY -1 UNTIL CAMPO2(IND:1) NUMERIC
                  CONTINUE
                 END-PERFORM

                 IF IND = 1
                  MOVE CAMPO2(1:1) TO CAMPO2(2:)
                  MOVE ZEROS       TO CAMPO2(1:1)
                  ELSE
                  MOVE CAMPO2(IND - 1: 2) TO CAMPO2(1:)
                  MOVE SPACES      TO CAMPO2(3:)

                    IF SW-VERBOSE = "S"
                     AND CAMPO2 NOT = "88"
                     Display "changed level field "
                      "from " FUNCTION TRIM(REC-VAR)
                             " to " CAMPO2
                    END-IF
                 END-IF
                END-IF.

                IF NOT LIVELLO-OK

                    IF SW-VERBOSE = "S"
                     AND CAMPO2 NOT = "88"
                     Display "skipping name field: "
                      FUNCTION TRIM(REC-VAR)
                     END-IF

                GO TO CICLO-CARICA-VAR.



      ***** A 02 PIC X VALUE SPACE....WITHOUT THE NAME (COBOL ALLOWS IT!)

                IF CAMPO3 = "PIC" GO TO CICLO-CARICA-VAR.

      ***** Another missing name definition eg: 05. <--- with no filler
      ******                                     09 filler1.
      ******
                IF CAMPO3 = SPACES GO TO CICLO-CARICA-VAR.

      ***** Another missing name definition eg: 05 REDEFINES
                IF CAMPO3 = "REDEFINES" GO TO CICLO-CARICA-VAR.

      **** a composite field AND/OR record

                IF CAMPO4 = SPACES
                INSPECT CAMPO3 REPLACING ALL "." BY SPACES.

                MOVE SPACES TO REC-CPY.

                IF CAMPO3 = "FILLER"  GO TO CICLO-CARICA-VAR.

                IF CAMPO3 = "THRU"  GO TO CICLO-CARICA-VAR.

                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND FOR ALL " OCCURS".

                IF IND = 1
                 OR CAMPO4 = "OCCURS"


      *
      ** skip the 2th dimension of a table (this version doesn't manage them
      *

                  IF LIVELLO > RIC-LIVELLO
                   AND INSIDE-OCCURS = "SI"
                    MOVE "SI"          TO SECOND-DIMENSION
                     Display "skipping 2th table definition for "
                      FUNCTION TRIM(REC-VAR)

                    IF SW-VERBOSE = "S"
                     Display "skipping 2th table definition for "
                      FUNCTION TRIM(REC-VAR)
                     END-IF

                    GO TO CICLO-CARICA-VAR


                  END-IF

                  MOVE "SI"    TO INSIDE-OCCURS
                  MOVE LIVELLO TO RIC-LIVELLO
                  MOVE SPACES  TO SECOND-DIMENSION
                   GO TO INIZIO-ACCEPT.


               IF LIVELLO > RIC-LIVELLO
                 AND SECOND-DIMENSION = "SI"
                   Display "skipping 2th table definition for "
                    FUNCTION TRIM(REC-VAR)
                     GO TO CICLO-CARICA-VAR.


                MOVE ZEROS TO IND.
                INSPECT REC-VAR TALLYING IND FOR ALL " PIC ".
      *         IF IND NOT = 1 GO TO CICLO-CARICA-VAR.

                IF LIVELLO = 77
                 AND INSIDE-OCCURS = "SI"
                   MOVE SPACES TO INSIDE-OCCURS
                   MOVE SPACES TO SECOND-DIMENSION
                   MOVE ZEROS  TO RIC-LIVELLO
                   GO TO INIZIO-ACCEPT.

                IF LIVELLO NOT > RIC-LIVELLO
                 AND INSIDE-OCCURS = "SI"
                   MOVE SPACES TO INSIDE-OCCURS
                   MOVE SPACES TO SECOND-DIMENSION
                   MOVE ZEROS  TO RIC-LIVELLO.

         INIZIO-ACCEPT.


                 INITIALIZE  REC-FIELD.

                 MOVE FUNCTION UPPER-CASE(CAMPO3) TO NOME-FIELD.
                 MOVE INSIDE-OCCURS   TO OCC-FIELD.

                 WRITE REC-FIELD
                  INVALID KEY

                  IF SW-VERBOSE = "S"
                  DISPLAY "Warning:duplicated field name "
                   FUNCTION TRIM(NOME-FIELD)
                   " won't be used for data analysis "
                  END-IF

                  READ ARK-FIELD KEY IS CHIAVE-FIELD

                  ADD 1 TO TIMES-FIELD
                  REWRITE REC-FIELD

                 END-WRITE.


                 GO TO CICLO-CARICA-VAR.

        FINE-CARICA-VAR.

                MOVE LOW-VALUE TO CHIAVE-FIELD.
                START ARK-FIELD KEY IS NOT < CHIAVE-FIELD
                 INVALID KEY GO TO QUIT-CARICA-VAR.

                STRING "          "
                '  EVALUATE TPD-NAME '
                 DELIMITED BY SIZE INTO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.

        LOOP-CARICA-VAR.

                READ ARK-FIELD NEXT RECORD AT END GO TO END-CARICA-VAR.

                IF TIMES-FIELD NOT = ZEROS
                 GO TO LOOP-CARICA-VAR.


                IF OCC-FIELD = "SI"
                MOVE "      *>                                 "
                                       TO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY
                STRING  "            WHEN "
                 '"' DELIMITED BY SIZE
                 NOME-FIELD DELIMITED BY "    "
                 '"' DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "           "
                    " IF TPD-INDEX = ZEROS "
                    "MOVE 1 TO TPD-INDEX END-IF"
                    DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY

                   STRING "            SET TPD-ADDRESS TO ADDRESS OF "
                     DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "            "
                   DELIMITED BY SIZE
                    NOME-FIELD DELIMITED BY "   "
                    "(TPD-INDEX)"
                     DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "            MOVE LENGTH OF "
                        DELIMITED BY SIZE
                    NOME-FIELD DELIMITED BY "   "  INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "                 "
                   "(TPD-INDEX) TO TPD-LENGTH"
                   DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   ELSE

                MOVE "      *>                                 "
                                       TO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY
                STRING  "            WHEN "
                 '"' DELIMITED BY SIZE
                 NOME-FIELD DELIMITED BY "    "
                 '"' DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY

                   STRING "            SET TPD-ADDRESS TO ADDRESS OF "
                   DELIMITED BY SIZE
                    NOME-FIELD DELIMITED BY "   "  INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "            MOVE LENGTH OF  "
                        DELIMITED BY SIZE
                    NOME-FIELD DELIMITED BY "    " INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY
                   STRING "            "
                    " TO TPD-LENGTH" DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY.


                MOVE SPACES            TO REC-CPY.
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.

                GO TO LOOP-CARICA-VAR.


        END-CARICA-VAR.

                MOVE  "           END-EVALUATE." TO REC-CPY
                PERFORM WRITE-CPY THRU EX-WRITE-CPY.

                STRING "          "
                  " PERFORM DO-VIEW-ANIM THRU EX-DO-VIEW-ANIM."
                   DELIMITED BY SIZE INTO REC-CPY
                   PERFORM WRITE-CPY THRU EX-WRITE-CPY.

        QUIT-CARICA-VAR.

                 CLOSE ARK-VAR ARK-CPY ARK-FIELD.

        EX-CARICA-VAR.
                EXIT.

        WRITE-CPY.

                 WRITE REC-CPY.

                 IF STATUS-CPY NOT = "00"
                  DISPLAY "Severe error writing cpy file.."
                  STATUS-CPY
                  STOP RUN
                 END-IF.

                 MOVE SPACES      TO REC-CPY.

        EX-WRITE-CPY.
                EXIT.

        RINUMERA.

                MOVE LOW-VALUE TO CHIAVE-LIN.
                START ARK-LIN KEY IS NOT < CHIAVE-LIN
                 INVALID KEY GO TO EX-RINUMERA.

        CICLO-RINUMERA.

                READ ARK-LIN NEXT RECORD AT END GO TO EX-RINUMERA.

                MOVE DATI-LIN(1:6) TO THIS-LINE(1:)

      *
      *  calcultating this line in that % is in the video range about this
      *  animated file
      *

                COMPUTE ROW-PERC-LIN ROUNDED =

                ( ( ( ( THIS-LINE / CONTA-LINE ) * 100 ) * 25 ) / 100 )
                          + 1
                    ON SIZE ERROR MOVE 25 TO ROW-PERC-LIN

                END-COMPUTE.

                IF ROW-PERC-LIN > 25 MOVE 25 TO ROW-PERC-LIN.

                REWRITE REC-LIN.

                GO TO CICLO-RINUMERA.

        EX-RINUMERA.
                EXIT.

        OPZIONI.

      *
      * put the cobol source name as default for "wait" open debugger variable
      *

               MOVE SPACES  TO START-CYCLE.
               PERFORM VARYING IND FROM 1 BY 1
               UNTIL IND > LENGTH OF START-CYCLE
                OR FILE-IN(IND:1) = "."
                OR FILE-IN(IND:1) = SPACES
               CONTINUE
               END-PERFORM
               IF IND NOT > LENGTH OF START-CYCLE
                SUBTRACT 1 FROM IND
                 STRING "start-" FUNCTION TRIM(FILE-IN(1:IND))
                  DELIMITED BY SIZE INTO START-CYCLE
                 STRING "cycle-" FUNCTION TRIM(FILE-IN(1:IND))
                  DELIMITED BY SIZE INTO ACTUAL-CYCLE
               PERFORM VARYING IND FROM LENGTH OF START-CYCLE BY -1
               UNTIL START-CYCLE(IND:1) NOT = " "
               CONTINUE
               END-PERFORM
               ADD 1 TO IND
               MOVE ".inf" TO START-CYCLE(IND:)
               MOVE ".inf" TO ACTUAL-CYCLE(IND:).


               ACCEPT FILE-DO FROM ENVIRONMENT "animator.inf"
                END-ACCEPT.

                IF FILE-DO = SPACES
                MOVE "animator.inf"    TO FILE-DO.

                PERFORM OPZIONI-ANIMATOR THRU EX-OPZIONI-ANIMATOR.

                MOVE "S"               TO SW-DATI.
      *
      ** obtain options from command line... max 10 elements
      *

      ****************** > here we are sure that the command have something

                PERFORM VARYING IND FROM 1 BY 1 UNTIL IND >
                 LENGTH OF FILE-IN
                 OR FILE-IN(IND:1) NOT = SPACES
                 CONTINUE
                END-PERFORM.

      *
      ****************** > if first is "-" we could have -? or -m option..
      *

                IF FILE-IN(IND:1) NOT = "-"

      ****************** > from here we can check for the option

                 PERFORM VARYING IND FROM IND BY 1 UNTIL IND >
                  LENGTH OF FILE-IN
                  OR FILE-IN(IND:1) = SPACES
                  CONTINUE
                 END-PERFORM

                END-IF.

                MOVE 1                 TO IND1.

                PERFORM VARYING IND FROM IND BY 1 UNTIL IND >
                 LENGTH OF FILE-IN
                 OR IND1 > MAXOPZ

                 IF FILE-IN(IND:1) = "-"
                 MOVE SPACES TO FILE-IN(IND:1)

                 ADD 1                 TO IND
                 MOVE FILE-IN(IND:1)   TO SW-OPTION(IND1)

                 IF SW-OPTION(IND1) = "o" or "w"
      *
      ** both procedure use the oc-buffer to load the parameters required
      *

                  IF SW-OPTION(IND1) = "o"
                  move OC-OPTIONS      TO OC-BUFFER
                  else
      *
      **** only one -w can actually be managed
      *
                  IF OC-CONDITIONS > SPACES
                  DISPLAY
                  "the -w option doesn't allows multiple invocations "
                  END-IF
                  move SPACES          TO OC-BUFFER
                  END-IF
      *
      ** move IND2 at the end of last parameter added/declared
      *

                 MOVE SPACES TO FILE-IN(IND:1)
                  PERFORM VARYING IND2 FROM LENGTH OF OC-BUFFER
                   BY -1 UNTIL IND2 = ZEROS
                    OR OC-BUFFER(IND2:1) > SPACES
                    CONTINUE
                  END-PERFORM

      *
      *** then ahead to leave a blank space
      *

                  ADD 1 TO IND2

      *
      *** move the option that is specified into "        "
      *

                  PERFORM VARYING IND FROM IND BY 1 UNTIL IND >
                  LENGTH OF FILE-IN
                  OR FILE-IN(IND:1) = "'"


                  IF FILE-IN(IND:1) NOT = "'"
                  MOVE SPACES TO FILE-IN(IND:1)
                  END-IF

                  END-PERFORM

      *
      *** IND rappresents the start of the option..
      *

                  IF IND NOT > LENGTH OF FILE-IN
                  AND FILE-IN(IND:1) = "'"
                  MOVE SPACES TO FILE-IN(IND:1)
      *
      *** then ahead to start after "
      *
                  ADD 1 TO IND

                  PERFORM VARYING IND FROM IND BY 1 UNTIL IND >
                  LENGTH OF FILE-IN
                  OR FILE-IN(IND:1) = "'"

                   MOVE FILE-IN(IND:1) TO OC-BUFFER(IND2:1)
                   MOVE SPACES TO FILE-IN(IND:1)
                   ADD 1               TO IND2

                  END-PERFORM

                  IF IND NOT > LENGTH OF FILE-IN
                   MOVE SPACES TO FILE-IN(IND:1)
                   ADD 1     TO IND
                  END-IF

                  END-IF

                  IF SW-OPTION(IND1) = "o"
                  move OC-BUFFER       TO OC-OPTIONS
                  else
                  move OC-BUFFER       TO OC-CONDITIONS
                  END-IF
      *
      ***** end if of the manager for the -o or -w options
      *
                 END-IF


                 ADD 1       TO IND1

                  IF IND1 > MAXOPZ
                   DISPLAY "Too many conditions for parameters"
                   STOP RUN
                  END-IF

                 PERFORM VARYING IND FROM IND BY 1 UNTIL
                  IND > LENGTH OF FILE-IN
                  OR FILE-IN(IND:1) = SPACES

                  MOVE SPACES TO FILE-IN(IND:1)

                 END-PERFORM

                 END-IF

                END-PERFORM.

                DISPLAY "Animator.inf file used is: "
                 FUNCTION TRIM(FILE-DO).

                IF TAB-OPTIONS > SPACES
                DISPLAY "invoked argument(S) for the animator:".

      *
      *  set the appropriate switch
      *
                PERFORM VARYING IND FROM 1 BY 1 UNTIL IND = IND1

                 EVALUATE SW-OPTION(IND)
                  WHEN "c" MOVE "S"   TO SW-NOCOMPILA
                  DISPLAY "do not compile the animated source.."

                  WHEN "k" MOVE "S"   TO SW-SAVE
                  DISPLAY "keep the .ani generated source.."
                  WHEN "K" MOVE "S"   TO SW-SAVE
                  DISPLAY "keep the .ani generated source.."
                  WHEN "x" MOVE "S"   TO SW-EXE
                  DISPLAY "compile main executable (.exe).."
                  WHEN "d" MOVE SPACE TO SW-DATI
                  DISPLAY "do not use the data analysis...."
                  WHEN "h" MOVE "S"   TO SW-HELP
                  DISPLAY "show the help panel............."
                  WHEN "m" MOVE "S"   TO SW-MANUAL
                  DISPLAY "show the http online manual....."
                  WHEN "f" MOVE "S"   TO SW-CONST
                  DISPLAY "force the temp file to constants"
                  WHEN "F" MOVE "s"   TO SW-FREE
                  DISPLAY "use the free format reading file"
                  WHEN "S" MOVE "S"   TO SW-STOP
                  DISPLAY "use STOP run as verb for exit..."
                  WHEN "A" MOVE "S"   TO SW-ANALYSIS
                  DISPLAY "run only the analyzer module    "
                  WHEN "v" MOVE "S"   TO SW-VERBOSE
                  DISPLAY "show verbose process during make"
                  WHEN "w"
                  DISPLAY "use this  cobol condition for starting "
                  DISPLAY "    " FUNCTION TRIM(OC-CONDITIONS)
                  WHEN "o"
                  DISPLAY "add " FUNCTION TRIM(OC-OPTIONS)
                   " invoking the compiler (eg: -o'-Wall')"
                  WHEN OTHER
                  DISPLAY "Unrecognisez option in command line: '"
                    SW-OPTION(IND) "'"
                  STOP RUN
                 END-EVALUATE

                END-PERFORM.


                IF SW-STOP = "S" MOVE "STOP" TO END-POINT.

                IF SW-DATI = "S"

                  MOVE '          copy "workanim.cpy".' TO COPY1
                  MOVE '          copy "procanim.cpy".' TO COPY2
                  ELSE
                  MOVE '          copy "workanim.cpy".' TO COPY1

      *         END-IF.

                IF SW-HELP = "S"
                 PERFORM HELP THRU EX-HELP
                 STOP RUN.

                IF SW-MANUAL = "S"
                 PERFORM MANUALE THRU EX-MANUALE
                 STOP RUN.

                IF SW-EXE = SPACES
                  MOVE "-m" TO DO-DLL
                  DISPLAY "working on a callable module (.dll).."
                  ELSE
                  MOVE "-x" TO DO-DLL
                 END-IF.

                IF SW-VERBOSE = "S"
                 DISPLAY "the startup variable is:"
                   FUNCTION TRIM(START-CYCLE)
                 DISPLAY "the control variable is:"
                   FUNCTION TRIM(ACTUAL-CYCLE).

        EX-OPZIONI.
                EXIT.

        OPZIONI-ANIMATOR.


                MOVE SPACES TO EOF-DO.
                MOVE SPACES TO PATH-TEMP.

                OPEN INPUT ARK-DO.



                IF STATUS-DO = 35
                 DISPLAY
                 "The animator configuration file is not available:"
                   FILE-DO
                  STOP RUN.

                PERFORM UNTIL EOF-DO = "S"

                PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO

                MOVE ZEROS  TO IND
                INSPECT DATI-DO TALLYING IND FOR ALL "="

                IF IND > ZEROS

                 IF IND = 1
                 MOVE SPACES TO PARAMETRO VALORE
                 UNSTRING DATI-DO DELIMITED BY "="
                  INTO PARAMETRO VALORE
                 END-UNSTRING
                 ELSE
                  PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 500
                   OR DATI-DO(IND:1) = "="
                   CONTINUE
                  END-PERFORM

                  SUBTRACT 1 FROM IND
                  MOVE DATI-DO(1:IND) TO PARAMETRO
                  ADD  2     TO IND
                  MOVE DATI-DO(IND:)  TO VALORE
                 END-IF



                 EVALUATE PARAMETRO
                 WHEN "TEMP" MOVE VALORE TO PATH-TEMP
                             CALL "CBL_CREATE_DIR" USING PATH-TEMP
                 WHEN "COBC" MOVE VALORE TO COBC-INVOKE

                END-IF

                END-PERFORM.

                CLOSE ARK-DO.

        EX-OPZIONI-ANIMATOR.
                 EXIT.

        ANALYSIS.

                 IF SW-VERBOSE = "S"
                 DISPLAY "Invoking the analysis module...".

                 CALL "analyzer" USING FILE-IN.


        EX-ANALYSIS.
                 EXIT.

        DO-WATCH.

                 OPEN INPUT ARK-FIELD.

                 MOVE SPACES TO WATCH-TABLE.

                 MOVE SPACES TO WATCH-REC WATCH-INSIDE.
                 MOVE 1      TO IND1.

                 PERFORM VARYING IND FROM 1 BY 1 UNTIL IND >
                 LENGTH OF REC-IN

                 IF REC-IN(IND:1) = "(" AND
                     REC-IN(IND:2) NOT EQUAL '("'
                   PERFORM VARYING IND1 FROM LENGTH OF WATCH-REC
                    BY -1 UNTIL IND1 = ZERO
                    OR WATCH-REC(IND1:) > SPACES
                    CONTINUE
                   END-PERFORM
                   ADD 1 TO IND1 IND2
                   MOVE SPACES         TO WATCH-INSIDE
                   PERFORM VARYING IND FROM IND BY 1 UNTIL
                   IND > LENGTH OF REC-IN
                   OR REC-IN(IND:1) = ")"

                    IF REC-IN(IND:1) = SPACES
                    MOVE "@"            TO WATCH-REC(IND1:1)
                    ELSE
                    MOVE REC-IN(IND:1)  TO WATCH-REC(IND1:1)

                    IF REC-IN(IND:1) NOT = "("
                    MOVE REC-IN(IND:1)  TO WATCH-INSIDE(IND2:1)
                    ADD 1               TO IND2
                    END-IF

                    END-IF

                    ADD 1               TO IND1

                   END-PERFORM
                   MOVE REC-IN(IND:1)  TO WATCH-REC(IND1:1)
                   ADD 1               TO IND1

                  ELSE
                  MOVE REC-IN(IND:1)   TO WATCH-REC(IND1:1)
                  ADD 1                TO IND1
                  END-IF

                 END-PERFORM.


                 MOVE ZEROS            TO IND.

                 UNSTRING WATCH-REC DELIMITED BY ALL SPACES
                  INTO WATCH-NAME(1)
                       WATCH-NAME(2)
                       WATCH-NAME(3)
                       WATCH-NAME(4)
                       WATCH-NAME(5)
                       WATCH-NAME(6)
                       WATCH-NAME(7)
                       WATCH-NAME(8)
                       WATCH-NAME(9)
                       WATCH-NAME(10)
                        TALLYING IND.

                   IF IND NOT > 10
                    AND WATCH-INSIDE > SPACES
                    ADD 1 TO IND
                    MOVE WATCH-INSIDE TO WATCH-NAME(IND).

                 MOVE ZEROS TO WATCH-ITEMS
                 INSPECT WATCH-TABLE REPLACING ALL "@" BY " ".

                 PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 10


                 IF WATCH-NAME(IND) > SPACES
                  MOVE FUNCTION UPPER-CASE(WATCH-NAME(IND)) TO
                                           WATCH-NAME(IND)
                  INSPECT WATCH-NAME(IND) REPLACING ALL "." BY " "


                  PERFORM MANAGE-WATCH    THRU EX-MANAGE-WATCH

                  IF WATCH-NAME(IND) > SPACES
                   ADD 1                TO WATCH-ITEMS
                  END-IF

                  END-IF

                 END-PERFORM.

                 CLOSE ARK-FIELD.

                 IF WATCH-TABLE = SPACES
                      STRING
                       '            IF TPD-SW-ANIM = "D" PERFORM'
                       " DO-ANIM THRU EX-DO-ANIM END-IF "
                       DELIMITED BY SIZE INTO REC-OUT
                 GO TO EX-DO-WATCH.

                 MOVE SPACES TO REC-OUT

                 STRING
                       '            '
                       'EVALUATE TPD-SW-ANIM '
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT.

                 STRING
                       '            '
                       ' WHEN "D" PERFORM'
                       " DO-ANIM THRU EX-DO-ANIM  "
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT.

                 MOVE SPACES TO REC-OUT

                 STRING
                       '            '
                   ' WHEN "W" MOVE '
                   '"' WATCH-ITEMS '" TO TPD-WATCH-TABLE(1:)'
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT.

                 MOVE ZEROS                TO IND5.

                 PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 10

                 IF WATCH-NAME(IND) > SPACES
                 ADD 1                      TO IND5
                      STRING
                       '            '
                       ' MOVE "' DELIMITED BY SIZE
                       WATCH-NAME(IND) DELIMITED BY "   "
                       '"'
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                      STRING
                       '            '
                       'TO TPD-WATCH-TITLE (' IND5 ")"
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                       MOVE ZEROS          TO IND3
                       INSPECT WATCH-NAME(IND) TALLYING IND3 FOR ALL "("
                       INSPECT WATCH-NAME(IND) TALLYING IND3 FOR ALL ")"
                       INSPECT WATCH-NAME(IND) TALLYING IND3 FOR ALL ":"
                       IF IND3 NOT < 3
                        MOVE " TO "      TO WOFFSET
                        ELSE
                        MOVE " (1:) TO " TO WOFFSET
                       END-IF


                       STRING
                       '            '
                       ' MOVE ' DELIMITED BY SIZE
                       WATCH-NAME(IND) DELIMITED BY "   "
                       WOFFSET DELIMITED BY SIZE
                       INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                       STRING
                       '            '
                       " TPD-WATCH-VALUE (" IND5 ") (1:)"
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT

                 END-IF

                 END-PERFORM

                 STRING

                       '            '
                       ' CALL TPD-WATCH'
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT.


                 MOVE SPACES TO REC-OUT

                 STRING
                       '            '
                       'END-EVALUATE'
                       DELIMITED BY SIZE INTO REC-OUT
                       MOVE "D"            TO TIPO-RIGA
                       PERFORM SCRITTURA   THRU EX-SCRITTURA
                       MOVE SPACES         TO REC-OUT
                       MOVE SPACES         TO DATI-OUT.

      *          DISPLAY FUNCTION TRIM(WATCH-NAME(1)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(2)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(3)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(4)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(5)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(6)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(7)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(8)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(9)).
      *          DISPLAY FUNCTION TRIM(WATCH-NAME(10)).

        EX-DO-WATCH.
                 EXIT.

        MANAGE-WATCH.

                MOVE SPACES  TO CHIAVE-FIELD.

                MOVE ZEROS   TO IND2.

                INSPECT WATCH-NAME(IND) TALLYING IND2 FOR ALL "(".

                 IF IND2 = 1

                  IF WATCH-NAME(IND) (1:1) NOT = "("
                  UNSTRING WATCH-NAME(IND) DELIMITED BY "("
                   INTO CHIAVE-FIELD
                   ELSE
                    MOVE ZEROS      TO IND2
                    PERFORM VARYING IND1 FROM 2 BY 1 UNTIL IND1 >
                    LENGTH OF WATCH-NAME(IND)
                    OR WATCH-NAME(IND) (IND:1) = ")"
                    ADD 1 TO IND2
                    END-PERFORM
                    MOVE WATCH-NAME(IND) (2:IND2) TO CHIAVE-FIELD
                  END-IF

                 ELSE

                   MOVE WATCH-NAME(IND) TO CHIAVE-FIELD.

                 READ ARK-FIELD  KEY IS CHIAVE-FIELD
                  INVALID KEY
                  MOVE SPACES  TO WATCH-NAME(IND)
                   GO TO EX-MANAGE-WATCH.

                 IF SW-VERBOSE = "S"
                  DISPLAY "added watch for "
                   FUNCTION TRIM(WATCH-NAME(IND)).

        EX-MANAGE-WATCH.
                 EXIT.



        end program animator.

