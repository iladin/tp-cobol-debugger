        IDENTIFICATION DIVISION.
        PROGRAM-ID.    animdata.
      *---------------------------------------------------------------------
      *
      * ANIMATORE INTERATTIVO CODICE OPENCOBOL
      *
      * Copyright (C) 2010-2014 Federico Priolo T&P Soluzioni Informatiche Srl
      *                         federico.priolo@tp-srl.it
      *
      * This program is free software; you can redistribute it and/or modify
      * it under the terms of the GNU General Public License as published by
      * the Free Software Foundation; either version 2, or (at your option)
      * any later version.
      *
      * This program is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU General Public License for more details.
      *
      * You should have received a COPY of the GNU General Public License
      * along with this software; see the file COPYING.  If not, write to
      * the Free Software Foundation, 51 Franklin Street, Fifth Floor
      * Boston, MA 02110-1301 USA
      *---------------------------------------------------------------------
        ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
        SOURCE-COMPUTER.        PC-IBM.
        OBJECT-COMPUTER.        PC-IBM.
        SPECIAL-NAMES.

           DECIMAL-POINT IS COMMA.

        INPUT-OUTPUT SECTION.
        FILE-CONTROL.


                SELECT ARK-FIELD ASSIGN TO FILE-FIELD
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS  DYNAMIC
                 RECORD KEY   IS  CHIAVE-FIELD
                 ALTERNATE RECORD KEY IS BREAK-FIELD WITH DUPLICATES
                 FILE STATUS  IS STATUS-FIELD.

                SELECT ARK-DO ASSIGN TO  FILE-DO
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-DO.

        DATA DIVISION.
        FILE SECTION.

        FD ARK-DO.
        01 REC-DO.
           02 DATI-DO                 PIC X(100).


        FD ARK-FIELD.
        01 REC-FIELD GLOBAL.
         02 CHIAVE-FIELD.
            09 NOME-FIELD             PIC X(30).
         02 TIPO-FIELD                PIC X(01).
         02 VALUE-FIELD               PIC X(256).
         02 SIZE-FIELD                PIC 9(9) COMP-5.
         02 BREAK-FIELD               PIC X.
           88 BREAK-IF-EQUAL          VALUE "1".
           88 BREAK-IF-LESS           VALUE "2".
           88 BREAK-IF-MAJIOR         VALUE "3".
           88 BREAK-IF-NOT-MAJOR      VALUE "4".
           88 BREAK-IF-NOT-LESS       VALUE "5".
           88 BREAK-IF-CHANGE         VALUE "6".
           88 BREAK-IF-OK             VALUE "1" THRU "6".
         02 IBREAK-FIELD REDEFINES BREAK-FIELD PIC 9.
         02 FILLER                    PIC XX.
         02 VALUE-BREAK-FIELD         PIC X(256).
         02 KEY-FIELD-FIELD           PIC X(30).
         02 ADDRESS-FIELD             USAGE POINTER.
         02 LINE-FIELD                PIC X(6).
         02 FILLER-FIELD              PIC X(50).


        WORKING-STORAGE SECTION.
        01 CAR-VALUE.
          02 FILLER                   PIC X(10) VALUE "equal".
          02 FILLER                   PIC X(10) VALUE "less ".
          02 FILLER                   PIC X(10) VALUE "major".
          02 FILLER                   PIC X(10) VALUE "not >".
          02 FILLER                   PIC X(10) VALUE "not <".
          02 FILLER                   PIC X(10) VALUE "change".
        01 TAB-VALUE REDEFINES CAR-VALUE.
          02 TESTO                    PIC X(10) OCCURS 6 TIMES.

        01 EDIT-SIZE                  PIC ZZZZ9  VALUE ZEROS.
        01 EOF-DO                     PIC X      VALUE SPACE.
        01 EOF-FIELD                  PIC X      VALUE SPACE.
        01 FILE-FIELD                 PIC X(200) VALUE SPACE.
        01 STATUS-FIELD               PIC XX     VALUE SPACE.
        01 OFFSET-CAMPO               PIC 9(5)   VALUE ZEROS.
        01 FILE-DO                    PIC X(200) VALUE SPACE.
        01 STATUS-DO                  PIC XX     VALUE SPACE.
        01 CHARBYTE.
            02 NUMBYTE                USAGE BINARY-CHAR.
        01 IND                        PIC 999 VALUE ZEROS.
        01 IND1                       PIC 999 VALUE ZEROS.
        01 IND2                       PIC 999 VALUE ZEROS.
        01 IND3                       PIC 999 VALUE ZEROS.
        01 STRINGA                    PIC X(100) VALUE SPACE.
        01 COMANDO                    PIC X(100) VALUE SPACE.
        01 MESSAGGIO                  PIC X(100) VALUE SPACE.
        01 XX                         PIC X.

        78 A1                         VALUE X"20".
        78 A2                         VALUE X"20".
        78 A3                         VALUE X"20".
        78 A4                         VALUE X"20".
        78 T1                         VALUE X"20".
        78 T2                         VALUE X"20".
        78 LO                         VALUE X"20".
        78 LV                         VALUE X"20".

           COPY "screenio.cpy".

        78 MAX-ROW-COUNT               VALUE 16.

        01 PARAMETRI-CONVERSIONE.
         02 TIPO-CONVERSIONE           PIC X.
         02 NUMERO-DECIMALE            PIC 9(6).
         02 CIFRA-BINARIA.
            05 DIGIT                   PIC 9 OCCURS 16 TIMES.
         02 SPEZZA-BINARIA REDEFINES CIFRA-BINARIA.
            05 PARTE-BINARIA           PIC X(4) OCCURS 4 TIMES.
         02 VALORE-HEX.
            05 HEX                     PIC X OCCURS 4  TIMES.

        01 RIGA-VIDEO                 PIC X(56).
        01 TAB-HEX                    PIC X(100) VALUE SPACE.
        01 DATA-WORK                  PIC X(1024) BASED.
        01 MAX                        PIC 9(5)   VALUE ZERO.
        01 MAX1                       PIC 9(5)   VALUE ZERO.
        01 PATH-INF                   PIC X(200) VALUE SPACE.
        01 TAB-SCREEN.
           02 ELEMENTI-TAB OCCURS 16 TIMES.
            05 ELEM-TAB               PIC X(30).
           02 RIGA-TAB                PIC 99     VALUE ZEROS.
           02 IND-TAB                 PIC 99     VALUE ZEROS.
           02 MAX-TAB                 PIC 99     VALUE ZEROS.
           02 SIZE-SCREEN             PIC 99     VALUE ZEROS.
        01 FILLER                     PIC X.
           88 KEY-VALID                          VALUE 'S'.
           88 KEY-INVALID                        VALUE SPACE.

      *> Buffer for lines in the variable list.  MIN and MAX
      *> referance the lowest and highest values currently in the
      *> buffer.  TOP and BOTTOM reference the variables shown
      *> at the top and bottom of the displayed list.
        78 VAR-LINE-CAPCY                        VALUE 48.
        78 VAR-LINE-MAX                          VALUE 16.

        01 VAR-PANEL.
           02 VAR-IND                 PIC S9(4) COMP-5.
           02 VAR-CURR                PIC S9(4) COMP-5.
           02 VAR-MIN                 PIC S9(4) COMP-5.
           02 VAR-MAX                 PIC S9(4) COMP-5.
           02 VAR-TOP                 PIC S9(4) COMP-5.
           02 VAR-BOTTOM              PIC S9(4) COMP-5.
           02 VAR-TEMP                PIC S9(4) COMP-5.
           02 VAR-COUNT               PIC S9(4) COMP-5.
           02 FILLER                  PIC X.
              88 VAR-FORWARD                    VALUE 'F'.
              88 VAR-BACKWARD                   VALUE 'B'.
              88 VAR-NOWHERE                    VALUE SPACE.
           02 VAR-LINE OCCURS VAR-LINE-CAPCY TIMES.
              05 VAR-NAME             PIC X(30).
              05 VAR-BRK-COND         PIC X.
                 88 VAR-BRK-NOT-SET VALUE SPACE.
                 88 VAR-BRK-SET     VALUES '1' THRU '6'.
              05 VAR-BRK-COND-9 REDEFINES VAR-BRK-COND
                                      PIC 9.
              05 VAR-BRK-VAL          PIC x(12).

                 COPY "workanim.cpy".

        LINKAGE SECTION.
        01 TIPO-CAMPO                 PIC X.
        01 INDICE-CAMPO               PIC 9(4).
        01 NOME-CAMPO                 PIC X(30).
        01 FILE-CAMPO                 PIC X(06).
        01 ADDRESS-CAMPO              USAGE POINTER.
        01 LENGTH-CAMPO               PIC 9(9) COMP-5.

        PROCEDURE DIVISION USING TIPO-CAMPO
                                 INDICE-CAMPO
                                 NOME-CAMPO
                                 FILE-CAMPO
                                 ADDRESS-CAMPO
                                 LENGTH-CAMPO.


       INIZIO SECTION.
      *
      * TIPO-CAMPO: define the action to perform, first time it assume the
      * "A" (accept) value then you can choice a name into the box or using
      * the list field for available names, then the source (in the animate)
      * move the address of the specified field into the ADDRESS-CAMPO
      * (see the do-anim.cpy)  and return the control this build passing the
      * "V" (visualize the value) switch.
      *

           INITIALIZE PARAMETRI-CONVERSIONE.

           PERFORM VISUALIZZA.
           PERFORM CERCA-DO.

           OPEN I-O ARK-FIELD.

           IF STATUS-FIELD  NOT = "00"
              MOVE SPACES     TO MESSAGGIO
              string "status:" STATUS-FIELD " su:" DELIMITED BY SIZE
                FILE-FIELD DELIMITED BY "   "
                INTO MESSAGGIO
              DISPLAY MESSAGGIO
              STOP RUN
           END-IF.

       AC-KEY.

           IF TIPO-CAMPO = "V"
              PERFORM LETTURA
           ELSE
              ACCEPT NOME-CAMPO LINE 07 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT

              IF NOME-CAMPO > SPACE
                 MOVE  FUNCTION UPPER-CASE(NOME-CAMPO) TO NOME-FIELD

                 READ ARK-FIELD KEY IS CHIAVE-FIELD
                   INVALID KEY
                      GO TO AC-KEY
                 END-READ
              END-IF
           END-IF.


           MOVE FUNCTION UPPER-CASE(NOME-CAMPO) TO NOME-CAMPO.

           DISPLAY NOME-CAMPO LINE 07 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF TIPO-CAMPO = "A"
           AND ( NOME-CAMPO = SPACES
                 OR COB-CRT-STATUS = COB-SCR-F5 )
              PERFORM LIST-FIELDS
              PERFORM VISUALIZZA
              GO TO AC-KEY
           END-IF.

           IF COB-CRT-STATUS =  COB-SCR-F4
              PERFORM REMOVE-BREAK
              MOVE COB-SCR-F3          TO COB-CRT-STATUS
              GO TO AC-KEY
           END-IF.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE SPACES TO NOME-CAMPO
              DISPLAY NOME-CAMPO LINE 07 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                     BACKGROUND-COLOR COB-COLOR-BLUE
              GO TO FINE
           END-IF.

           IF TIPO-CAMPO = "A"
              ACCEPT INDICE-CAMPO LINE 09 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT
           END-IF.

           DISPLAY INDICE-CAMPO LINE 09 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE SPACES TO NOME-CAMPO
              DISPLAY NOME-CAMPO LINE 07 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                     BACKGROUND-COLOR COB-COLOR-BLUE
              GO TO FINE
           END-IF.

           IF TIPO-CAMPO = "A"
           AND COB-CRT-STATUS = COB-SCR-F5
              PERFORM LIST-FIELDS
      *       MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
              PERFORM VISUALIZZA
      *       MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
              GO TO AC-KEY
           END-IF.

           IF TIPO-CAMPO = "A"
           AND COB-CRT-STATUS = COB-SCR-F6
              PERFORM LIST-FIELDS
      *       MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
              PERFORM VISUALIZZA
      *       MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
              GO TO AC-KEY
           END-IF.


           IF TIPO-CAMPO = "A"
              ACCEPT OFFSET-CAMPO LINE 09 COL 47
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT
           END-IF.

           IF OFFSET-CAMPO = ZEROS
              MOVE 1 TO OFFSET-CAMPO
           END-IF.

           MOVE  SIZE-FIELD           TO MAX.

           IF OFFSET-CAMPO > MAX
              MOVE 1  TO OFFSET-CAMPO
           END-IF.

           IF OFFSET-CAMPO > 1
              SUBTRACT OFFSET-CAMPO FROM MAX
           END-IF.

           IF MAX > 30
              MOVE 30 TO MAX
           END-IF.

           DISPLAY OFFSET-CAMPO LINE 09 COL 47
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE ZEROS TO COB-CRT-STATUS
              GO TO AC-KEY
           END-IF.


           IF TIPO-CAMPO = "A"
              DISPLAY "............................."
                                LINE 11 COL 33  WITH LOWLIGHT
           ELSE
              PERFORM VEDI-HEX

              ACCEPT DATA-WORK(OFFSET-CAMPO:MAX) LINE 11 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT
           END-IF.


           DISPLAY DATA-WORK(OFFSET-CAMPO:MAX) LINE 11 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE ZEROS TO COB-CRT-STATUS
              GO TO FINE
           END-IF.

       AC-BREAK.

           IF TIPO-CAMPO = "V"
              ACCEPT BREAK-FIELD   LINE 17 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT

              IF NOT BREAK-IF-OK
                 MOVE " " TO BREAK-FIELD
              END-IF

              REWRITE REC-FIELD
           END-IF.

           DISPLAY BREAK-FIELD   LINE 17 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE ZEROS TO COB-CRT-STATUS
              GO TO AC-KEY
           END-IF.

       AC-COMPARE.

           IF OFFSET-CAMPO = ZEROS
              MOVE 1 TO OFFSET-CAMPO
           END-IF.


           IF TIPO-CAMPO = "V"
              ACCEPT VALUE-BREAK-FIELD(1:30)  LINE 19 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT

              REWRITE REC-FIELD
           END-IF.

           DISPLAY VALUE-BREAK-FIELD(1:30)  LINE 19 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE ZEROS TO COB-CRT-STATUS
              GO TO AC-BREAK
           END-IF.

       AC-WHERE.

           IF TIPO-CAMPO = "V"
              MOVE TPD-LINE                  TO LINE-FIELD
              ACCEPT LINE-FIELD               LINE 21 COL 33
                WITH FOREGROUND-COLOR COB-COLOR-BLUE UPDATE
                     BACKGROUND-COLOR COB-COLOR-WHITE LOWLIGHT

              REWRITE REC-FIELD
           END-IF.

           DISPLAY LINE-FIELD               LINE 21 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           IF COB-CRT-STATUS =  COB-SCR-F3
              MOVE ZEROS TO COB-CRT-STATUS
              GO TO AC-COMPARE
           END-IF.


       FINE.

           CLOSE ARK-FIELD.
           GOBACK.


      *> Open a picklist for the user to select a variable for display.
       LIST-FIELDS.

           INITIALIZE VAR-PANEL.

           MOVE NOME-CAMPO            TO NOME-FIELD.
           START ARK-FIELD KEY IS NOT < CHIAVE-FIELD
              INVALID KEY
                 SET KEY-INVALID TO TRUE
              NOT INVALID KEY
                 SET KEY-VALID TO TRUE
           END-START.

           IF KEY-INVALID
              EXIT PARAGRAPH   *> nothing to list
           END-IF.

      *>   Load a panel's worth of variables into the table
           MOVE 1     TO VAR-MIN
                         VAR-TOP.
           MOVE SPACE TO EOF-FIELD.
           PERFORM VARYING VAR-IND FROM 1 BY 1
            UNTIL VAR-IND > 16
            OR EOF-FIELD = 'S'
              READ ARK-FIELD NEXT RECORD
                AT END
                   MOVE "S" TO EOF-FIELD
              END-READ

              IF EOF-FIELD = SPACE
                 MOVE NOME-FIELD      TO VAR-NAME     ( VAR-IND )
                 MOVE BREAK-FIELD     TO VAR-BRK-COND ( VAR-IND )
                 MOVE VALUE-BREAK-FIELD (1:12)
                                      TO VAR-BRK-VAL  ( VAR-IND )
                 MOVE VAR-IND TO VAR-MAX
                                 VAR-BOTTOM
              END-IF
           END-PERFORM.

           SET VAR-FORWARD TO TRUE.

      *>   Display the stored lines
           PERFORM DO-FORM.
           DISPLAY "Field name ....                "
                 & "Set break if Condition "
             LINE 7 COL 20
             WITH FOREGROUND-COLOR COB-COLOR-CYAN REVERSE-VIDEO
                  BACKGROUND-COLOR COB-COLOR-BLACK.

           MOVE +1 TO VAR-CURR.
           PERFORM DISPLAY-VAR-LINES.

           PERFORM FOREVER
              ACCEPT XX  LINE 25 COL 79 WITH AUTO
              EVALUATE COB-CRT-STATUS
                 WHEN COB-SCR-OK
                    MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
                    EXIT PERFORM
                 WHEN COB-SCR-F3
                    MOVE VAR-NAME( VAR-CURR ) TO NOME-CAMPO
                    EXIT PERFORM
                 WHEN COB-SCR-F4          *> cancel
                    EXIT PERFORM
                 WHEN COB-SCR-F7
                    PERFORM PAGE-UP
                 WHEN COB-SCR-F8
                    PERFORM PAGE-DOWN
                 WHEN COB-SCR-ESC         *> cancel
                    EXIT PERFORM
                 WHEN COB-SCR-PAGE_UP
                    PERFORM PAGE-UP
                 WHEN COB-SCR-PAGE_DOWN
                    PERFORM PAGE-DOWN
                 WHEN COB-SCR-KEY-UP
                    PERFORM KEY-UP
                 WHEN COB-SCR-KEY-DOWN
                    PERFORM KEY-DOWN
                 WHEN OTHER
                    CONTINUE
              END-EVALUATE
           END-PERFORM.


      *> Respond to page-up key (or F7).
       PAGE-UP.
      *>   Save the current screen position of the highlighted line.
      *>   Later we'll restore the highlighting to the same line.
           MOVE VAR-CURR TO VAR-IND.
           PERFORM CALC-RIGA-TAB.
           MOVE RIGA-TAB TO VAR-TEMP.

           MOVE VAR-TOP TO VAR-CURR.
           IF VAR-FORWARD
      *>      If we'e been going forward, going backward
      *>      won't encounter end-of-file.
              MOVE SPACE TO EOF-FIELD
           END-IF.

           PERFORM VAR-LINE-MAX TIMES
              PERFORM KEY-UP
              IF EOF-FIELD = 'S'
                 EXIT PERFORM
              END-IF
           END-PERFORM.

      *>   Return the highlighting to the saved position.
           PERFORM RESTORE-VAR-CURR.
           PERFORM DISPLAY-VAR-LINES.

      *> Respond to up arrow.
       KEY-UP.
           IF VAR-CURR = VAR-TOP   *> if already at top of panel
              PERFORM SCROLL-UP
              PERFORM DISPLAY-VAR-LINES
           ELSE
              MOVE VAR-CURR  TO VAR-IND
              PERFORM DECREMENT-CURR-LINE
              PERFORM DISPLAY-ONE-VAR-LINE

              MOVE VAR-CURR TO VAR-IND
              PERFORM DISPLAY-ONE-VAR-LINE
           END-IF.

      *> Read the previous variable into the array.  Juggle the
      *> indexes as needed to shift the display window, but
      *> don't display anything yet.
       SCROLL-UP.

           IF VAR-TOP = VAR-MIN
      *>      Read the previous record from the file
              IF NOT VAR-BACKWARD
                 MOVE VAR-NAME ( VAR-CURR ) TO NOME-FIELD
                 START ARK-FIELD KEY IS < CHIAVE-FIELD
                    INVALID KEY
                       SET KEY-INVALID TO TRUE
                    NOT INVALID KEY
                       SET KEY-VALID TO TRUE
                 END-START

                 IF KEY-VALID
                    SET VAR-BACKWARD TO TRUE
                 ELSE
                    SET VAR-NOWHERE TO TRUE
                    EXIT PARAGRAPH     *> No more variables
                 END-IF
              END-IF

              READ ARK-FIELD PREVIOUS RECORD
                AT END
                   MOVE "S" TO EOF-FIELD
                NOT AT END
                   MOVE SPACE TO EOF-FIELD
              END-READ

              IF EOF-FIELD = "S"
                 EXIT PARAGRAPH  *> No more variables
              ELSE
                 PERFORM DECREMENT-VAR-WINDOW
                 MOVE NOME-FIELD      TO VAR-NAME     ( VAR-CURR )
                 MOVE BREAK-FIELD     TO VAR-BRK-COND ( VAR-CURR )
                 MOVE VALUE-BREAK-FIELD (1:12)
                                      TO VAR-BRK-VAL  ( VAR-CURR )
              END-IF
           ELSE
      *>      The previous variable is already cached
              PERFORM DECREMENT-VAR-WINDOW
           END-IF.

      *> Shift the displayed range down by one within the array
      *> of variables; wrap around as needed.
       DECREMENT-VAR-WINDOW.
           
      *>   Determine how many variables on currently on-screen
           COMPUTE VAR-COUNT = VAR-BOTTOM - VAR-TOP + 1.
           IF VAR-COUNT <= 0
              ADD VAR-LINE-CAPCY TO VAR-COUNT
           END-IF.
 
           IF VAR-TOP > +1
              SUBTRACT +1 FROM VAR-TOP
           ELSE
              MOVE VAR-LINE-CAPCY TO VAR-TOP       *> wrap-around
           END-IF.

           IF VAR-LINE-MAX > VAR-COUNT
      *>      There's more room available on the screen; no need
      *>      to adjust VAR-BOTTOM.
              CONTINUE
           ELSE
      *>      The screen is full; remove the bottom line
      *>      from the displayed range.
              IF VAR-BOTTOM > +1
                 SUBTRACT +1 FROM VAR-BOTTOM
              ELSE
                 MOVE VAR-LINE-CAPCY TO VAR-BOTTOM    *> wrap-around
              END-IF
           END-IF.

           IF VAR-CURR = VAR-MIN
              IF VAR-MIN > +1
                 SUBTRACT +1 FROM VAR-MIN
              ELSE
                 MOVE VAR-LINE-CAPCY TO VAR-MIN    *> wrap-around
              END-IF
           END-IF.
 
           IF VAR-MAX = VAR-MIN
      *       The array is full; bump down the upper bound.
              IF VAR-MAX > +1
                 SUBTRACT +1 FROM VAR-MAX
              ELSE
                 MOVE VAR-LINE-CAPCY TO VAR-MAX   *> wrap-around
              END-IF
           END-IF.

           PERFORM DECREMENT-CURR-LINE.

       DECREMENT-CURR-LINE.

           IF VAR-CURR > 1
              SUBTRACT +1 FROM VAR-CURR
           ELSE
              MOVE VAR-LINE-CAPCY TO VAR-CURR   *> wrap-around
           END-IF.


      *> Respond to page-down key (or F8).
       PAGE-DOWN.
      *>   Save the current screen position of the highlighted line.
      *>   Later we'll restore the highlighting to the same line.
           MOVE VAR-CURR TO VAR-IND.
           PERFORM CALC-RIGA-TAB.
           MOVE RIGA-TAB TO VAR-TEMP.

           MOVE VAR-BOTTOM TO VAR-CURR.
           IF VAR-BACKWARD
      *>      If we'e been going backward, going forward
      *>      won't encounter end-of-file.
              MOVE SPACE TO EOF-FIELD
           END-IF.

           PERFORM VAR-LINE-MAX TIMES
              PERFORM KEY-DOWN
              IF EOF-FIELD = 'S'
                 EXIT PERFORM
              END-IF
           END-PERFORM.

      *>   Return the highlighting to the saved position.
           PERFORM RESTORE-VAR-CURR.
           PERFORM DISPLAY-VAR-LINES.

      *> Respond to down arrow.
       KEY-DOWN.
           IF VAR-CURR = VAR-BOTTOM
              PERFORM SCROLL-DOWN
              PERFORM DISPLAY-VAR-LINES
           ELSE
              MOVE VAR-CURR  TO VAR-IND
              PERFORM INCREMENT-CURR-LINE
              PERFORM DISPLAY-ONE-VAR-LINE

              MOVE VAR-CURR TO VAR-IND
              PERFORM DISPLAY-ONE-VAR-LINE
           END-IF.

      *> Read the next variable into the array.  Juggle the
      *> indexes as needed to shift the display window, but
      *> don't display anything yet.
       SCROLL-DOWN.

           IF VAR-BOTTOM = VAR-MAX
      *>      Read the next record from the file
              IF NOT VAR-FORWARD
                 MOVE VAR-NAME ( VAR-CURR ) TO NOME-FIELD
                 START ARK-FIELD KEY IS > CHIAVE-FIELD
                    INVALID KEY
                       SET KEY-INVALID TO TRUE
                    NOT INVALID KEY
                       SET KEY-VALID TO TRUE
                 END-START

                 IF KEY-VALID
                    SET VAR-FORWARD TO TRUE
                 ELSE
                    SET VAR-NOWHERE TO TRUE
                    EXIT PARAGRAPH     *> No more variables
                 END-IF
              END-IF

              READ ARK-FIELD NEXT RECORD
                AT END
                   MOVE "S" TO EOF-FIELD
                NOT AT END
                   MOVE SPACE TO EOF-FIELD
              END-READ

              IF EOF-FIELD = "S"
                 EXIT PARAGRAPH  *> No more variables
              ELSE
                 PERFORM INCREMENT-VAR-WINDOW
                 MOVE NOME-FIELD      TO VAR-NAME     ( VAR-CURR )
                 MOVE BREAK-FIELD     TO VAR-BRK-COND ( VAR-CURR )
                 MOVE VALUE-BREAK-FIELD (1:12)
                                      TO VAR-BRK-VAL  ( VAR-CURR )
              END-IF
           ELSE
      *>      The next variable is already cached
              PERFORM INCREMENT-VAR-WINDOW
           END-IF.


      *> Shift the displayed range up by one within the array
      *> of variables; wrap around as needed.
       INCREMENT-VAR-WINDOW.
           
           IF VAR-TOP < VAR-LINE-CAPCY
              ADD  +1 TO VAR-TOP
           ELSE
              MOVE +1 TO VAR-TOP       *> wrap-around
           END-IF.

           IF VAR-BOTTOM < VAR-LINE-CAPCY
              ADD  +1 TO VAR-BOTTOM
           ELSE
              MOVE +1 TO VAR-BOTTOM    *> wrap-around
           END-IF.

           IF VAR-CURR = VAR-MAX
              IF VAR-MAX < VAR-LINE-CAPCY
                 ADD  +1 TO VAR-MAX
              ELSE
                 MOVE +1 TO VAR-MAX    *> wrap-around
              END-IF
           END-IF.
 
           IF VAR-MAX = VAR-MIN
      *       The array is full; bump up the lower bound.
              IF VAR-MIN < VAR-LINE-CAPCY
                 ADD  +1 TO VAR-MIN
              ELSE
                 MOVE +1 TO VAR-MIN    *> wrap-around
              END-IF
           END-IF.

           PERFORM INCREMENT-CURR-LINE.


       INCREMENT-CURR-LINE.

           IF VAR-CURR < VAR-LINE-CAPCY
              ADD  +1 TO VAR-CURR
           ELSE
              MOVE +1 TO VAR-CURR      *> wrap-around
           END-IF.


      *> Populate each screen line with a variable, or leave it
      *> empty if we're past the end of the list.
       DISPLAY-VAR-LINES.

           MOVE ALL SPACES           TO RIGA-VIDEO.
           MOVE LV                   TO RIGA-VIDEO(1:1).
           MOVE LV                   TO RIGA-VIDEO(56:1).

           MOVE VAR-TOP              TO VAR-IND.

           PERFORM VAR-LINE-MAX TIMES
              PERFORM DISPLAY-ONE-VAR-LINE

              IF VAR-IND < VAR-LINE-CAPCY
                 ADD  +1 TO VAR-IND
              ELSE
                 MOVE +1 TO VAR-IND
              END-IF
           END-PERFORM.


      *> Display a single variable (as designated by var-ind) on the
      *> variable list, with information on any applicable break.
       DISPLAY-ONE-VAR-LINE.

           PERFORM CALC-RIGA-TAB.    *> Calculate physical screen line

           IF VAR-BOTTOM >= VAR-TOP
           AND VAR-IND > VAR-BOTTOM
              PERFORM BLANK-OUT-VAR-LINE
           ELSE IF VAR-BOTTOM  < VAR-TOP
           AND VAR-IND > VAR-BOTTOM
           AND VAR-IND < VAR-TOP
              PERFORM BLANK-OUT-VAR-LINE
           ELSE                               *>> display line
              IF VAR-IND = VAR-CURR
                 DISPLAY VAR-NAME( VAR-IND )  *> highlighted
                   LINE RIGA-TAB COL 20
                   WITH FOREGROUND-COLOR COB-COLOR-WHITE REVERSE-VIDEO
                     BACKGROUND-COLOR COB-COLOR-BLACK
              ELSE                            *> not highlighted
                 DISPLAY VAR-NAME (VAR-IND)
                   LINE RIGA-TAB COL 20
                   WITH FOREGROUND-COLOR COB-COLOR-WHITE
                        BACKGROUND-COLOR COB-COLOR-BLACK
              END-IF

              IF VAR-BRK-SET( VAR-IND )       *> show break condition
                 DISPLAY TESTO( VAR-BRK-COND-9( VAR-IND ))
                   LINE RIGA-TAB COL 51
                   WITH FOREGROUND-COLOR COB-COLOR-WHITE
                        BACKGROUND-COLOR COB-COLOR-BLACK
                 DISPLAY VAR-BRK-VAL( VAR-BRK-COND-9( VAR-IND ))
                   LINE RIGA-TAB COL 62
                   WITH FOREGROUND-COLOR COB-COLOR-WHITE
                        BACKGROUND-COLOR COB-COLOR-BLACK
              ELSE
                 DISPLAY "                       "
                   LINE RIGA-TAB COL 51
                   WITH FOREGROUND-COLOR COB-COLOR-BLUE
                        BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                        REVERSE-VIDEO
              END-IF
           END-IF.

      *> Display a blank line at line riga-tab.
       BLANK-OUT-VAR-LINE.
           DISPLAY RIGA-VIDEO
             LINE RIGA-TAB COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                  REVERSE-VIDEO.

      *> Calculate the physical screen line corresponding to var-ind.
       CALC-RIGA-TAB.
 
           IF VAR-IND < VAR-TOP
              COMPUTE RIGA-TAB =
                VAR-IND + VAR-LINE-CAPCY - VAR-TOP + 8
           ELSE
              COMPUTE RIGA-TAB = VAR-IND - VAR-TOP + 8
           END-IF.

      *> From a screen position saved in var-temp, calculate the
      *> corresponding index.  Leave the result in var-curr.
       RESTORE-VAR-CURR.

           SUBTRACT 8 FROM VAR-TEMP GIVING VAR-CURR.
           ADD VAR-TOP TO VAR-CURR.
           IF VAR-CURR > VAR-LINE-CAPCY
              SUBTRACT VAR-LINE-CAPCY FROM VAR-CURR
           END-IF.


       LETTURA.

           MOVE ADDRESS-CAMPO TO ADDRESS OF DATA-WORK.
           MOVE NOME-CAMPO    TO NOME-FIELD.

           READ ARK-FIELD KEY IS CHIAVE-FIELD
             INVALID KEY
                SET KEY-INVALID TO TRUE
             NOT INVALID KEY
                SET KEY-VALID   TO TRUE
           END-READ.

           IF KEY-INVALID
              MOVE 1                     TO SIZE-FIELD
              MOVE TPD-LINE              TO LINE-FIELD
           ELSE
              IF LINE-FIELD = SPACES
              OR LINE-FIELD(1:6) NUMERIC
                 CONTINUE
              ELSE
                 MOVE TPD-LINE           TO LINE-FIELD
              END-IF

              MOVE LENGTH-CAMPO          TO SIZE-FIELD
              IF SIZE-FIELD > 1024
                 MOVE 1024               TO MAX1
              ELSE
                 MOVE SIZE-FIELD         TO MAX1
              END-IF

              MOVE DATA-WORK(1:MAX1)     TO VALUE-FIELD
              MOVE ADDRESS-CAMPO         TO ADDRESS-FIELD

              REWRITE REC-FIELD
           END-IF.

           DISPLAY NOME-CAMPO LINE 07 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           MOVE SIZE-FIELD   TO EDIT-SIZE.

           DISPLAY EDIT-SIZE  LINE 09 COL 58
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY INDICE-CAMPO LINE 09 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY BREAK-FIELD  LINE 17 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY VALUE-BREAK-FIELD(1:30) LINE 19 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY LINE-FIELD       LINE 21 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.


       VEDI-HEX.

           INITIALIZE TAB-HEX.
      ************************************************** THE hex pointer
      *
      *      DISPLAY ADDRESS-CAMPO  LINE 20 COL 34
      *        WITH FOREGROUND-COLOR COB-COLOR-WHITE
      *              BACKGROUND-COLOR COB-COLOR-GREEN.
      *******************************************************************

           MOVE 1          TO IND1.
           PERFORM VARYING IND FROM OFFSET-CAMPO BY 1
             UNTIL IND > MAX
              MOVE DATA-WORK(IND:1) TO CHARBYTE
              MOVE NUMBYTE   TO NUMERO-DECIMALE
              MOVE "B"       TO TIPO-CONVERSIONE
              CALL "tpbinar" USING PARAMETRI-CONVERSIONE

              MOVE VALORE-HEX(3:2) TO TAB-HEX(IND1:)

              IF IND NOT = MAX
                 ADD  2               TO IND1
                 MOVE "-"             TO TAB-HEX(IND1:1)
                 ADD  1               TO IND1
              END-IF
           END-PERFORM.

           DISPLAY TAB-HEX(1:30)  LINE 13 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY TAB-HEX(31:30) LINE 14 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.

           DISPLAY TAB-HEX(61:30) LINE 15 COL 33
             WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                  BACKGROUND-COLOR COB-COLOR-BLUE.


       DO-FORM.

           MOVE ALL LO               TO RIGA-VIDEO.
           MOVE A1                   TO RIGA-VIDEO(1:1).
           MOVE A3                   TO RIGA-VIDEO(56:1).

           DISPLAY RIGA-VIDEO        LINE 06 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-YELLOW
                  REVERSE-VIDEO.

           MOVE ALL SPACES           TO RIGA-VIDEO.
           MOVE LV                   TO RIGA-VIDEO(1:1).
           MOVE LV                   TO RIGA-VIDEO(56:1).

           PERFORM VARYING IND FROM 07 BY 1
             UNTIL IND > 24
              DISPLAY RIGA-VIDEO LINE IND COL 19
                WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                     REVERSE-VIDEO
           END-PERFORM.

           MOVE ALL LO               TO RIGA-VIDEO.
           MOVE A2                   TO RIGA-VIDEO(1:1).
           MOVE A4                   TO RIGA-VIDEO(56:1).

           DISPLAY RIGA-VIDEO        LINE 21 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-YELLOW
                  REVERSE-VIDEO.


       VISUALIZZA.

           PERFORM DO-FORM.

           DISPLAY "  Field name [                              ]"
                                      LINE 07 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "  Index      [    ] Offset [    ] Len.[     ]"
                                      LINE 09 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "  Value      [                              ]"
                                      LINE 11 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "  Hex.       [                              ]"
                                        LINE 13 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "             [                              ]"
                                      LINE 14 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "             [                              ]"
                                      LINE 15 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "  Break when [ ] (1=,2<,3>,4 no>,5 no<,6 diff"
                                      LINE 17 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "  Compare to [                              ]"
                                      LINE 19 COL 19
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY
             "  At cursor  [      ] (spaces for global)   ]   "
                                      LINE 21 COL 19

             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "F3" LINE 23 COL 21 WITH REVERSE-VIDEO.
           DISPLAY "=Done"  LINE 23 COL 23
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "F4" LINE 23 COL 29 WITH REVERSE-VIDEO.
           DISPLAY "=remove all BP" LINE 23 COL 31
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.

           DISPLAY "F5" LINE 23 COL 46 WITH REVERSE-VIDEO.
           DISPLAY "=list field" LINE 23 COL 48
             WITH FOREGROUND-COLOR COB-COLOR-BLUE
                  BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                  REVERSE-VIDEO.


       CERCA-DO.

           ACCEPT PATH-INF FROM ENVIRONMENT "tp-inf-path".

           IF PATH-INF > SPACES
              STRING PATH-INF delimited by "       "
                "animate.inf" delimited by size into file-do
           ELSE
              MOVE SPACES TO FILE-DO
              ACCEPT FILE-DO  FROM ENVIRONMENT "animate.inf"
              IF FILE-DO = SPACES
                MOVE "animate.inf"         TO FILE-DO
              END-IF
           END-IF.

           OPEN INPUT ARK-DO.

           IF STATUS-DO NOT = "00"    *> if open failed
              MOVE SPACES     TO MESSAGGIO
              string "Animate:file " DELIMITED BY SIZE
                FILE-DO DELIMITED BY "   "
                  " unavailable, check the configuration "
                DELIMITED BY SIZE
                INTO MESSAGGIO
              DISPLAY MESSAGGIO
                LINE 1 COL 01 WITH REVERSE-VIDEO
           ELSE
              PERFORM WITH TEST AFTER
                UNTIL EOF-DO = "S"

                 PERFORM READ-NEXT-DO
                 IF EOF-DO NOT = "S"
                    MOVE ZEROS  TO IND
                    MOVE SPACES TO STRINGA
                    INSPECT DATI-DO TALLYING IND FOR ALL "="
                    IF IND = 1
                       UNSTRING DATI-DO DELIMITED BY "="
                         INTO COMANDO STRINGA

                       EVALUATE COMANDO
                         WHEN "TEMP"
                            MOVE STRINGA TO PATH-TEMP
                            MOVE "S"     TO EOF-DO
                       END-EVALUATE
                    END-IF
                 END-IF

              END-PERFORM
           END-IF.

           CLOSE ARK-DO.

           MOVE SPACES     TO FILE-FIELD.
           STRING PATH-TEMP DELIMITED BY "     "
             FILE-CAMPO ".fld" DELIMITED BY SIZE
             INTO FILE-FIELD.


       READ-NEXT-DO.

           MOVE SPACES TO REC-DO EOF-DO.

           PERFORM WITH TEST AFTER
           UNTIL EOF-DO = "S"
           OR
           (
              DATI-DO(1:1) NOT = "*"
              AND DATI-DO  NOT = SPACES
           )
              READ ARK-DO NEXT RECORD
                AT END
                   MOVE "S"    TO EOF-DO
              END-READ
           END-PERFORM.


       REMOVE-BREAK.

           MOVE "1"         TO BREAK-FIELD.

           START ARK-FIELD KEY IS NOT < BREAK-FIELD
             INVALID KEY
                SET KEY-INVALID TO TRUE
             NOT INVALID KEY
                SET KEY-VALID   TO TRUE
           END-START.

           IF KEY-VALID
              MOVE SPACES     TO EOF-FIELD

              PERFORM UNTIL EOF-FIELD = "S"

                 READ ARK-FIELD NEXT RECORD
                   AT END
                      MOVE "S" TO EOF-FIELD
                   NOT AT END
                      MOVE SPACE TO EOF-FIELD
                 END-READ

                 IF EOF-FIELD NOT = "S"
                 AND BREAK-FIELD NOT =  SPACES
                    MOVE SPACES TO BREAK-FIELD
                    MOVE SPACES TO VALUE-BREAK-FIELD
                    REWRITE REC-FIELD
                 END-IF

              END-PERFORM
           END-IF.


        end program animdata.


        IDENTIFICATION DIVISION.
        PROGRAM-ID.    tpbinar.
      ******************************************************************
      *                                                                *
      * IL PROGRAMMA CONVERTE UN NUMERO DA BINARIO A DECIMALE E        *
      * VICEVERSA A SECONDO DEL TIPO-CONVERSIONEO.                     *
      * LANCIARE CON CALL "TP-BINAR" USING B o D nn bbbbbbbb XXXX      *
      * DOVE:B=converte il numero specificato in nn in una seq. di bit *
      *      D=converte la sequenza di bit in un numero decimale intero*
      *      nn=numero da convertire/convertito                        *
      *      bbbbbbbbbbbbbbbb=stringa di bit convertiti/da convertire  *
      *      XXXX=numero espresso in esadecimale                       *
      ******************************************************************
        ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
        SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
        DATA DIVISION.
        WORKING-STORAGE SECTION.

        77 BASE                        PIC 9     VALUE 2.
        77 IND                         PIC 999   VALUE ZEROS.
        77 IND1                        PIC 999   VALUE ZEROS.
        77 MAX                         PIC 99    VALUE 16.
        77 MOLTIPLICATORE              PIC 99    VALUE ZEROS.
        77 SALVA-NUMERO                PIC 9(06) VALUE ZEROS.
        01 SALVA-BINARIO.
         02 NIBBLE-BINARI.
            05 NIBBLE                  PIC X(4) OCCURS 4 TIMES.
      *
      ** TABELLA DI CONVERSIONE VELOCE IN ESADECIMALE
      *

        01 CARICA-ESADECIMALE.
           02 FILLER                  PIC X(15) VALUE "123456789ABCDEF".

        01 TABELLA-ESADECIMALE REDEFINES CARICA-ESADECIMALE.
           02 CAR-HEX                  PIC X     OCCURS 15 TIMES.

        LINKAGE SECTION.
      *
        01 PARAMETRI-CONVERSIONE.
         02 TIPO-CONVERSIONE           PIC X.
         02 NUMERO                     PIC 9(6).
         02 CIFRA-BINARIA.
            05 DIGIT                   PIC 9 OCCURS 16 TIMES.
         02 SPEZZA-BINARIA REDEFINES CIFRA-BINARIA.
            05 PARTE-BINARIA           PIC X(4) OCCURS 4 TIMES.
         02 VALORE-HEX.
            05 HEX                     PIC X OCCURS 4  TIMES.
      *
        PROCEDURE DIVISION USING PARAMETRI-CONVERSIONE.

        INIZIO.


            IF TIPO-CONVERSIONE = "D"
            PERFORM BIN-TO-DEC         THRU EX-BIN-TO-DEC.

            IF TIPO-CONVERSIONE = "B"
            PERFORM DEC-TO-BIN         THRU EX-DEC-TO-BIN.
      *
      ** viene restituito comunque in esadecimale
      *
            PERFORM BIN-TO-ESA         THRU EX-BIN-TO-ESA.

        FINE.
            GOBACK.
      *
      ** conversione della sequenza di bit in numero
      *
        BIN-TO-DEC.

            MOVE   MAX       TO IND.
            MOVE   ZEROS     TO MOLTIPLICATORE.
            MOVE   ZEROS     TO NUMERO.

            PERFORM VARYING IND FROM MAX BY -1 UNTIL IND = ZERO

            IF DIGIT(IND) NOT NUMERIC
             MOVE ZEROS TO DIGIT(IND)
            END-IF

            COMPUTE NUMERO = NUMERO +
             ( DIGIT(IND) * BASE ** MOLTIPLICATORE )
            END-COMPUTE

            ADD    1         TO MOLTIPLICATORE

            END-PERFORM.

        EX-BIN-TO-DEC.
            EXIT.
      *
      ** conversione di un numero in una sequenza di bit
      *
        DEC-TO-BIN.

            MOVE   ALL SPACES          TO CIFRA-BINARIA.

            PERFORM VARYING IND FROM MAX BY -1 UNTIL NUMERO = ZEROS

            DIVIDE NUMERO BY BASE GIVING NUMERO
             REMAINDER DIGIT (IND)
            END-PERFORM

            INSPECT CIFRA-BINARIA REPLACING ALL " " BY "0".

        EX-DEC-TO-BIN.
            EXIT.
      *
      ** conversione di un numero in esadecimale
      *
         BIN-TO-ESA.

            MOVE CIFRA-BINARIA          TO SALVA-BINARIO.
            MOVE NUMERO                 TO SALVA-NUMERO.

            MOVE 4                      TO IND1.
            MOVE ZEROS                  TO NUMERO.

            PERFORM VARYING IND1 FROM 4 BY -1 UNTIL IND1 = ZEROS

            MOVE SPACES                 TO CIFRA-BINARIA
            MOVE NIBBLE(IND1)           TO PARTE-BINARIA(4)
            PERFORM BIN-TO-DEC          THRU EX-BIN-TO-DEC
            IF NUMERO  = ZEROS MOVE ZEROS TO HEX(IND1)
             ELSE
            MOVE CAR-HEX(NUMERO)        TO HEX(IND1)
            END-IF

            END-PERFORM.

            MOVE SALVA-BINARIO          TO CIFRA-BINARIA.
            MOVE SALVA-NUMERO           TO NUMERO.

         EX-BIN-TO-ESA.
            EXIT.

        end program tpbinar.
