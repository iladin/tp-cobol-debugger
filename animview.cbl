        IDENTIFICATION DIVISION.
        PROGRAM-ID.    animview.
      *---------------------------------------------------------------------
      *
      * ANIMATORE INTERATTIVO CODICE OPENCOBOL   0.1.9.008
      *
      * Copyright (C) 2010-2012 Federico Priolo T&P Soluzioni Informatiche Srl
      *                    priolof@tp-srl.it
      *
      * This program is free software; you can redistribute it and/or modify
      * it under the terms of the GNU General Public License as published by
      * the Free Software Foundation; either version 2, or (at your option)
      * any later version.
      *
      * This program is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU General Public License for more details.
      *
      * You should have received a COPY of the GNU General Public License
      * along with this software; see the file COPYING.  If not, write to
      * the Free Software Foundation, 51 Franklin Street, Fifth Floor
      * Boston, MA 02110-1301 USA
      *---------------------------------------------------------------------
        ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
        SOURCE-COMPUTER.        PC-IBM.
        OBJECT-COMPUTER.        PC-IBM.
        SPECIAL-NAMES.

           DECIMAL-POINT IS COMMA.

        INPUT-OUTPUT SECTION.
        FILE-CONTROL.



        DATA DIVISION.
        FILE SECTION.


        WORKING-STORAGE SECTION.
        77 XX                PIC X.
        77 RIGA              PIC 99.
        77 IND               PIC 99.
        77 IND1              PIC 99.
        77 IND2              PIC 99.
        77 IND3              PIC 99.
        77 VOLTE             PIC 99.
        77 BARRA             PIC X(70) VALUE SPACES.

           COPY "screenio.cpy".
           COPY "workanim.cpy".


        PROCEDURE DIVISION.

        INIZIO SECTION.

            MOVE SPACES               TO TPD-SW-ANIM.
            MOVE TPD-WATCH-ITEMS(1:)  TO VOLTE(1:).

            MOVE 10                   TO RIGA.

            DISPLAY
             BARRA
                                   LINE 08 COL 05
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.

            DISPLAY
             " Watch immediate BEFORE executing current line "
                                   LINE 08 COL 05
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-WHITE HIGHLIGHT
                      REVERSE-VIDEO.

            DISPLAY
             BARRA
                                   LINE 09 COL 05
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.

            PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > VOLTE

            PERFORM VISUALIZZA    THRU EX-VISUALIZZA

            END-PERFORM.

            DISPLAY
             BARRA
                                     LINE RIGA COL 05
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.

        FINE.

            ACCEPT XX      LINE 25 COL 79  WITH SECURE AUTO.
            GOBACK.


        VISUALIZZA.



            DISPLAY
             BARRA
                                     LINE RIGA COL 05
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.


             DISPLAY TPD-WATCH-TITLE(IND)
                                        LINE RIGA COL 06
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.

             IF TPD-WATCH-VALUE (IND) = SPACES
              MOVE "<unassigned>"      TO TPD-WATCH-VALUE (IND).

             IF TPD-WATCH-VALUE (IND) = LOW-VALUE
              MOVE "<low value>"       TO TPD-WATCH-VALUE (IND).

             IF TPD-WATCH-VALUE (IND) = HIGH-VALUE
              MOVE "<high value>"       TO TPD-WATCH-VALUE (IND).


             MOVE ZEROS TO IND1.

             INSPECT TPD-WATCH-VALUE(IND) TALLYING IND1 FOR ALL x"00"


             IF IND1 = LENGTH OF TPD-WATCH-VALUE(IND)
              MOVE "<all null  >"       TO TPD-WATCH-VALUE (IND).

             IF IND1 > ZEROS
              MOVE 37                  TO IND3
              perform varying IND2 FROM 1 BY 1 UNTIL IND2 >
                    LENGTH OF TPD-WATCH-VALUE(IND)

              IF TPD-WATCH-VALUE(IND)(IND2:1) = x"00" OR x"0D" OR "0A"
              DISPLAY "?"              LINE RIGA  COL IND3
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-WHITE
                      REVERSE-VIDEO
               ELSE
              DISPLAY TPD-WATCH-VALUE(IND) (IND2:1)
                                        LINE RIGA COL IND3
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO
              END-IF
              ADD 1          TO IND3

              END-PERFORM

             ELSE
             DISPLAY TPD-WATCH-VALUE(IND)
                                        LINE RIGA COL 37
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO.

              ADD 1 TO RIGA.

        EX-VISUALIZZA.
            EXIT.


        end program animview.
