@echo off
echo Compiling the Animator
cobc -x animator.cbl
cobc animate.cbl
cobc animdata.cbl
cobc analyzer.cbl
cobc animfind.cbl
cobc animview.cbl
goto fine

echo Installing the Animator

set animtarget c:\animate
md %animtarget%

xcopy animator.exe %animtarget% /y
xcopy animate.dll  %animtarget% /y
xcopy animdata.dll %animtarget% /y
xcopy analyzer.dll %animtarget% /y
xcopy animfind.dll %animtarget% /y
xcopy animview.dll %animtarget% /y

set cbl_library_path=%animtarget%
set animate.inf=%animtarget%
set animator.inf=%animtarget%

:fine
echo Testing the Animator
.\animator test.cbl -x
.\test
type telco.txt
