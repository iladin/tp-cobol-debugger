         01 TPD-FILE          PIC X(06)  VALUE SPACE.
         01 TPD-NAME          PIC X(30)  VALUE SPACE.
         01 TPD-INDEX         PIC 9(4)   VALUE ZEROS.
         01 TPD-DLL           PIC X(08)  VALUE "animate".
         01 TPD-VIEW          PIC X(08)  VALUE "animdata".
         01 TPD-FIND          PIC X(08)  VALUE "animfind".
         01 TPD-WATCH         PIC X(08)  VALUE "animview".
         01 TPD-LOCATE        PIC X(08)  VALUE "animloca".
         01 TPD-ADDRESS       USAGE POINTER.
         01 TPD-LENGTH        PIC 9(9)   COMP-5.
         01 TPD-ACTION        PIC X      IS EXTERNAL.
         01 TPD-SW-ANIM       PIC X      IS EXTERNAL.
         01 TPD-WHERE         PIC X(6)   IS EXTERNAL.
         01 TPD-SEARCH        PIC X(30)  IS EXTERNAL.
         01 TPD-CONTINUE      PIC X      IS EXTERNAL.
         01 TPD-LINE          PIC X(06)  IS EXTERNAL.
         01 TPD-RUN           PIC 999999 VALUE ZEROS.
         01 TPD-WAIT          PIC 999999 VALUE ZEROS.
         01 TPD-START         PIC X      VALUE SPACES.
         01 TPD-COPYRIGHT     PIC X(80)  VALUE
           "TP-COBOL-DEBUGGER release 2.2.0  Package 27-09-2016".
         01 TPD-COPYRIGHT1.
            02 FILLER         PIC X(80) VALUE
           "Copyright (C) 2010-2016 Federico Priolo "
           & "TP ONE SRL ".

         01 TPD-WATCH-TABLE   IS EXTERNAL.
           09 TPD-WATCH-ITEMS PIC 99.
           09 TPD-ITEMS OCCURS 10 TIMES.
            19 TPD-WATCH-TITLE PIC X(30).
            19 TPD-WATCH-VALUE PIC X(30).

         01 PATH-TEMP        PIC X(200) IS EXTERNAL.
         01 SECONDI          PIC 9(18) IS EXTERNAL.

001470  01 KEYS IS EXTERNAL.
001480   02 PAG-HOME                   PIC 9999.
001480   02 PAG-END                    PIC 9999.
001480   02 PAG-PREV                   PIC 9999.
001490   02 PAG-NEXT                   PIC 9999.
001500   02 LINE-PREV                  PIC 9999.
001510   02 LINE-NEXT                  PIC 9999.
001520   02 SET-BREAK.
001530    05 VAL-BREAK                 USAGE BINARY-CHAR.
001540   02 DO-GO.
001550    05 VAL-DO-GO                 USAGE BINARY-CHAR.
001560   02 STEP1.
001570    05 VAL-STEP1                 USAGE BINARY-CHAR.
001580   02 STEP2.
001590    05 VAL-STEP2                 USAGE BINARY-CHAR.
001600   02 STEP3.
001610    05 VAL-STEP3                 USAGE BINARY-CHAR.
001620   02 DOES-ANIM.
001630    05 VAL-ANIM                  USAGE BINARY-CHAR.
001640   02 DO-VIEW.
001650    05 VAL-VIEW                  USAGE BINARY-CHAR.
001660   02 DO-ESCAPE.
001670    05 VAL-DO-ESCAPE             USAGE BINARY-CHAR.
001660   02 DO-FIND.
001670    05 VAL-FIND                  USAGE BINARY-CHAR.
001660   02 DO-CONTINUE.
001670    05 VAL-CONTINUE              USAGE BINARY-CHAR.
001660   02 DOES-WATCH.
001670    05 VAL-WATCH                 USAGE BINARY-CHAR.
001660   02 DO-STRUCTURE.
001670    05 VAL-STRUCTURE             USAGE BINARY-CHAR.
001680
001690   02 ACC-COLOR-BACK             PIC 9.
001700   02 ACC-COLOR-FORE             PIC 9.
001710   02 REM-COLOR-BACK             PIC 9.
001720   02 REM-COLOR-FORE             PIC 9.
001730   02 LAB-COLOR-BACK             PIC 9.
001740   02 LAB-COLOR-FORE             PIC 9.
001750   02 NUL-COLOR-BACK             PIC 9.
001760   02 NUL-COLOR-FORE             PIC 9.
001770   02 WOK-COLOR-BACK             PIC 9.
001780   02 WOK-COLOR-FORE             PIC 9.
001790
