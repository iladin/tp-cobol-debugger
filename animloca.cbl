000010  IDENTIFICATION DIVISION.
000020  PROGRAM-ID.    animloca.
000030*---------------------------------------------------------------------
000040*
000050* ANIMATORE INTERATTIVO PER GNU-COBOL
000060*
000070* Copyright (C) 2010-2014 Federico Priolo T&P Soluzioni Informatiche Srl
000080*                    federico.priolo@tp-srl.it
000090*
000100* This program is free software; you can redistribute it and/or modify
000110* it under the terms of the GNU General Public License as published by
000120* the Free Software Foundation; either version 2, or (at your option)
000130* any later version.
000140*
000150* This program is distributed in the hope that it will be useful,
000160* but WITHOUT ANY WARRANTY; without even the implied warranty of
000170* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
000180* GNU General Public License for more details.
000190*
000200* You should have received a COPY of the GNU General Public License
000210* along with this software; see the file COPYING.  If not, write to
000220* the Free Software Foundation, 51 Franklin Street, Fifth Floor
000230* Boston, MA 02110-1301 USA
000240*---------------------------------------------------------------------
000250  ENVIRONMENT DIVISION.
000260  CONFIGURATION SECTION.
000270  SOURCE-COMPUTER.        PC-IBM.
000280  OBJECT-COMPUTER.        PC-IBM.
000290  SPECIAL-NAMES.
000300
000310     DECIMAL-POINT IS COMMA.
000320
000330  INPUT-OUTPUT SECTION.
000340  FILE-CONTROL.
000350
000360
000370          SELECT ARK-LIN ASSIGN TO FILE-LIN
000380           ORGANIZATION IS INDEXED
000390           ACCESS MODE  IS  DYNAMIC
000400           RECORD KEY   IS  CHIAVE-LIN
000080           LOCK MODE    IS AUTOMATIC WITH LOCK ON RECORD
000410           FILE STATUS  IS STATUS-LIN.

000370          SELECT ARK-LABEL ASSIGN TO FILE-LABEL
000380           ORGANIZATION IS INDEXED
000390           ACCESS MODE  IS  DYNAMIC
000400           RECORD KEY   IS  CHIAVE-LABEL
                 ALTERNATE RECORD KEY IS CHIAVE-SEC-LABEL
                 ALTERNATE RECORD KEY IS FIELD-LABEL DUPLICATES
000080           LOCK MODE    IS AUTOMATIC WITH LOCK ON RECORD
000410           FILE STATUS  IS STATUS-LABEL.
000420
000430
000470
000480  DATA DIVISION.
000490  FILE SECTION.
000500
000560  FD ARK-LABEL.
0       01 REC-LABEL.
          02 CHIAVE-LABEL.
           05 CHIAVE-POS-LABEL.
            09 PAG-LABEL             PIC 999.
            09 ROW-LABEL             PIC 999.
           05 NAME-LABEL             PIC X(30).
           05 FIELD-LABEL            PIC X(30).
          02 CHIAVE-SEC-LABEL.
           05 NAME-SEC-LABEL         PIC X(30).
           05 FIELD-SEC-LABEL        PIC X(30).
000550
000560  FD ARK-LIN.
000570  01 REC-LIN.
000580   02 CHIAVE-LIN.
000590    05 PAG-LIN                  PIC 9(4).
000600    05 ROW-LIN                  PIC 9(2).
000610   02 DATI-LIN                  PIC X(80).
000620   02 TIPO-LIN                  PIC X.
000630   02 BREAK-LIN                 PIC X.
000640   02 CODICE-LIN                PIC X.
000650   02 FILLER                    PIC X(19).
000660
000670
000680  WORKING-STORAGE SECTION.
        01 FIELDS.
          02 NAME-FIELD               PIC X(30) OCCURS 10 TIMES.
          02 MAX-FIELD                PIC 99.
        01 ALL-FIELDS                 PIC X(100) VALUE SPACE.
        01 DUMMY                      PIC X.
        01 COBOL-TEST                 PIC X(30) VALUE SPACES.
        01 COBOL-SIZE                 PIC 99 VALUE ZEROS.



        78 A1                         VALUE X"20".
        78 A2                         VALUE X"20".
        78 A3                         VALUE X"20".
        78 A4                         VALUE X"20".
        78 T1                         VALUE X"20".
        78 T2                         VALUE X"20".
        78 LO                         VALUE X"20".
        78 LV                         VALUE X"20".
        01 ROW-VIDEO                 PIC X(48).

000690  01 FILE-LABEL                 PIC X(200) VALUE SPACE.
000700  01 STATUS-LABEL               PIC XX     VALUE "00".
000690  01 FILE-FIELD                 PIC X(200) VALUE SPACE.
000700  01 STATUS-FIELD               PIC XX     VALUE "00".
000710  01 FILE-LIN                   PIC X(200) VALUE SPACE.
000720  01 STATUS-LIN                 PIC XX     VALUE "00".
000750  01 IND                        PIC 999    VALUE ZEROS.
000760  01 IND1                       PIC 999    VALUE ZEROS.
000760  01 IND2                       PIC 999    VALUE ZEROS.
000760  01 IND3                       PIC 999    VALUE ZEROS.
000760  01 IND4                       PIC 999    VALUE ZEROS.
0       01 PAGINA                     PIC 9(9)   VALUE ZEROS.
0       01 MAX-PAGINA                 PIC 9(9)   VALUE ZEROS.
0       01 RIGA                       PIC 9(9)   VALUE ZEROS.
        01 FINE-FILE                  PIC X      VALUE SPACE.
        01 WLABEL                     PIC X(30)  VALUE SPACE.
        01 LABEL-NAME                 PIC X(30)  VALUE SPACE.
        01 FIELD-NAME                 PIC X(30)  VALUE SPACE.
        88 FIELD-NAME-COBOL VALUE "THRU" "UPON" "CONSOLE" "ALL" "FOR".
000770  01 XX                         PIC X.
000780
000790     COPY "workanim.cpy".
000800     COPY "screenio.cpy".
000810
000820  LINKAGE SECTION.
000830  77 FILE-TO-OPEN                PIC X(200).
000840
000850  PROCEDURE DIVISION USING FILE-TO-OPEN.
000860
000870
000880  INIZIO SECTION.



000900      MOVE FILE-TO-OPEN         TO FILE-LIN.

            MOVE SPACES               TO FILE-LABEL.

            STRING FILE-TO-OPEN DELIMITED BY "     "
             "-labels" DELIMITED BY SIZE INTO FILE-LABEL.

000920      OPEN INPUT ARK-LIN.
000930
000990      IF STATUS-LIN NOT = "00"
001000       DISPLAY " Errore apertura (" LINE 25 COL 01
001420         WITH FOREGROUND-COLOR COB-COLOR-BLUE
001430               BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
001440                REVERSE-VIDEO
             DISPLAY
001010         STATUS-LIN LINE 25 COL 00

001420         WITH FOREGROUND-COLOR COB-COLOR-BLUE
001430               BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
001440                REVERSE-VIDEO
             DISPLAY
                 ") "  LINE 25 COL 00

001420         WITH FOREGROUND-COLOR COB-COLOR-BLUE
001430               BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
001440                REVERSE-VIDEO
             DISPLAY
                 FILE-LIN(1:40) LINE 25 COL 00
001420         WITH FOREGROUND-COLOR COB-COLOR-BLUE
001430               BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
001440                REVERSE-VIDEO
001020          ACCEPT STATUS-LIN
001030             STOP RUN.

            OPEN OUTPUT ARK-LABEL
            CLOSE ARK-LABEL

            OPEN I-O ARK-LABEL

            MOVE 1 TO PAGINA.
            MOVE 7 TO RIGA.


001670      INITIALIZE REC-LIN.
001680
001690      MOVE LOW-VALUE   TO CHIAVE-LIN.
001700
001710      START ARK-LIN KEY IS NOT < CHIAVE-LIN
001720       INVALID KEY GO TO FINE.
001040
001270
001280      PERFORM TRATTA          THRU EX-TRATTA.

001280      PERFORM LISTA-LABEL     THRU EX-LISTA-LABEL.
001290
001300  FINE.
001310
001320      CLOSE ARK-LIN ARK-LABEL.
001330
001340      GOBACK.
001350
001360
001630
001640  TRATTA.

      **** look for initial procedure area

             PERFORM UNTIL FINE-FILE = "S" OR IND1 = 2

             PERFORM LETTURA-CODICE    THRU EX-LETTURA-CODICE

001840       INSPECT DATI-LIN TALLYING IND1 FOR ALL "PROCEDURE"
001840       INSPECT DATI-LIN TALLYING IND1 FOR ALL "DIVISION"

             END-PERFORM.

             IF FINE-FILE = "S" GO TO EX-TRATTA.

001790
             PERFORM UNTIL FINE-FILE = "S"


              IF FINE-FILE NOT = "S"

      **** look for a perform label

              PERFORM UNTIL FINE-FILE = "S" OR LABEL-NAME > SPACES

              PERFORM LETTURA-CODICE    THRU EX-LETTURA-CODICE

              END-PERFORM

              MOVE SPACES               TO FIELD-NAME

              PERFORM SCRIVO-LABEL      THRU EX-SCRIVO-LABEL

              MOVE LABEL-NAME           TO WLABEL

              END-IF


      **** look for an EXIT label


              IF FINE-FILE NOT = "S"

               PERFORM UNTIL FINE-FILE = "S" OR IND1 = 1

                PERFORM LETTURA-CODICE    THRU EX-LETTURA-CODICE

001840          INSPECT DATI-LIN TALLYING IND1 FOR ALL "EXIT."

                IF IND1 NOT = 1
                 MOVE WLABEL           TO LABEL-NAME
                 PERFORM SCRIVO-FIELD  THRU EX-SCRIVO-FIELD
                END-IF

001850
               END-PERFORM

              END-IF

              MOVE SPACES              TO LABEL-NAME

             END-PERFORM.


001890
001920  END-TRATTA.

             MOVE SPACES     TO FINE-FILE.

002230  EX-TRATTA.
002240       EXIT.

        LETTURA-CODICE.
001750
             MOVE SPACES TO DATI-LIN FINE-FILE LABEL-NAME.

001780       READ ARK-LIN NEXT RECORD AT END MOVE "S" TO FINE-FILE.

             IF FINE-FILE = "S" GO TO EX-LETTURA-CODICE.

001820       MOVE FUNCTION UPPER-CASE(DATI-LIN) TO DATI-LIN.

             MOVE ZEROS TO IND2 IND1.

001840       INSPECT DATI-LIN TALLYING IND2 FOR ALL ".".

             IF IND2 = 1

               UNSTRING DATI-LIN(7:) DELIMITED BY "."

                INTO LABEL-NAME

                IF LABEL-NAME = "EXIT"
                  MOVE SPACES TO LABEL-NAME
                END-IF

      *** remove any SECTION... after the label name


                PERFORM VARYING IND1 FROM 1 BY 1 UNTIL IND1 > 30
                OR LABEL-NAME(IND1:1) > SPACES
                CONTINUE
                END-PERFORM

                PERFORM VARYING IND1 FROM IND1 BY 1 UNTIL IND1 > 30
                OR LABEL-NAME(IND1:1) = SPACES
                CONTINUE
                END-PERFORM

                MOVE SPACES TO LABEL-NAME(IND1:).


             MOVE ZEROS       TO IND1.


             IF DATI-LIN(7:1) = "*"
              GO TO LETTURA-CODICE.


        EX-LETTURA-CODICE.
             EXIT.

        SCRIVO-LABEL.

             INITIALIZE REC-LABEL


             PERFORM VARYING IND4 FROM 1 BY 1 UNTIL IND4 > 30
             OR LABEL-NAME(IND4:1) > SPACES
             CONTINUE
             END-PERFORM

             MOVE LABEL-NAME(IND4:) TO NAME-LABEL.

             PERFORM VARYING IND4 FROM 1 BY 1 UNTIL IND4 > 30
             OR FIELD-NAME(IND4:1) > SPACES
             CONTINUE
             END-PERFORM

             MOVE FIELD-NAME(IND4:) TO FIELD-LABEL.

             INSPECT FIELD-LABEL REPLACING ALL "." BY " ".


             MOVE NAME-LABEL TO NAME-SEC-LABEL.
             MOVE FIELD-LABEL TO FIELD-SEC-LABEL.

             READ ARK-LABEL KEY IS CHIAVE-SEC-LABEL
              INVALID  KEY CONTINUE
               NOT INVALID KEY GO TO EX-SCRIVO-LABEL.

             ADD 1            TO RIGA.

             IF RIGA > 24 ADD 1 TO PAGINA
              MOVE 8          TO RIGA.

             MOVE PAGINA      TO PAG-LABEL MAX-PAGINA.
             MOVE RIGA        TO ROW-LABEL.

             MOVE NAME-LABEL  TO NAME-SEC-LABEL.
             MOVE FIELD-LABEL TO FIELD-SEC-LABEL.

             WRITE REC-LABEL
              INVALID KEY CONTINUE
                GO TO FINE.


        EX-SCRIVO-LABEL.
             EXIT.

        LISTA-LABEL.


             MOVE 1                     TO PAGINA.

        INIZIO-LISTA-LABEL.

             INITIALIZE CHIAVE-LABEL.

             MOVE SPACES                TO FINE-FILE.

             MOVE PAGINA                TO PAG-LABEL.
             START ARK-LABEL KEY IS NOT < CHIAVE-LABEL
              INVALID KEY GO TO FINE-LISTA-LABEL.


        CICLO-LISTA-LABEL.

             PERFORM DO-FORM            THRU EX-DO-FORM.


             DISPLAY "ROUTINE NAME...               CHANGED FIELDS  "
               LINE 7 COL 20
                 WITH FOREGROUND-COLOR COB-COLOR-CYAN REVERSE-VIDEO
                      BACKGROUND-COLOR COB-COLOR-BLACK.

             PERFORM UNTIL FINE-FILE = "S"
               OR PAG-LABEL NOT = PAGINA

               READ ARK-LABEL NEXT RECORD
                AT END MOVE "S" TO FINE-FILE
               END-READ

               IF PAG-LABEL NOT = PAGINA
               MOVE "S"         TO FINE-FILE
               END-IF

               IF FINE-FILE NOT = "S"


                IF FIELD-LABEL = SPACES
                DISPLAY NAME-LABEL LINE ROW-LABEL COL 20
                 WITH FOREGROUND-COLOR COB-COLOR-WHITE
                      BACKGROUND-COLOR COB-COLOR-BLACK
                END-DISPLAY
                END-IF

                DISPLAY FIELD-LABEL(1:15)
                 LINE ROW-LABEL COL 51
                 WITH FOREGROUND-COLOR COB-COLOR-YELLOW
                      BACKGROUND-COLOR COB-COLOR-BLUE
                END-DISPLAY

               END-IF

              END-PERFORM.

            IF FINE-FILE = "S"
             MOVE LOW-VALUE TO CHIAVE-LABEL
             START ARK-LABEL KEY IS NOT < CHIAVE-LABEL
             INVALID KEY GO TO FINE-LISTA-LABEL
             END-START
            END-IF.


        AC-LISTA.

            ACCEPT XX  LINE 25 COL 79 WITH AUTO.

            IF COB-CRT-STATUS = COB-SCR-PAGE_UP
             SUBTRACT 1 FROM PAGINA

             IF PAGINA = ZERO
             MOVE 1          TO PAGINA
             END-IF

             GO TO INIZIO-LISTA-LABEL
            END-IF.

            IF COB-CRT-STATUS = COB-SCR-PAGE_DOWN
              ADD 1          TO PAGINA

              IF PAGINA > MAX-PAGINA
              MOVE 1         TO PAGINA
              END-IF

             GO TO INIZIO-LISTA-LABEL
            END-IF.

        FINE-LISTA-LABEL.


        EX-LISTA-LABEL.
            EXIT.

        DO-FORM.

             MOVE ALL LO               TO ROW-VIDEO.
             MOVE A1                   TO ROW-VIDEO(1:1)
             MOVE A3                   TO ROW-VIDEO(48:1)

             DISPLAY ROW-VIDEO        LINE 06 COL 19
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW
                      REVERSE-VIDEO.

             MOVE ALL SPACES           TO ROW-VIDEO.
             MOVE LV                   TO ROW-VIDEO(1:1)
             MOVE LV                   TO ROW-VIDEO(48:1)
             PERFORM VARYING IND FROM 07 BY 1 UNTIL IND > 24
              DISPLAY ROW-VIDEO LINE IND COL 19

               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW HIGHLIGHT
                      REVERSE-VIDEO

              END-DISPLAY
             END-PERFORM.

             MOVE ALL LO               TO ROW-VIDEO.
             MOVE A2                   TO ROW-VIDEO(1:1)
             MOVE A4                   TO ROW-VIDEO(48:1)

             DISPLAY ROW-VIDEO        LINE 21 COL 19
               WITH FOREGROUND-COLOR COB-COLOR-BLUE
                     BACKGROUND-COLOR COB-COLOR-YELLOW
                      REVERSE-VIDEO.

        EX-DO-FORM.
             EXIT.

        SCRIVO-FIELD.


              MOVE " TO "            TO COBOL-TEST.
              MOVE 4                 TO COBOL-SIZE.
              PERFORM DO-FIELD       THRU EX-DO-FIELD.

              MOVE " GIVING "        TO COBOL-TEST.
              MOVE 8                 TO COBOL-SIZE.
              PERFORM DO-FIELD       THRU EX-DO-FIELD.

              MOVE " INTO "          TO COBOL-TEST.
              MOVE 6                 TO COBOL-SIZE.
              PERFORM DO-FIELD       THRU EX-DO-FIELD.

              MOVE " TALLYING "      TO COBOL-TEST.
              MOVE 10                TO COBOL-SIZE.
              PERFORM DO-FIELD       THRU EX-DO-FIELD.


        EX-SCRIVO-FIELD.
             EXIT.

        DO-FIELD.

             INITIALIZE FIELDS.

             MOVE ZERO  TO IND2.

             INSPECT DATI-LIN TALLYING IND2
               FOR ALL COBOL-TEST(1:COBOL-SIZE)

               IF IND2 NOT = ZEROS

               MOVE SPACES TO ALL-FIELDS
               UNSTRING DATI-LIN DELIMITED BY COBOL-TEST(1:COBOL-SIZE)
                INTO DUMMY ALL-FIELDS

                 UNSTRING ALL-FIELDS DELIMITED BY ALL SPACES
                 INTO
                 NAME-FIELD(1)
                  NAME-FIELD(2)
                   NAME-FIELD(3)
                    NAME-FIELD(4)
                     NAME-FIELD(5)
                      NAME-FIELD(6)
                       NAME-FIELD(7)
                        NAME-FIELD(8)
                         NAME-FIELD(9)
                          NAME-FIELD(10)
                            TALLYING MAX-FIELD

               PERFORM VARYING IND2 FROM 1 BY 1 UNTIL IND2 > MAX-FIELD

                 MOVE NAME-FIELD(IND2) TO FIELD-NAME

                 INSPECT FIELD-NAME REPLACING ALL '"' BY "0"
                 INSPECT FIELD-NAME REPLACING ALL ' ' BY "0"
                 INSPECT FIELD-NAME REPLACING ALL '(' BY "0"
                 INSPECT FIELD-NAME REPLACING ALL ')' BY "0"
                 INSPECT FIELD-NAME REPLACING ALL '.' BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "'" BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "+" BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "/" BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "," BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "-" BY "0"
                 INSPECT FIELD-NAME REPLACING ALL "*" BY "0"

                 IF NAME-FIELD(IND2) (1:1) = "("
                  MOVE ZERO                      TO FIELD-NAME
                 END-IF

                 IF FIELD-NAME NOT NUMERIC
                  MOVE NAME-FIELD(IND2) TO FIELD-NAME

                  IF NOT FIELD-NAME-COBOL
                  PERFORM SCRIVO-LABEL  THRU EX-SCRIVO-LABEL
                  END-IF

                 END-IF


               END-PERFORM.

        EX-DO-FIELD.
             EXIT.

002250
002260  end program animloca.
002270

