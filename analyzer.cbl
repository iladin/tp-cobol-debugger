        IDENTIFICATION DIVISION.
        PROGRAM-ID.    analyzer.
      *---------------------------------------------------------------------
      *
      * ANALIZER evaluate a cobol source
      *
      * LAST  08 APRIL 2012  0.1.8 ONE YEAR LATER
      *       11 JULY  2010  0.1.7
      *       04 APRIL 2010  0.1.6
      *       25 MARCH 2010  0.1.5
      *       24 MARCH 2010  0.1.4
      *       21 MARCH 2010  0.1.3
      *       17 MARCH 2010  0.1.2
      *       15 MARCH 2010  0.1.1
      * FIRST 13 MARCH 2010  0.1.0
      *
      * Copyright (C) 2010-2012 Federico Priolo T&P Soluzioni Informatiche Srl
      *
      * This program is free software; you can redistribute it and/or modify
      * it under the terms of the GNU General Public License as published by
      * the Free Software Foundation; either version 2, or (at your option)
      * any later version.
      *
      * This program is distributed in the hope that it will be useful,
      * but WITHOUT ANY WARRANTY; without even the implied warranty of
      * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      * GNU General Public License for more details.
      *
      * You should have received a copy of the GNU General Public License
      * along with this software; see the file COPYING.  If not, write to
      * the Free Software Foundation, 51 Franklin Street, Fifth Floor
      * Boston, MA 02110-1301 USA
      *
      *
      *---------------------------------------------------------------------
        ENVIRONMENT DIVISION.
        CONFIGURATION SECTION.
        SOURCE-COMPUTER.        PC-IBM.
        OBJECT-COMPUTER.        PC-IBM.
        SPECIAL-NAMES.

           DECIMAL-POINT IS COMMA.

        INPUT-OUTPUT SECTION.
        FILE-CONTROL.

                SELECT ARK-IN ASSIGN TO  FILE-IN
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-IN.

                SELECT ARK-DO ASSIGN TO  FILE-DO
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-DO.

                SELECT ARK-OUT ASSIGN TO FILE-OUT
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-OUT.

                SELECT ARK-VAR ASSIGN TO FILE-VAR
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-VAR.

                SELECT ARK-FIELD ASSIGN TO FILE-FIELD
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS  DYNAMIC
                 RECORD KEY   IS  CHIAVE-FIELD
                 ALTERNATE RECORD KEY IS BREAK-FIELD WITH DUPLICATES
                 FILE STATUS  IS STATUS-FIELD.

                SELECT ARK-CPY ASSIGN TO FILE-CPY
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-CPY.

                SELECT ARK-LIN ASSIGN TO FILE-LIN
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS  DYNAMIC
                 RECORD KEY   IS  CHIAVE-LIN
                 FILE STATUS  IS STATUS-LIN.

                SELECT ARK-STACK ASSIGN TO FILE-STACK
                 ORGANIZATION IS INDEXED
                 ACCESS MODE  IS DYNAMIC
                 RECORD KEY   IS CHIAVE-STACK
                 FILE STATUS  IS STATUS-STACK.

                SELECT ARK-FLOW ASSIGN TO FILE-FLOW
                 ORGANIZATION IS LINE SEQUENTIAL
                 FILE STATUS  IS STATUS-FLOW.


        DATA DIVISION.
        FILE SECTION.

        FD ARK-LIN.
        01 REC-LIN.
         02 CHIAVE-LIN.
          05 PAG-LIN                  PIC 9(4).
          05 RIGA-LIN                 PIC 9(2).
         02 DATI-LIN                  PIC X(80).
         02 TIPO-LIN                  PIC X.
         02 BREAK-LIN                 PIC X.
         02 CODICE-LIN                PIC X.
         02 FILLER                    PIC X(19).


        FD ARK-STACK.
        01 REC-STACK.
           02 CHIAVE-STACK.
             05 KEY-STACK             PIC 9(9).
           02 LINE-STACK              PIC 9(9).
           02 START-STACK             PIC X(30).
           02 EXIT-STACK              PIC X(30).
           02 PARENT-STACK            PIC X(30).
           02 WHERE-STACK             PIC 9(9).
           02 MIN-STACK               PIC 9(9).
           02 MAX-STACK               PIC 9(9).
           02 LEVEL-STACK             PIC 99.
           02 FILLER-STACK            PIC X(119).



        FD ARK-FIELD.
        01 REC-FIELD.
         02 CHIAVE-FIELD.
            09 NOME-FIELD             PIC X(30).
         02 TIPO-FIELD                PIC X(01).
         02 VALUE-FIELD               PIC X(256).
         02 SIZE-FIELD                PIC 9(9) COMP-5.
         02 BREAK-FIELD               PIC X.
           88 BREAK-ON-CHANGE         VALUE "X".
         02 TEST-FIELD                PIC X.
           88 LESS-THEN               VALUE "L".
           88 EQUAL-TO                VALUE "E".
           88 MAJOR-OF                VALUE "M".
         02 NOT-FIELD                 PIC X.
           88 NOT-OF                  VALUE "N".
         02 VALUE-BREAK-FIELD         PIC X(256).
         02 KEY-FIELD-FIELD           PIC X(30).
         02 ADDRESS-FIELD             USAGE POINTER.
         02 FILLER-FIELD              PIC X(56).



        FD ARK-IN.
        01 REC-IN.
           02 DATI-IN                 PIC X(256).

        FD ARK-VAR.
        01 REC-VAR.
           02 DATI-VAR                PIC X(100).

        FD ARK-CPY.
        01 REC-CPY.
           02 DATI-CPY                PIC X(100).

        FD ARK-DO.
        01 REC-DO.
           02 DATI-DO                 PIC X(500).

        FD ARK-OUT.
        01 REC-OUT.
           02 DATI-OUT                PIC X(256).

        FD ARK-FLOW.
        01 REC-FLOW.
           02 DATI-FLOW               PIC X(256).

        WORKING-STORAGE SECTION.
        77 CC                          PIC X.

        01 PARAMETRI.
         07 TIME-SYS                  PIC 9(6)   VALUE ZEROS.
         07 FILE-SYS                  PIC X(6)   VALUE SPACE.
         07 ISTRUZIONE                PIC X(30)  VALUE SPACE.
         07 END-POINT                 PIC X(30)  VALUE SPACE.
         07 COBC-INVOKE               PIC X(512) VALUE SPACE.
         07 ANALYZER-INVOKE           PIC X(512) VALUE SPACE.
         07 ANALYZER-VIEW             PIC X(512) VALUE SPACE.
         07 BASE-NODE                 PIC X(512) VALUE SPACE.
         07 DEEP-NODE                 PIC X(512) VALUE SPACE.
         07 PARAMETRO                 PIC X(30)  VALUE SPACE.
         07 VALORE                    PIC X(512) VALUE SPACE.
         07 FINE-FILE                 PIC X      VALUE SPACE.
         07 FILE-FIELD                PIC X(200) VALUE SPACE.
         07 FILE-CPY                  PIC X(200) VALUE SPACE.
         07 FILE-DO                   PIC X(200) VALUE SPACE.
         07 FILE-LIN                  PIC X(200) VALUE SPACE.
         07 FILE-LOG                  PIC X(200) VALUE SPACE.
         07 FILE-OUT                  PIC X(200) VALUE SPACE.
         07 FILE-VAR                  PIC X(200) VALUE SPACE.
         07 STATUS-FLOW               PIC XX     VALUE SPACE.
         07 STATUS-CPY                PIC XX     VALUE SPACE.
         07 STATUS-DO                 PIC XX     VALUE SPACE.
         07 STATUS-IN                 PIC XX     VALUE SPACE.
         07 STATUS-LIN                PIC XX     VALUE SPACE.
         07 STATUS-OUT                PIC XX     VALUE SPACE.
         07 STATUS-VAR                PIC XX     VALUE SPACE.
         07 STATUS-FIELD              PIC XX     VALUE SPACE.
         07 STATUS-STACK              PIC XX     VALUE SPACE.
         07 POSIZIONE.
          17 PAGINA                   PIC 9(4)   VALUE ZEROS.
          17 CONTA                    PIC 9(2)   VALUE ZEROS.
         07 CONTA-DEBUG               PIC 9(2)   VALUE ZEROS.
         07 CONTA-LINE                PIC 9(6)   VALUE ZEROS.
         07 IND                       PIC 9(9)   VALUE ZEROS.
         07 IND1                      PIC 9(9)   VALUE ZEROS.
         07 VERBO-COBOL               PIC XX     VALUE SPACE.
         07 STRINGA                   PIC X(100) VALUE SPACE.
         07 TIPO-RIGA                 PIC X      VALUE SPACE.
         07 NOME-LABEL                PIC X(30)  VALUE SPACE.
         07 TAB-ALTRO.
           19 ALTRO                   PIC X(30)  OCCURS 10 TIMES.
         07 DA-PERFORM                PIC X(30)  VALUE SPACE.
         07 A-PERFORM                 PIC X(30)  VALUE SPACE.
         07 START-PERFORM             PIC X(30)  VALUE SPACE.
         07 EXIT-PERFORM              PIC X(30)  VALUE SPACE.

         07 CHIUSO                    PIC XX     VALUE SPACE.
         07 COMANDO                   PIC X(1024) VALUE SPACES.
         07 COPY1                     PIC X(100) VALUE SPACE.
         07 COPY2                     PIC X(100) VALUE SPACE.
         07 DO-DLL                    PIC XX     VALUE SPACE.
         07 TAB-OPTIONS.
          09 SW-OPTION                PIC X OCCURS 10 TIMES.
         07 SW-NOCOMPILA              PIC X.
         07 SW-SAVE                   PIC X.
         07 SW-DATI                   PIC X.
         07 SW-DATI-OK                PIC X.
         07 SW-HELP                   PIC X.
         07 SW-MANUAL                 PIC X.
         07 SW-CODICE                 PIC X.
         07 SW-FREE                   PIC X.
         07 SW-CONST                  PIC X.
         07 SW-STOP                   PIC X.
         07 SW-EXE                    PIC X.
         07 SW-ANALYSIS               PIC X.
         07 SW-VERBOSE                PIC X.
         07 XX                        PIC XX.
         07 REM-COLUMN                PIC 9.
         07 CAMPO1                    PIC X(30).
         07 CAMPO2.
           09 LIVELLO                 PIC 99.
             88 LIVELLO-OK            VALUE 01 THRU 87.

         07 CAMPO3                    PIC X(30).
         07 CAMPO4                    PIC X(30).
         07 RIC-LIVELLO               PIC 99.
         07 INSIDE-OCCURS             PIC XX.
         07 EOF-DO                    PIC X.
         07 AREA-DATI                 PIC X.
         07 TYPE-SYSTEM               PIC X(10).
         07 CONTA-STACK               PIC 9(9).
         07 CONTA-RECORD              PIC 9(9).
         07 MIN-RECORD                PIC 9(9).
         07 MAX-RECORD                PIC 9(9).
         07 START-RECORD              PIC 9(9).
         07 POINT-RECORD              PIC 9(9).
         07 ESCI                      PIC XX     VALUE SPACE.
         07 PARENT                    PIC X(30)  VALUE SPACE.
         07 GLOBAL-LEVEL              PIC 99     VALUE ZEROS.
         07 LAST-LABEL                PIC X(30)  VALUE SPACE.
         07 THIS-LABEL                PIC X(30)  VALUE SPACE.

        01 FILE-FLOW                  PIC X(200) IS EXTERNAL.
        01 FILE-STACK                 PIC X(200) IS EXTERNAL.

                 copy "workanim.cpy".

        LINKAGE SECTION.
        01 FILE-IN           PIC X(200).
        PROCEDURE DIVISION USING FILE-IN.
        INIZIO SECTION.

                PERFORM OPZIONI  THRU EX-OPZIONI.

                PERFORM ANALYSIS THRU EX-ANALYSIS.

                GOBACK.

        OPZIONI.

               INITIALIZE PARAMETRI.

               MOVE "GOBACK" TO END-POINT.

               ACCEPT FILE-DO FROM ENVIRONMENT "animator.inf"
                END-ACCEPT.

                IF FILE-DO = SPACES
                MOVE "animator.inf"    TO FILE-DO.

                PERFORM OPZIONI-ANIMATOR THRU EX-OPZIONI-ANIMATOR.


        EX-OPZIONI.
                EXIT.

        OPZIONI-ANIMATOR.

                DISPLAY "Getting animator options..".

                MOVE SPACES TO EOF-DO.
                MOVE SPACES TO PATH-TEMP.

                OPEN INPUT ARK-DO.

                IF STATUS-DO = 35
                 DISPLAY
                 "The animator configuration file is not available:"
                   FILE-DO
                  STOP RUN.

                PERFORM UNTIL EOF-DO = "S"

                PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO

                MOVE ZEROS  TO IND
                INSPECT DATI-DO TALLYING IND FOR ALL "="

                IF IND > ZEROS

                 IF IND = 1
                 MOVE SPACES TO PARAMETRO VALORE
                 UNSTRING DATI-DO DELIMITED BY "="
                  INTO PARAMETRO VALORE
                 END-UNSTRING
                 ELSE
                  PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 500
                   OR DATI-DO(IND:1) = "="
                   CONTINUE
                  END-PERFORM

                  SUBTRACT 1 FROM IND
                  MOVE DATI-DO(1:IND) TO PARAMETRO
                  ADD  2     TO IND
                  MOVE DATI-DO(IND:)  TO VALORE
                 END-IF



                 EVALUATE PARAMETRO
                 WHEN "TEMP" MOVE VALORE TO PATH-TEMP
                             CALL "CBL_CREATE_DIR" USING PATH-TEMP
                 WHEN "COBC" MOVE VALORE TO COBC-INVOKE

                END-IF

                END-PERFORM.

                CLOSE ARK-DO.

        EX-OPZIONI-ANIMATOR.
                 EXIT.

        ANALYSIS.

                 DISPLAY "Building the analysis features....".

                 OPEN INPUT ARK-IN.
                 OPEN OUTPUT ARK-FLOW.

                 OPEN OUTPUT ARK-STACK.
                 CLOSE ARK-STACK.
                 OPEN I-O ARK-STACK.

                 MOVE SPACES             TO REC-FLOW.
                 PERFORM SCRIVO-FLOW     THRU EX-SCRIVO-FLOW.

                 MOVE "DIgraph G   {"    TO REC-FLOW.
                 PERFORM SCRIVO-FLOW     THRU EX-SCRIVO-FLOW.

                 PERFORM OPZIONI-ANALYZER THRU EX-OPZIONI-ANALYZER.

                 MOVE SPACES TO FINE-FILE.
                 MOVE SPACES TO ISTRUZIONE.
                 MOVE SPACES TO NOME-LABEL.
                 MOVE ZEROS  TO CONTA-RECORD.
                 MOVE ZEROS  TO START-RECORD.
                 MOVE ZEROS  TO MIN-RECORD.
                 MOVE ZEROS  TO MAX-RECORD.
                 MOVE ZEROS  TO POINT-RECORD.

                 PERFORM UNTIL FINE-FILE = "S"
                  OR NOME-LABEL = "PROCEDURE"

                  PERFORM READ-IN THRU EX-READ-IN

                  MOVE ZEROS TO IND
                  MOVE FUNCTION UPPER-CASE(REC-IN) TO STRINGA

                  INSPECT STRINGA TALLYING IND FOR ALL "PROCEDURE"
                  INSPECT STRINGA TALLYING IND FOR ALL "DIVISION"
                   IF IND = 2
                     MOVE CONTA-RECORD TO START-RECORD
                     MOVE "PROCEDURE"  TO NOME-LABEL
                   END-IF

                 END-PERFORM.

                 INITIALIZE REC-STACK.

                 MOVE 1                 TO CONTA-STACK.

                 MOVE CONTA-RECORD      TO LINE-STACK.
                 MOVE SPACES            TO START-STACK.
                 MOVE SPACES            TO EXIT-STACK.
                 MOVE SPACES            TO PARENT.
                 MOVE SPACES            TO LAST-LABEL.
                 MOVE ZEROS             TO GLOBAL-LEVEL.
                 PERFORM SCRIVO-STACK   THRU EX-SCRIVO-STACK.

      *>
      *> start analysis from PERFORM found in the code...
      *>
        CICLO-ANALYSIS.

                 MOVE SPACES TO NOME-LABEL.
                 MOVE SPACES TO ISTRUZIONE.

                 IF FINE-FILE = "S"
                  MOVE SPACES TO FINE-FILE
                  OPEN INPUT ARK-IN.

                 MOVE CONTA-RECORD        TO START-RECORD.
                 MOVE CONTA-RECORD        TO POINT-RECORD.


        MAIN-LOOP.

                 PERFORM RIAPRE-IN        THRU EX-RIAPRE-IN.

                 MOVE SPACES              TO DA-PERFORM.
                 MOVE SPACES              TO A-PERFORM.

                 PERFORM UNTIL FINE-FILE = "S"
                                OR ESCI = "SI"
                  OR DA-PERFORM > SPACES
                  PERFORM TRATTA-IN THRU EX-TRATTA-IN
                 END-PERFORM.

                 IF ESCI = "SI" GO TO END-ANALYSIS.

                 IF FINE-FILE = "S"
                   SUBTRACT 1 FROM GLOBAL-LEVEL
                    GO TO TEST-ANALYSIS.

                 MOVE CONTA-RECORD      TO LINE-STACK.
                 MOVE DA-PERFORM        TO START-STACK.
                 MOVE A-PERFORM         TO EXIT-STACK.
                 PERFORM SCRIVO-STACK   THRU EX-SCRIVO-STACK.

                 PERFORM CERCA-RANGE    THRU EX-CERCA-RANGE.

                 MOVE MIN-RECORD        TO MIN-STACK.
                 MOVE MAX-RECORD        TO MAX-STACK.
                 MOVE DA-PERFORM        TO PARENT.


                DISPLAY "adding node/edge named "
                FUNCTION TRIM (START-STACK) " level "
                    LEVEL-STACK " of " FUNCTION TRIM(PARENT-STACK)
                    " after " FUNCTION TRIM (LAST-LABEL).

                MOVE SPACES TO REC-FLOW.

                IF LEVEL-STACK = 2
                 MOVE START-STACK      TO PARENT.


                IF LEVEL-STACK = 2
                MOVE BASE-NODE         TO REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW

                 IF LAST-LABEL = SPACES
                 MOVE "MAIN"           TO THIS-LABEL
                 ELSE
                 MOVE LAST-LABEL       TO THIS-LABEL
                 END-IF

                STRING THIS-LABEL DELIMITED BY "    "
                        "->" DELIMITED BY SIZE
                        start-stack DELIMITED BY "    "
                        ";" delimited by size into REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW
                  ELSE
                MOVE DEEP-NODE         TO REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW
                STRING parent-stack DELIMITED BY "    "
                        "->" DELIMITED BY SIZE
                        start-stack DELIMITED BY "    "
                        ";"
                         delimited by size into REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW.


                IF LEVEL-STACK > 2
                MOVE DEEP-NODE         TO REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW
                STRING start-stack DELIMITED BY "    "
                        "->" DELIMITED BY SIZE
                        parent-stack DELIMITED BY "    "
                        "[style=dotted color=blue]"
                        ";" delimited by size into REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW.

                 REWRITE REC-STACK.

                 IF LEVEL-STACK = 02
                 MOVE START-STACK      TO LAST-LABEL.

                 GO TO MAIN-LOOP.

        TEST-ANALYSIS.

                MOVE SPACES            TO FINE-FILE.
                MOVE ALL "9"           TO CHIAVE-STACK.
                START ARK-STACK KEY IS LESS CHIAVE-STACK
                   INVALID KEY
                      GO TO END-ANALYSIS.

                READ ARK-STACK PREVIOUS RECORD AT END
                      GO TO END-ANALYSIS.

                DELETE ARK-STACK.

                MOVE LINE-STACK                 TO POINT-RECORD.
                MOVE KEY-STACK                  TO CONTA-STACK.

                ADD  1                          TO POINT-RECORD.
                MOVE POINT-RECORD               TO MIN-RECORD.
                MOVE MAX-STACK                  TO MAX-RECORD.
                MOVE SPACES                     TO FINE-FILE.
                MOVE PARENT-STACK               TO PARENT.


                READ ARK-STACK PREVIOUS RECORD AT END
                      GO TO END-ANALYSIS.

                MOVE MAX-STACK                 TO MAX-RECORD.

                GO TO MAIN-LOOP.

        END-ANALYSIS.


                MOVE BASE-NODE         TO REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW.

                STRING LAST-LABEL DELIMITED BY "    "
                        "->" DELIMITED BY SIZE
                        "END_PROGRAM;"
                         delimited by size into REC-FLOW
                PERFORM SCRIVO-FLOW    THRU EX-SCRIVO-FLOW.

                 MOVE "}"                TO REC-FLOW.
                 PERFORM SCRIVO-FLOW     THRU EX-SCRIVO-FLOW.

                 CLOSE ARK-IN.
                 CLOSE ARK-STACK.
                 CLOSE ARK-FLOW.

                 IF ANALYZER-INVOKE > SPACES
                 STRING ANALYZER-INVOKE DELIMITED BY "     "
                  " " FILE-FLOW DELIMITED BY SIZE INTO COMANDO
                  DISPLAY "Making CFG.."  COMANDO
                  CALL "SYSTEM" USING COMANDO.

                 IF ANALYZER-VIEW > SPACES
                 MOVE ANALYZER-VIEW TO COMANDO
                  CALL "SYSTEM" USING COMANDO.


        EX-ANALYSIS.
                 EXIT.


        RIAPRE-IN.


                 CLOSE ARK-IN.
                 OPEN INPUT ARK-IN.
                 MOVE ZEROS TO CONTA-RECORD.
                 MOVE SPACES TO STRINGA.

        EX-RIAPRE-IN.
                 EXIT.

        READ-IN.

                IF FINE-FILE = "S" GO TO EX-READ-IN.

                READ ARK-IN NEXT RECORD AT END
                 MOVE "S" TO FINE-FILE
                 PERFORM RIAPRE-IN      THRU EX-RIAPRE-IN
                 GO TO EX-READ-IN.

                ADD 1 TO CONTA-RECORD.

                PERFORM VARYING IND1 FROM 1 BY 1 UNTIL IND1 > 100
                  OR REC-IN(IND1:1) > SPACES
                  CONTINUE
                END-PERFORM.

                IF IND1 > 100 GO TO READ-IN.

                IF REC-IN(IND1:1) = "*" GO TO READ-IN.

                MOVE SPACES TO STRINGA.

        EX-READ-IN.
                 EXIT.

        TRATTA-IN.

                MOVE SPACES  TO TAB-ALTRO.

                PERFORM READ-IN THRU EX-READ-IN.

                IF FINE-FILE = "S" GO TO EX-TRATTA-IN.

                IF CONTA-RECORD < START-RECORD
                 AND START-RECORD NOT = ZEROS
                  GO TO TRATTA-IN.

                IF CONTA-RECORD < POINT-RECORD
                 AND POINT-RECORD NOT = ZEROS
                  GO TO TRATTA-IN.

                IF CONTA-RECORD < MIN-RECORD
                 AND MIN-RECORD NOT = ZEROS
                  GO TO TRATTA-IN.

                IF CONTA-RECORD > MAX-RECORD
                 AND MAX-RECORD NOT = ZEROS
                 MOVE "S" TO FINE-FILE
                  GO TO EX-TRATTA-IN.

                 PERFORM VARYING IND FROM 1 BY  1 UNTIL IND > 100
                 OR REC-IN(IND:1) > "9"
                  CONTINUE
                 END-PERFORM.

                IF IND > 100 GO TO EX-TRATTA-IN.

                MOVE REC-IN(IND:)   TO STRINGA.

                MOVE FUNCTION UPPER-CASE(STRINGA) TO STRINGA.


                MOVE ZEROS TO IND.

                PERFORM VARYING IND1 FROM 30 BY -1 UNTIL IND1 = ZERO
                 OR END-POINT(IND1:1) > SPACES
                END-PERFORM.

                INSPECT STRINGA TALLYING IND FOR ALL END-POINT(1:IND1).

                IF IND = 1 MOVE "SI" TO ESCI.

                MOVE ZEROS TO IND.

                INSPECT STRINGA TALLYING IND FOR ALL "VARYING".

                IF IND = 1 GO TO TRATTA-IN.

                MOVE ZEROS TO IND.

                INSPECT STRINGA TALLYING IND FOR ALL "END-PERFORM".

                IF IND = 1 GO TO TRATTA-IN.



                MOVE ZEROS TO IND.

                INSPECT STRINGA TALLYING IND FOR ALL "PERFORM ".

                IF IND = ZEROS GO TO TRATTA-IN.


                MOVE ZEROS TO IND.

                INSPECT STRINGA TALLYING IND FOR ALL '"'.

                IF IND NOT = ZEROS
                 PERFORM VARYING IND FROM 1 BY 1
                 UNTIL STRINGA(IND:1) = '"'
                 CONTINUE
                 END-PERFORM
                 ADD 1 TO IND
                 PERFORM VARYING IND FROM IND BY 1
                 UNTIL STRINGA(IND:1) = '"'
                 MOVE SPACES TO STRINGA(IND:1)
                 END-PERFORM.

                UNSTRING STRINGA DELIMITED BY ALL  SPACES
                 INTO ALTRO(1) ALTRO(2) ALTRO(3) ALTRO(4) ALTRO(5)
                      ALTRO(6) ALTRO(7) ALTRO(8) ALTRO(9) ALTRO(10).

                PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 10

                 IF ALTRO(IND) = "PERFORM"
                  AND ALTRO(IND + 2 ) = "THRU"
                   MOVE ALTRO(IND + 1) TO DA-PERFORM
                   MOVE ALTRO(IND + 3) TO A-PERFORM
                 END-IF

                 IF ALTRO(IND) = "PERFORM"
                  AND ALTRO(IND + 2 ) NOT = SPACES
                  AND ALTRO(IND + 3 ) = SPACES
                   MOVE ALTRO(IND + 1) TO DA-PERFORM
                   MOVE SPACES         TO A-PERFORM
                 END-IF

                END-PERFORM.

                IF DA-PERFORM = SPACES GO TO EX-TRATTA-IN.

                INSPECT DA-PERFORM REPLACING ALL "." BY " ".
                INSPECT A-PERFORM REPLACING ALL "." BY " ".


        EX-TRATTA-IN.
                EXIT.


        LOCATE-IN.


                PERFORM READ-IN THRU EX-READ-IN.

                IF FINE-FILE = "S" GO TO EX-LOCATE-IN.

                IF CONTA-RECORD < START-RECORD
                 AND START-RECORD NOT = ZEROS
                  GO TO LOCATE-IN.

                 PERFORM VARYING IND FROM 1 BY  1 UNTIL IND > 100
                 OR REC-IN(IND:1) > "9"
                  CONTINUE
                 END-PERFORM.

                IF IND > 100 GO TO EX-LOCATE-IN.

                MOVE ZEROS   TO IND1
                MOVE SPACES  TO STRINGA

                PERFORM VARYING IND FROM IND BY 1 UNTIL IND > 100
                 OR REC-IN(IND:1) = SPACES
                 OR REC-IN(IND:1) = "."

                  ADD  1             TO IND1
                  MOVE REC-IN(IND:1) TO STRINGA(IND1:1)

                END-PERFORM

                MOVE FUNCTION UPPER-CASE(STRINGA) TO STRINGA.

                INSPECT STRINGA REPLACING ALL "." BY " ".

        EX-LOCATE-IN.
                EXIT.


        SCRIVO-STACK.


                ADD 1             TO CONTA-STACK.
                ADD 1             TO GLOBAL-LEVEL.

                MOVE GLOBAL-LEVEL TO LEVEL-STACK.
                MOVE CONTA-STACK  TO KEY-STACK.

                IF PARENT = SPACES
                  MOVE "MAIN"     TO PARENT-STACK.

                WRITE REC-STACK
                 INVALID KEY GO TO SCRIVO-STACK.

                IF LAST-LABEL > SPACES
                 AND LEVEL-STACK = 02
                  MOVE LAST-LABEL TO PARENT-STACK.

                 IF PARENT > SPACES
                 MOVE PARENT          TO PARENT-STACK.

      *>          DISPLAY PARENT " HA " DA-PERFORM "->" A-PERFORM.


        EX-SCRIVO-STACK.
                EXIT.


        CERCA-RANGE.

        LOOP-INSIDE.

                MOVE DA-PERFORM        TO START-PERFORM.
                MOVE A-PERFORM         TO EXIT-PERFORM.
                MOVE ZEROS             TO MIN-RECORD.
                MOVE ZEROS             TO MAX-RECORD.


                PERFORM RIAPRE-IN      THRU EX-RIAPRE-IN.

                PERFORM UNTIL FINE-FILE = "S"
                 OR STRINGA = START-PERFORM
                 PERFORM LOCATE-IN THRU EX-LOCATE-IN
                END-PERFORM.

                MOVE CONTA-RECORD      TO MIN-RECORD.

                PERFORM RIAPRE-IN      THRU EX-RIAPRE-IN.


                PERFORM UNTIL FINE-FILE = "S"
                 OR STRINGA = EXIT-PERFORM
                  OR ( STRINGA > SPACES AND EXIT-PERFORM = SPACES
                       AND STRINGA NOT = START-PERFORM)

                 PERFORM LOCATE-IN THRU EX-LOCATE-IN
                END-PERFORM.

                MOVE CONTA-RECORD      TO MAX-RECORD.


        EX-CERCA-RANGE.
                EXIT.

        SCRIVO-FLOW.

                INSPECT REC-FLOW REPLACING ALL "-" BY "_".
                INSPECT REC-FLOW REPLACING ALL "_>" BY "->".

                WRITE REC-FLOW.
                IF STATUS-FLOW NOT = "00"
                 DISplay "Error " status-flow
                 "writing flow..." FUNCTION TRIM(FILE-FLOW).

                MOVE SPACES                      TO REC-FLOW.

        EX-SCRIVO-FLOW.
                EXIT.

        OPZIONI-ANALYZER.

                DISPLAY "Getting anlyzer options..".

                MOVE SPACES TO EOF-DO.
                MOVE SPACES TO PATH-TEMP.

                OPEN INPUT ARK-DO.

                IF STATUS-DO = 35
                 DISPLAY
                 "The animator configuration file is not available:"
                   FILE-DO
                  STOP RUN.

                PERFORM UNTIL EOF-DO = "S"

                PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO

                MOVE ZEROS  TO IND
                INSPECT DATI-DO TALLYING IND FOR ALL "="

                IF IND > ZEROS

                 IF IND = 1
                 MOVE SPACES TO PARAMETRO VALORE
                 UNSTRING DATI-DO DELIMITED BY "="
                  INTO PARAMETRO VALORE
                 END-UNSTRING
                 ELSE
                  PERFORM VARYING IND FROM 1 BY 1 UNTIL IND > 500
                   OR DATI-DO(IND:1) = "="
                   CONTINUE
                  END-PERFORM

                  SUBTRACT 1 FROM IND
                  MOVE DATI-DO(1:IND) TO PARAMETRO
                  ADD  2     TO IND
                  MOVE DATI-DO(IND:)  TO VALORE
                 END-IF



                 EVALUATE PARAMETRO

                 WHEN "ANALYZER" MOVE VALORE TO ANALYZER-INVOKE

                 WHEN "EDGE"     MOVE VALORE TO REC-FLOW
                                 PERFORM SCRIVO-FLOW THRU EX-SCRIVO-FLOW

                 WHEN "NODE"     MOVE VALORE TO DEEP-NODE

                 WHEN "VIEW"     MOVE VALORE TO ANALYZER-VIEW

                 WHEN "BASE"     MOVE VALORE TO BASE-NODE

                 END-EVALUATE

                END-IF

                END-PERFORM.

                CLOSE ARK-DO.

        EX-OPZIONI-ANALYZER.
                 EXIT.

        READ-NEXT-DO.

                MOVE SPACES TO REC-DO EOF-DO.

                READ ARK-DO NEXT RECORD AT END
                 MOVE "S"    TO EOF-DO.


                IF EOF-DO = "S"
                 MOVE SPACES TO REC-DO
                  GO TO EX-READ-NEXT-DO.

                IF DATI-DO = SPACES GO TO READ-NEXT-DO.

                IF DATI-DO(1:1) = "*" GO TO READ-NEXT-DO.


        EX-READ-NEXT-DO.
                EXIT.

        CERCA-DO.

                MOVE SPACES TO EOF-DO.

                MOVE SPACES TO VERBO-COBOL.
      *
      * get size of the INSTRUCTION...
      *

                PERFORM VARYING IND FROM 30 BY -1 UNTIL IND = ZERO
                   OR ISTRUZIONE(IND:1) > SPACES
                   CONTINUE
                END-PERFORM.


                OPEN INPUT ARK-DO.
      *
      * arrive to the #verbs declare section
      *

                PERFORM UNTIL EOF-DO = "S"
                 OR DATI-DO = "#verbs"
                 PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO
                END-PERFORM.
      *
      * and then process records until eof or next --> #
      *
                 IF EOF-DO NOT = "S"
                 MOVE SPACES TO REC-DO
                 PERFORM UNTIL EOF-DO = "S"
                  OR DATI-DO(1:1) = "#"
                  OR VERBO-COBOL = "SI"
                  PERFORM READ-NEXT-DO THRU EX-READ-NEXT-DO
                  IF EOF-DO NOT = "S"
                   IF DATI-DO(1:IND) = ISTRUZIONE(1:IND)
                    MOVE "SI"  TO VERBO-COBOL
                    MOVE "S"   TO EOF-DO
                   END-IF
                  END-IF
                 END-PERFORM.

                CLOSE ARK-DO.

        EX-CERCA-DO.
               EXIT.

        end program analyzer.


