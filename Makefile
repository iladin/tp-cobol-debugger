# OpenCOBOL animated debugger

COBCWARN = -W -g -debug

# first rule is default
tp-animator: tp-library animator animate-animator

# create executable from .cbl
%: %.cbl
	cobc -conf=default.conf $(COBCWARN) -x $^ -o $@

# create module from .cbl
%.so: %.cbl
	cobc $(COBCWARN) -m $^ -o $@

animator: animator.cbl

animate.so: animate.cbl
animdata.so: animdata.cbl
analyzer.so: analyzer.cbl
animfind.so: animfind.cbl
animview.so: animview.cbl
animload.so: animload.cbl
animloca.so: animloca.cbl

tp-library: animate.so animdata.so analyzer.so animfind.so animview.so animload.so animloca.so
	export CBL_LIBRARY_PATH=/home/btiffin/inst/tp-cobol
	#export ANIMATE.INF=/home/btiffin/inst/tp-cobol
	#export ANIMATOR.INF=/home/btiffin/inst/tp-cobol

clean:
	-rm -f animate.so animdata.so analyzer.so animfind.so animview.so animator 
	-rm -f animate.c animate.c.h animate.c.l.h animate.i animload.so animloca.so
	-rm -f animdata.c animdata.c.h animdata.c.l1.h animdata.c.l2.h animdata.i
	-rm -f analyzer.c analyzer.c.h analyzer.c.l.h analyzer.i
	-rm -f animfind.c animfind.c.h animfind.c.l.h animfind.i
	-rm -f animview.c animview.c.h animview.c.l.h animview.i
	-rm -f animator.c animator.c.h animator.c.l.h animator.i
	-rm -f animload.c animload.c.h animload.c.l.h animload.i
	-rm -f animloca.c animloca.c.h animloca.c.l.h animloca.i
	-rm -f anim.c anim.c.h anim.c.l.h anim.i anim.so anim.o
	-rm -f test.c test.c.h test.c.l.h test.o test.i test
	-rm -f anim.cbl anim.i anim.ani
	-rm -f do-anim.cpy
	-rm -rf temp

tp-test: tp-library animator test.cbl
	./animator test.cbl -x
	./test
	cat TELCO.TXT

animate-animator:
	cp animator.cbl anim.cbl
	sed -i 's/END PROGRAM/ENDXPROGRAM/' anim.cbl
	sed -i 's/"GOBACK"/"STOP RUN"/' anim.cbl
	./animator anim.cbl -x -k -vv
